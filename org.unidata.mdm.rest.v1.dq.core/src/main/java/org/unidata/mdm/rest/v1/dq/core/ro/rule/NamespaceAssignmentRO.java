/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.ro.rule;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Mikhail Mikhailov on Mar 27, 2021
 * NS assignment REST view.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NamespaceAssignmentRO {
    /**
     * The NS.
     */
    private String nameSpace;
    /**
     * Entity assignments.
     */
    private List<EntityAssignmentRO> assignments;
    /**
     * Constructor.
     */
    public NamespaceAssignmentRO() {
        super();
    }
    /**
     * @return the nameSpace
     */
    public String getNameSpace() {
        return nameSpace;
    }
    /**
     * @param nameSpace the nameSpace to set
     */
    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }
    /**
     * @return the assignments
     */
    public List<EntityAssignmentRO> getAssignments() {
        return Objects.isNull(assignments) ? Collections.emptyList() : assignments;
    }
    /**
     * @param assignments the assignments to set
     */
    public void setAssignments(List<EntityAssignmentRO> assignments) {
        this.assignments = assignments;
    }
}
