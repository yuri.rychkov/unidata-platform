/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.core.module;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.system.type.module.AbstractModule;
import org.unidata.mdm.system.type.module.Dependency;
import org.unidata.mdm.system.type.rendering.RenderingProvider;

// import org.unidata.mdm.rest.v1.dq.core.rendering.DataQualityRenderingProvider;

public class DataQualityRestModule extends AbstractModule {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataQualityRestModule.class);

    private static final Collection<Dependency> DEPENDENCIES = Arrays.asList(
            new Dependency("org.unidata.mdm.rest.core", "6.0"),
            new Dependency("org.unidata.mdm.dq.core", "6.0"));


//    @Autowired
//    private DataQualityRenderingProvider renderingProvider;

    @Override
    public Collection<Dependency> getDependencies() {
        return DEPENDENCIES;
    }

//    @Override
//    public Collection<RenderingProvider> getRenderingProviders() {
//        return Collections.singletonList(renderingProvider);
//    }

    @Override
    public String getId() {
        return "org.unidata.mdm.rest.v1.dq.core";
    }

    @Override
    public String getVersion() {
        return "6.0";
    }

    @Override
    public String getName() {
        return "DQ Rest";
    }

    @Override
    public String getDescription() {
        return "DQ Rest module";
    }

    @Override
    public void install() {
        LOGGER.info("Install");
    }

    @Override
    public void uninstall() {
        LOGGER.info("Uninstall");
    }

    @Override
    public void start() {
        LOGGER.info("Start");
    }

    @Override
    public void stop() {
        LOGGER.info("Stop");
    }
}
