/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */
package org.unidata.mdm.rest.v1.dq.core.ro.functions;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;

import org.unidata.mdm.rest.v1.dq.core.ro.AbstractQualityElementRO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Michael Yashin. Created on 20.05.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CleanseFunctionGroupRO extends AbstractQualityElementRO {

    @ArraySchema(arraySchema = @Schema(implementation = CleanseFunctionGroupRO.class, description = "CleanseFunctionGroupRO children tree, if any."))
    protected List<CleanseFunctionGroupRO> children;

    protected List<JavaCleanseFunctionRO> javaFunctions;

    protected List<PythonCleanseFunctionRO> pythonFunctions;

    protected List<GroovyCleanseFunctionRO> groovyFunctions;

    protected List<CompositeCleanseFunctionRO> compositeFunctions;

    @Nonnull
    public List<CleanseFunctionGroupRO> getChildren() {
        return Objects.isNull(children) ? Collections.emptyList() : children;
    }

    public void setChildren(List<CleanseFunctionGroupRO> groups) {
        this.children = groups;
    }

    /**
     * @return the javaFunctions
     */
    @Nonnull
    public List<JavaCleanseFunctionRO> getJavaFunctions() {
        return Objects.isNull(javaFunctions) ? Collections.emptyList() : javaFunctions;
    }

    /**
     * @param javaFunctions the javaFunctions to set
     */
    public void setJavaFunctions(List<JavaCleanseFunctionRO> javaFunctions) {
        this.javaFunctions = javaFunctions;
    }

    /**
     * @return the pythonFunctions
     */
    @Nonnull
    public List<PythonCleanseFunctionRO> getPythonFunctions() {
        return Objects.isNull(pythonFunctions) ? Collections.emptyList() : pythonFunctions;
    }

    /**
     * @param pythonFunctions the pythonFunctions to set
     */
    public void setPythonFunctions(List<PythonCleanseFunctionRO> pythonFunctions) {
        this.pythonFunctions = pythonFunctions;
    }

    /**
     * @return the groovyFunctions
     */
    @Nonnull
    public List<GroovyCleanseFunctionRO> getGroovyFunctions() {
        return Objects.isNull(groovyFunctions) ? Collections.emptyList() : groovyFunctions;
    }

    /**
     * @param groovyFunctions the groovyFunctions to set
     */
    public void setGroovyFunctions(List<GroovyCleanseFunctionRO> groovyFunctions) {
        this.groovyFunctions = groovyFunctions;
    }

    /**
     * @return the compositeFunctions
     */
    @Nonnull
    public List<CompositeCleanseFunctionRO> getCompositeFunctions() {
        return Objects.isNull(compositeFunctions) ? Collections.emptyList() : compositeFunctions;
    }

    /**
     * @param compositeFunctions the compositeFunctions to set
     */
    public void setCompositeFunctions(List<CompositeCleanseFunctionRO> compositeFunctions) {
        this.compositeFunctions = compositeFunctions;
    }
}
