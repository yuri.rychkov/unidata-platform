package org.unidata.mdm.rest.v1.dq.core.ro.constant;

import org.unidata.mdm.dq.core.type.constant.ArrayValueConstantType;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov on Feb 8, 2021
 */
public class ArrayValueRO {
    /**
     * One of {@link ArrayValueConstantType}.
     */
    @Schema(allowableValues = {"DATE", "TIME", "TIMESTAMP", "STRING", "INTEGER", "NUMBER"})
    private String type;
    /**
     * The value.
     */
    private Object[] values;
    /**
     * Constructor.
     */
    public ArrayValueRO() {
        super();
    }
    /**
     * Constructor.
     */
    public ArrayValueRO(String type, Object[] values) {
        super();
        this.type = type;
        this.values = values;
    }
    /**
     * @return the type
     */
    public String getType() {
        return type;
    }
    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * @return the values
     */
    public Object[] getValues() {
        return values;
    }
    /**
     * @param values the values to set
     */
    public void setValues(Object[] values) {
        this.values = values;
    }

}
