/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.converter;

import java.time.OffsetDateTime;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.dq.core.type.model.source.rule.MappingSetSource;
import org.unidata.mdm.rest.v1.dq.core.ro.rule.MappingSetRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on Mar 1, 2021
 */
public class MappingSetsConverter extends Converter<MappingSetSource, MappingSetRO> {
    /**
     * RMC.
     */
    private static final RuleMappingConverter RULE_MAPPING_CONVERTER = new RuleMappingConverter();
    /**
     * Constructor.
     * @param to
     * @param from
     */
    public MappingSetsConverter() {
        super(MappingSetsConverter::convert, MappingSetsConverter::convert);
    }

    private static MappingSetRO convert(MappingSetSource source) {

        MappingSetRO target = new MappingSetRO();

        target.setName(source.getName());
        target.setDisplayName(source.getDisplayName());
        target.setDescription(source.getDescription());
        target.setCreateDate(source.getCreateDate());
        target.setUpdateDate(source.getUpdateDate());
        target.setCreatedBy(source.getCreatedBy());
        target.setUpdatedBy(source.getUpdatedBy());
        target.setMappings(RULE_MAPPING_CONVERTER.to(source.getMappings()));

        return target;
    }

    private static MappingSetSource convert(MappingSetRO source) {

        String user = SecurityUtils.getCurrentUserName();
        OffsetDateTime now = OffsetDateTime.now();

        return new MappingSetSource()
                .withName(source.getName())
                .withDisplayName(source.getDisplayName())
                .withDescription(source.getDescription())
                .withCreateDate(Objects.isNull(source.getCreateDate()) ? now : source.getCreateDate())
                .withCreatedBy(StringUtils.isBlank(source.getCreatedBy()) ? user : source.getCreatedBy())
                .withUpdateDate(Objects.isNull(source.getCreateDate()) ? null : now)
                .withUpdatedBy(StringUtils.isBlank(source.getCreatedBy()) ? null : user)
                .withMappings(RULE_MAPPING_CONVERTER.from(source.getMappings()));
    }
}
