/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.core.ro.functions;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.rest.v1.dq.core.ro.AbstractQualityElementRO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov on Feb 16, 2021
 * The type, used to update CF groups structure,
 * just to avoid properties clutter.
 */
@Schema(description = "The type is used to update groups structure.")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CleanseFunctionGroupInfoRO extends AbstractQualityElementRO {
    /**
     * Children.
     */
    @ArraySchema(arraySchema = @Schema(implementation = CleanseFunctionGroupInfoRO.class, description = "CleanseFunctionGroupInfoRO children tree, if any."))
    private List<CleanseFunctionGroupInfoRO> children;
    /**
     * MappedFunction names.
     */
    private List<String> functionsNames;
    /**
     * Constructor.
     */
    public CleanseFunctionGroupInfoRO() {
        super();
    }
    /**
     * @return the functionsNames
     */
    public List<String> getFunctionsNames() {
        return Objects.isNull(functionsNames) ? Collections.emptyList() : functionsNames;
    }
    /**
     * @param functionsNames the functionsNames to set
     */
    public void setFunctionsNames(List<String> functionsNames) {
        this.functionsNames = functionsNames;
    }
    /**
     * @return the children
     */
    public List<CleanseFunctionGroupInfoRO> getChildren() {
        return Objects.isNull(children) ? Collections.emptyList() : children;
    }
    /**
     * @param children the children to set
     */
    public void setChildren(List<CleanseFunctionGroupInfoRO> children) {
        this.children = children;
    }
}
