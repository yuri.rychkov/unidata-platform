/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */

package org.unidata.mdm.rest.v1.dq.data.configuration;

import java.util.Arrays;
import java.util.Collections;

import javax.ws.rs.ext.RuntimeDelegate;

import org.apache.cxf.Bus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.openapi.OpenApiFeature;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.unidata.mdm.rest.system.exception.DetailedRestExceptionMapper;
import org.unidata.mdm.rest.system.service.ReceiveInInterceptor;
import org.unidata.mdm.rest.system.util.OpenApiMetadataFactory;
import org.unidata.mdm.rest.v1.dq.data.service.DQSandboxRestService;
import org.unidata.mdm.rest.v1.dq.data.service.DataQualityDataRestApplication;
import org.unidata.mdm.system.configuration.AbstractConfiguration;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

/**
 * @author Alexey Tsarapkin
 */
@Configuration
public class DataQualityDataRestConfiguration extends AbstractConfiguration {

    private static final ConfigurationId ID = () -> "DQ_REST_DATA_CONFIGURATION";

    public static ApplicationContext getApplicationContext() {
        return CONFIGURED_CONTEXT_MAP.get(ID);
    }

    @Override
    protected ConfigurationId getId() {
        return ID;
    }

    @Bean
    public DataQualityDataRestApplication dqRestDataApplication() {
        return new DataQualityDataRestApplication();
    }

    @Bean
    public DQSandboxRestService dqSandboxRestService() {
        return new DQSandboxRestService();
    }

    @Bean
    public Server server(
            final DQSandboxRestService dqSandboxRestService,
            final Bus cxf,
            final JacksonJaxbJsonProvider jacksonJaxbJsonProvider,
            final DetailedRestExceptionMapper restExceptionMapper,
            final ReceiveInInterceptor receiveInInterceptor,
            final DataQualityDataRestApplication dqRestDataApplication) {

        final JAXRSServerFactoryBean jaxrsServerFactoryBean = RuntimeDelegate.getInstance()
                .createEndpoint(dqRestDataApplication, JAXRSServerFactoryBean.class);

        final OpenApiFeature dqOpenApiFeature = OpenApiMetadataFactory.openApiFeature(
                "Unidata DQ rest data API",
                "Unidata DQ data REST API operations",
                dqRestDataApplication, cxf,
                "org.unidata.mdm.rest.v1.dq.data.service", "org.unidata.mdm.rest.v1.dq.data.ro");

        jaxrsServerFactoryBean.setFeatures(Arrays.asList(dqOpenApiFeature));
        jaxrsServerFactoryBean.setProviders(Arrays.asList(jacksonJaxbJsonProvider, restExceptionMapper));
        jaxrsServerFactoryBean.setInInterceptors(Collections.singletonList(receiveInInterceptor));
        jaxrsServerFactoryBean.setServiceBeans(Arrays.asList(dqSandboxRestService));

        return jaxrsServerFactoryBean.create();
    }
}
