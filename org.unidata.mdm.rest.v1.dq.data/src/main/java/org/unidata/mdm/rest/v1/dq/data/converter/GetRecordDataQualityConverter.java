/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.v1.dq.data.converter;

import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;

import org.unidata.mdm.dq.data.dto.RecordGetQualityResult;
import org.unidata.mdm.rest.v1.dq.core.converter.DataQualityRuleResultConverter;
import org.unidata.mdm.rest.v1.dq.core.ro.result.DataQualityRuleResultRO;
import org.unidata.mdm.rest.v1.dq.core.ro.result.DataQualitySetResultRO;
import org.unidata.mdm.rest.v1.dq.data.ro.GetRecordsDataQualityResultRO;
import org.unidata.mdm.system.convert.Converter;

public class GetRecordDataQualityConverter extends Converter<RecordGetQualityResult, GetRecordsDataQualityResultRO> {
    /**
     * Rules converter.
     */
    private static final DataQualityRuleResultConverter DATA_QUALITY_RULE_RESULT_CONVERTER = new DataQualityRuleResultConverter();

    public GetRecordDataQualityConverter() {
        super(GetRecordDataQualityConverter::convert, null);
    }

    private static GetRecordsDataQualityResultRO convert(RecordGetQualityResult source) {

        GetRecordsDataQualityResultRO target = new GetRecordsDataQualityResultRO();
        target.setResults(Objects.isNull(source.getPayload())
                ? Collections.emptyList()
                : source.getPayload().getResults().entrySet().stream()
                    .map(s -> {

                        DataQualitySetResultRO set = new DataQualitySetResultRO();
                        set.setSetName(s.getKey().getName());
                        set.setSetDisplayName(s.getKey().getDisplayName());
                        set.setRules(s.getValue().getResult().entrySet().stream()
                                .map(entry -> new DataQualityRuleResultRO(entry.getKey().getName(), entry.getKey().getDisplayName(), DATA_QUALITY_RULE_RESULT_CONVERTER.to(entry.getValue())))
                                .collect(Collectors.toList()));

                        return set;
                    })
                    .collect(Collectors.toList()));

        return target;
    }

}
