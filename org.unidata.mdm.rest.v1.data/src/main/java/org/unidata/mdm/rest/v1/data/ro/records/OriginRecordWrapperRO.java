/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.ro.records;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ibykov
 */
public class OriginRecordWrapperRO {
	
	/** The origin record. */
	private OriginRecordRO originRecord;
	
	/** The winner paths. */
	private List<String> winnerPaths;

	/**
	 * Gets the origin record.
	 *
	 * @return the origin record
	 */
	public OriginRecordRO getOriginRecord() {
		return originRecord;
	}

	/**
	 * Sets the origin record.
	 *
	 * @param originRecord the new origin record
	 */
	public void setOriginRecord(OriginRecordRO originRecord) {
		this.originRecord = originRecord;
	}

	/**
	 * Gets the winner paths.
	 *
	 * @return the winner paths
	 */
	public List<String> getWinnerPaths() {
		if (this.winnerPaths == null) {
			this.winnerPaths = new ArrayList<>();
		}
		return winnerPaths;
	}

	/**
	 * Sets the winner paths.
	 *
	 * @param winnerPaths the new winner paths
	 */
	public void setWinnerPaths(List<String> winnerPaths) {
		this.winnerPaths = winnerPaths;
	}
}
