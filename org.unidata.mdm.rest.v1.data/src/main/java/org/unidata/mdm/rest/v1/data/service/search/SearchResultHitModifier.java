/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.service.search;

import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nonnull;

import org.apache.commons.lang3.tuple.Triple;
import org.unidata.mdm.rest.v1.search.type.result.SearchResultProcessingElements;
import org.unidata.mdm.search.dto.SearchResultDTO;

/**
 * @author Dmitry Kopin on 30.10.2017.
 */
public interface SearchResultHitModifier {

    /**
     * Method provide search with after processing which modify all processed elements to display view.
     * @param searchResult result of search by ctx
     * @param attributesSource the source of attributes (entity, relation, etc.)
     * @param fieldPrefix needed for relations and classifiers
     */
    void modifySearchResult(@Nonnull SearchResultDTO searchResult, String attributesSource,
            String fieldPrefix);

    /**
     * Method provide search with after processing which modify all processed elements to display view.
     *
     * @param searchResult result of search by ctx
     */
    void modifySearchResult(@Nonnull SearchResultDTO searchResult);

    /**
     * Method provide search with after processing which modify processed elements to display view.
     *
     * @param searchResult result of search by ctx
     * @param enumSet elements to process
     */
    void modifySearchResult(@Nonnull SearchResultDTO searchResult, EnumSet<SearchResultProcessingElements> enumSet);

    /**
     * Method provide search with after processing which modify all processed elements to display view.
     * @param searchResult result of search by ctx
     * @param attributesSource the source of attributes (entity, relation, etc.)
     * @param fieldPrefix needed for relations and classifiers
     * @param elements processing elements

     */
    void modifySearchResult(@Nonnull SearchResultDTO searchResult, String attributesSource,
            String fieldPrefix, Set<SearchResultProcessingElements> elements);

    /**
     * Get display name collections for relation by to Side ids and periods
     * @param relationName relation name
     * @param toIds collections of tuples (etalonIdTo, validFrom, validTo)
     * @return display names map, key - etalonIdTo, value collections of tuples (displayName, validFrom, validTo)
     */
    Map<String, List<Triple<String, Date, Date>>> getDisplayNamesForRelationTo(String relationName, List<Triple<String, Date, Date>> toIds);
    /**
     * Get display name collections for relation by to Side ids and periods
     * @param relationName relation name
     * @param fromEtalonId from etalon id
     * @param  relationId optiona relation id
     * @param validFrom valid from
     * @param validTo valid to
     * @return display names map, key - etalonIdTo, value collections of tuples (displayName, validFrom, validTo)
     */
    Map<String, List<Triple<String, Date, Date>>> getDisplayNamesForRelationTo(String relationName, @Nonnull  String fromEtalonId, String relationId, Date validFrom, Date validTo);
    /**
     * Extract display value for date from display names map
     * @param displayNames display names map, key - etalonIdTo, value collections of tuples (displayName, validFrom, validTo)
     * @param etalonIdTo etalon id to
     * @param validFrom
     * @param validTo
     * @param now
     * @return
     */
    String extractDisplayName(Map<String, List<Triple<String, Date, Date>>> displayNames, String etalonIdTo,
                              Date validFrom, Date validTo, Date now);
}
