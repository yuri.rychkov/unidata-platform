/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.ro.relations;

import java.util.List;

import org.unidata.mdm.rest.v1.data.ro.records.EtalonRelationToRO;

/**
 * Wrapper object for references relation (only diff)
 *
 * @author Dmitry Kopin on 19.06.2018.
 */
public class RelationReferencesWrapperRO {
    /**
     * records for update
     */
    private List<EtalonRelationToRO> toUpdate;
    /**
     * records for delete
     */
    private List<RelationDeleteWrapperRO> toDelete;

    public List<EtalonRelationToRO> getToUpdate() {
        return toUpdate;
    }

    public void setToUpdate(List<EtalonRelationToRO> toUpdate) {
        this.toUpdate = toUpdate;
    }

    public List<RelationDeleteWrapperRO> getToDelete() {
        return toDelete;
    }

    public void setToDelete(List<RelationDeleteWrapperRO> toDelete) {
        this.toDelete = toDelete;
    }
}
