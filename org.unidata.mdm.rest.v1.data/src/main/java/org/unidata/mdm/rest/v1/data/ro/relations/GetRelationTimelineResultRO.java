package org.unidata.mdm.rest.v1.data.ro.relations;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.data.ro.TimelineRO;

/**
 * Get relation timeline result
 *
 * @author Alexandr Serov
 * @since 23.10.2020
 **/
public class GetRelationTimelineResultRO extends DetailedOutputRO {

    private TimelineRO timeline;

    public TimelineRO getTimeline() {
        return timeline;
    }

    public void setTimeline(TimelineRO timeline) {
        this.timeline = timeline;
    }
}
