package org.unidata.mdm.rest.v1.data.ro.atomic;

import org.unidata.mdm.rest.system.ro.DetailedCompositeOutputRO;
import org.unidata.mdm.rest.v1.data.type.rendering.DataRestOutputRenderingAction;
import org.unidata.mdm.system.type.rendering.OutputRenderingAction;

/**
 * Atomic data upsert result
 *
 * @author Alexandr Serov
 * @since 09.11.2020
 **/
public class AtomicDataGetResultRO extends DetailedCompositeOutputRO {

    @Override
    public OutputRenderingAction getOutputRenderingAction() {
        return DataRestOutputRenderingAction.ATOMIC_GET_OUTPUT;
    }
}
