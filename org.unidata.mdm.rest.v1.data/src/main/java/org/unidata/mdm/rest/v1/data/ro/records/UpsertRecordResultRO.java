package org.unidata.mdm.rest.v1.data.ro.records;

import java.util.List;

import org.unidata.mdm.rest.v1.data.ro.relations.UpsertRelationsResultRO;

/**
 * Upsert record result
 *
 * @author Alexandr Serov
 * @since 06.10.2020
 **/
public class UpsertRecordResultRO extends RecordKeysSupportResultRO {

    /**
     * Data record
     */
    private EtalonRecordRO dataRecord;
    private List<String> duplicates;

    private UpsertRelationsResultRO relationsResult;

    public EtalonRecordRO getDataRecord() {
        return dataRecord;
    }

    public void setDataRecord(EtalonRecordRO dataRecord) {
        this.dataRecord = dataRecord;
    }

    public List<String> getDuplicates() {
        return duplicates;
    }

    public void setDuplicates(List<String> duplicates) {
        this.duplicates = duplicates;
    }

    /**
     * @return the relationsResult
     */
    public UpsertRelationsResultRO getRelationsResult() {
        return relationsResult;
    }

    /**
     * @param relationsResult the relationsResult to set
     */
    public void setRelationsResult(UpsertRelationsResultRO relationsResult) {
        this.relationsResult = relationsResult;
    }
}
