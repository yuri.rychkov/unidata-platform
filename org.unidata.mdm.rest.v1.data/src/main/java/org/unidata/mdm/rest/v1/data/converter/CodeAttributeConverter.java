/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.springframework.util.CollectionUtils;
import org.unidata.mdm.core.type.data.CodeAttribute;
import org.unidata.mdm.core.type.data.extended.WinnerInformationCodeAttribute;
import org.unidata.mdm.core.type.data.impl.IntegerCodeAttributeImpl;
import org.unidata.mdm.core.type.data.impl.StringCodeAttributeImpl;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.keys.RecordOriginKey;
import org.unidata.mdm.rest.meta.ro.CodeDataTypeRO;
import org.unidata.mdm.rest.v1.data.ro.attributes.CodeAttributeRO;
import org.unidata.mdm.rest.v1.data.ro.extended.ExtendedCodeAttributeRO;

/**
 * @author Mikhail Mikhailov
 *
 */
public class CodeAttributeConverter {

    /**
     * Constructor.
     */
    private CodeAttributeConverter() {
        super();
    }

    public static CodeAttribute<?> from(CodeAttributeRO source) {

        if (Objects.isNull(source)) {
            return null;
        }

        CodeAttribute<?> result;
        org.unidata.mdm.core.type.data.CodeAttribute.CodeDataType type
            = org.unidata.mdm.core.type.data.CodeAttribute.CodeDataType.valueOf(source.getType().name());

        switch (type) {
        case INTEGER:
            result = new IntegerCodeAttributeImpl(source.getName(), source.getValue() == null ? null : (Long) source.getValue());
            break;
        case STRING:
            result = new StringCodeAttributeImpl(source.getName(), source.getValue() == null ? null : (String) source.getValue());
            break;
        default:
            return null;
        }

        return result;
    }

    public static List<CodeAttribute<?>> from(Collection<CodeAttributeRO> source) {

        if (CollectionUtils.isEmpty(source)) {
            return Collections.emptyList();
        }

        List<CodeAttribute<?>> result = new ArrayList<>(source.size());
        for (CodeAttributeRO val : source) {
            CodeAttribute<?> converted = from(val);
            if (Objects.isNull(converted)) {
                continue;
            }

            result.add(converted);
        }

        return result;
    }

    public static CodeAttributeRO to(CodeAttribute<?> source) {

        if (Objects.isNull(source)) {
            return null;
        }

        CodeAttributeRO result = new CodeAttributeRO();
        List<Object> supplementary = null;

        if (source.hasSupplementary()) {
            supplementary = new ArrayList<>(source.getSupplementary().size());
            supplementary.addAll(source.getSupplementary());
        }

        result.setName(source.getName());
        result.setType(CodeDataTypeRO.valueOf(source.getDataType().name()));
        result.setValue(source.getValue());
        result.setSupplementary(supplementary);

        return result;
    }

    public static List<CodeAttributeRO> to(Collection<CodeAttribute<?>> source) {
        if (CollectionUtils.isEmpty(source)) {
            return Collections.emptyList();
        }

        List<CodeAttributeRO> result = new ArrayList<>(source.size());
        for (CodeAttribute<?> sourceAttribute : source) {
            if(sourceAttribute != null){
                CodeAttributeRO targetAttribute = new CodeAttributeRO();
                populate(sourceAttribute, targetAttribute);
                result.add(targetAttribute);
            }
        }
        return result;
    }

    public static List<CodeAttributeRO> to(Collection<CodeAttribute<?>> source, EtalonRecord etalonRecord, RecordOriginKey originKey) {
        if (CollectionUtils.isEmpty(source)) {
            return Collections.emptyList();
        }

        List<CodeAttributeRO> result = new ArrayList<>(source.size());
        for (CodeAttribute<?> sourceAttribute : source) {
            if(sourceAttribute != null){
                ExtendedCodeAttributeRO targetAttribute = new ExtendedCodeAttributeRO();
                populate(sourceAttribute, targetAttribute);

                CodeAttribute winnerAttribute = etalonRecord.getCodeAttribute(sourceAttribute.getName());
                targetAttribute.setWinner(winnerAttribute instanceof WinnerInformationCodeAttribute
                                && originKey.getExternalId().equals(((WinnerInformationCodeAttribute) winnerAttribute).getWinnerExternalId())
                                && originKey.getSourceSystem().equals(((WinnerInformationCodeAttribute) winnerAttribute).getWinnerSourceSystem()));


                result.add(targetAttribute);
            }
        }
        return result;
    }

    private static void populate(CodeAttribute<?> source, CodeAttributeRO target) {
        List<Object> supplementary = null;

        if (source.hasSupplementary()) {
            supplementary = new ArrayList<>(source.getSupplementary().size());
            supplementary.addAll(source.getSupplementary());
        }

        target.setName(source.getName());
        target.setType(CodeDataTypeRO.valueOf(source.getDataType().name()));
        target.setValue(source.getValue());
        target.setSupplementary(supplementary);
    }
}
