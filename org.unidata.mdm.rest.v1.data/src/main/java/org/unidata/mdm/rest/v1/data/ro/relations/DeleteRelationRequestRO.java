package org.unidata.mdm.rest.v1.data.ro.relations;

import java.time.LocalDateTime;

/**
 * Delete relations request
 *
 * @author Alexandr Serov
 * @since 19.10.2020
 **/
public class DeleteRelationRequestRO {

    private Long draftId;
    private String originId;
    private String etalonId;
    private LocalDateTime validFrom;
    private LocalDateTime validTo;
    private boolean inactivateEtalon;
    private boolean inactivateOrigin;
    private boolean inactivatePeriod;

    /**
     * @return the draftId
     */
    public Long getDraftId() {
        return draftId;
    }

    /**
     * @param draftId the draftId to set
     */
    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    public String getEtalonId() {
        return etalonId;
    }

    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }

    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDateTime validTo) {
        this.validTo = validTo;
    }

    public boolean isInactivateEtalon() {
        return inactivateEtalon;
    }

    public void setInactivateEtalon(boolean inactivateEtalon) {
        this.inactivateEtalon = inactivateEtalon;
    }

    public boolean isInactivateOrigin() {
        return inactivateOrigin;
    }

    public void setInactivateOrigin(boolean inactivateOrigin) {
        this.inactivateOrigin = inactivateOrigin;
    }

    public boolean isInactivatePeriod() {
        return inactivatePeriod;
    }

    public void setInactivatePeriod(boolean inactivatePeriod) {
        this.inactivatePeriod = inactivatePeriod;
    }
}
