/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.ro.attributes;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.unidata.mdm.rest.meta.ro.CodeDataTypeRO;
import org.unidata.mdm.rest.v1.data.ro.serializer.CodeAttributeDeserializer;
import org.unidata.mdm.rest.v1.data.ro.serializer.CodeAttributeSerializer;

/**
 * @author Michael Yashin. Created on 02.06.2015.
 */
@JsonDeserialize(using = CodeAttributeDeserializer.class)
@JsonSerialize(using = CodeAttributeSerializer.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CodeAttributeRO {
    /**
     * Name of the attribute.
     */
    private String name;
    /**
     * The value.
     */
    private Object value;
    /**
     * Its value.
     */
    private List<Object> supplementary;
    /**
     * Value data type.
     */
    private CodeDataTypeRO type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Object value) {
        this.value = value;
    }

    public List<Object> getSupplementary() {
        return supplementary;
    }

    public void setSupplementary(List<Object> supplementary) {
        this.supplementary = supplementary;
    }

    public CodeDataTypeRO getType() {
        return type;
    }

    public void setType(CodeDataTypeRO type) {
        this.type = type;
    }
}
