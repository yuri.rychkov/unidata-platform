package org.unidata.mdm.rest.v1.data.ro.search;

import java.util.List;

import io.swagger.v3.oas.annotations.Parameter;
import org.unidata.mdm.rest.v1.search.ro.SearchQueryRO;
import org.unidata.mdm.rest.v1.search.ro.SearchSortFieldRO;

/**
 * Base search data query
 *
 * @author Alexandr Serov
 * @since 04.12.2020
 **/
public class SimpleSearchDataQueryRO extends SearchQueryRO {

    private SearchDataType searchDataType;

    @Parameter(description = "Apply the following facets.")
    private List<String> facets;

    @Parameter(description = "Apply the following sorting settings")
    private List<SearchSortFieldRO> sortFields;

    @Parameter(description = "Fields to return.")
    private List<String> returnFields;

    @Parameter(description = "Search after values")
    private List<Object> searchAfter;

    @Parameter(description = "Return total count")
    private boolean totalCount;

    @Parameter(description = "Count only or return results too")
    private boolean countOnly;

    @Parameter(description = "Shortcut for all searchable fields")
    private boolean returnAllFields;

    @Parameter(description = "Set default sort if sortFields is empty")
    private boolean defaultSort;

    @Parameter(description = "Fetch all without applying a query")
    private boolean fetchAll;

    @Parameter(description = "Fetch source or not.")
    private boolean source;

    @Parameter(description = "Search as type")
    private boolean sayt;

    @Parameter(description = "Result limit")
    private int count;

    @Parameter(description = "Request offset")
    private int page;

    public SearchDataType getSearchDataType() {
        return searchDataType;
    }

    public void setSearchDataType(SearchDataType searchDataType) {
        this.searchDataType = searchDataType;
    }

    public List<String> getFacets() {
        return facets;
    }

    public void setFacets(List<String> facets) {
        this.facets = facets;
    }

    public List<SearchSortFieldRO> getSortFields() {
        return sortFields;
    }

    public void setSortFields(List<SearchSortFieldRO> sortFields) {
        this.sortFields = sortFields;
    }

    public List<String> getReturnFields() {
        return returnFields;
    }

    public void setReturnFields(List<String> returnFields) {
        this.returnFields = returnFields;
    }

    public List<Object> getSearchAfter() {
        return searchAfter;
    }

    public void setSearchAfter(List<Object> searchAfter) {
        this.searchAfter = searchAfter;
    }

    public boolean isTotalCount() {
        return totalCount;
    }

    public void setTotalCount(boolean totalCount) {
        this.totalCount = totalCount;
    }

    public boolean isCountOnly() {
        return countOnly;
    }

    public void setCountOnly(boolean countOnly) {
        this.countOnly = countOnly;
    }

    public boolean isReturnAllFields() {
        return returnAllFields;
    }

    public void setReturnAllFields(boolean returnAllFields) {
        this.returnAllFields = returnAllFields;
    }

    public boolean isDefaultSort() {
        return defaultSort;
    }

    public void setDefaultSort(boolean defaultSort) {
        this.defaultSort = defaultSort;
    }

    public boolean isFetchAll() {
        return fetchAll;
    }

    public void setFetchAll(boolean fetchAll) {
        this.fetchAll = fetchAll;
    }

    public boolean isSource() {
        return source;
    }

    public void setSource(boolean source) {
        this.source = source;
    }

    public boolean isSayt() {
        return sayt;
    }

    public void setSayt(boolean sayt) {
        this.sayt = sayt;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
