package org.unidata.mdm.rest.v1.data.ro.lob;

/**
 * Delete large object request
 *
 * @author Alexandr Serov
 * @since 14.10.2020
 **/
public class DeleteLobRequestRO {

    private String id;
    private String attribute;
    private boolean binary;

    public DeleteLobRequestRO(String id, String attribute, boolean binary) {
        this.id = id;
        this.attribute = attribute;
        this.binary = binary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public boolean isBinary() {
        return binary;
    }

    public void setBinary(boolean binary) {
        this.binary = binary;
    }
}
