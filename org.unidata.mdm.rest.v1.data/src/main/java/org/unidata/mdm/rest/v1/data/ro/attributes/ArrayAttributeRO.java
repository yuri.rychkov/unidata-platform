/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.ro.attributes;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.unidata.mdm.rest.meta.ro.ArrayDataType;
import org.unidata.mdm.rest.v1.data.ro.serializer.ArrayAttributeDeserializer;
import org.unidata.mdm.rest.v1.data.ro.serializer.ArrayAttributeSerializer;

/**
 * @author Michael Yashin. Created on 02.06.2015.
 */
@JsonDeserialize(using = ArrayAttributeDeserializer.class)
@JsonSerialize(using = ArrayAttributeSerializer.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArrayAttributeRO {
    /**
     * Name of the attribute.
     */
    protected String name;
    /**
     * Its value.
     */
    protected List<ArrayObjectRO> value;
    /**
     * Value data type.
     */
    protected ArrayDataType type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ArrayObjectRO> getValue() {
        return value;
    }

    public void setValue(List<ArrayObjectRO> value) {
        this.value = value;
    }

    public ArrayDataType getType() {
        return type;
    }

    public void setType(ArrayDataType type) {
        this.type = type;
    }
}
