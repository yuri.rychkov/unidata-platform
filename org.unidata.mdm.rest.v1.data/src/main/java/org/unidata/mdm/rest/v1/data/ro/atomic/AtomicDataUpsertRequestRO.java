package org.unidata.mdm.rest.v1.data.ro.atomic;

import org.unidata.mdm.rest.system.ro.CompositeInputRO;
import org.unidata.mdm.rest.v1.data.type.rendering.DataRestInputRenderingAction;
import org.unidata.mdm.system.type.rendering.InputRenderingAction;

/**
 * Atomic data upsert request
 *
 * @author Alexandr Serov
 * @since 06.11.2020
 **/
public class AtomicDataUpsertRequestRO extends CompositeInputRO {

    @Override
    public InputRenderingAction getInputRenderingAction() {
        return DataRestInputRenderingAction.ATOMIC_UPSERT_INPUT;
    }
}
