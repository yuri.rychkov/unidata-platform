package org.unidata.mdm.rest.v1.data.ro.records;

import org.unidata.mdm.rest.v1.data.ro.relations.RelationContainsWrapperRO;
import org.unidata.mdm.rest.v1.data.ro.relations.RelationManyToManyWrapperRO;
import org.unidata.mdm.rest.v1.data.ro.relations.RelationReferencesWrapperRO;

/**
 * Upsert entity result
 *
 * @author Alexandr Serov
 * @since 06.10.2020
 **/
public class UpsertRequestRO {
    /**
     * Data record
     */
    private Long draftId;
    private DataRecordRO dataRecord;
    private RelationContainsWrapperRO relationContains;
    private RelationReferencesWrapperRO relationReferences;
    private RelationManyToManyWrapperRO relationManyToMany;
    private String operationType;

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }
    public DataRecordRO getDataRecord() {
        return dataRecord;
    }

    public void setDataRecord(DataRecordRO dataRecord) {
        this.dataRecord = dataRecord;
    }

    public RelationContainsWrapperRO getRelationContains() {
        return relationContains;
    }

    public void setRelationContains(RelationContainsWrapperRO relationReference) {
        this.relationContains = relationReference;
    }

    public RelationReferencesWrapperRO getRelationReferences() {
        return relationReferences;
    }

    public void setRelationReferences(RelationReferencesWrapperRO relationContains) {
        this.relationReferences = relationContains;
    }

    public RelationManyToManyWrapperRO getRelationManyToMany() {
        return relationManyToMany;
    }

    public void setRelationManyToMany(RelationManyToManyWrapperRO relationManyToMany) {
        this.relationManyToMany = relationManyToMany;
    }

    /**
     * @return the draftId
     */
    public Long getDraftId() {
        return draftId;
    }

    /**
     * @param draftId the draftId to set
     */
    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }
}
