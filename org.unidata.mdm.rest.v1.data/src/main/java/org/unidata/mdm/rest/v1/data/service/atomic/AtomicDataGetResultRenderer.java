package org.unidata.mdm.rest.v1.data.service.atomic;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import org.unidata.mdm.data.dto.GetRecordDTO;
import org.unidata.mdm.rest.system.service.TypedRestOutputRenderer;
import org.unidata.mdm.rest.v1.data.converter.DataRecordEtalonConverter;
import org.unidata.mdm.rest.v1.data.converter.RecordKeysConverter;
import org.unidata.mdm.rest.v1.data.module.DataRestModule;
import org.unidata.mdm.rest.v1.data.ro.atomic.AtomicDataGetResultRO;
import org.unidata.mdm.rest.v1.data.ro.records.GetRecordResultRO;
import org.unidata.mdm.system.type.rendering.FieldDef;
import org.unidata.mdm.system.type.rendering.FragmentDef;

/**
 * Atomic upsert result renderer
 *
 * @author Alexandr Serov
 * @since 09.11.2020
 **/
public class AtomicDataGetResultRenderer extends TypedRestOutputRenderer<AtomicDataGetResultRO, GetRecordDTO> {

    public AtomicDataGetResultRenderer() {
        super(AtomicDataGetResultRO.class, GetRecordDTO.class, Collections.singletonList(FieldDef.fieldDef(DataRestModule.MODULE_ID, GetRecordResultRO.class)));
    }

    @Override
    protected Map<String, Object> renderFragmentFields(FragmentDef fragmentDef, GetRecordDTO container) {

        GetRecordResultRO result = new GetRecordResultRO();

        result.setRecordKeys(RecordKeysConverter.to(container.getRecordKeys()));
        if (Objects.nonNull(container.getEtalon())) {
            result.setRecord(DataRecordEtalonConverter.to(container.getEtalon(), container.getRecordKeys()));
        }

        return Collections.singletonMap(DataRestModule.MODULE_ID, result);
    }
}
