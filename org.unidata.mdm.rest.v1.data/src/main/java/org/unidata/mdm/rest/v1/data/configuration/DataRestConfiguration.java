/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.configuration;

import java.util.Arrays;
import java.util.Collections;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.RuntimeDelegate;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.apache.cxf.Bus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.openapi.OpenApiFeature;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.unidata.mdm.rest.system.exception.DetailedRestExceptionMapper;
import org.unidata.mdm.rest.system.service.ReceiveInInterceptor;
import org.unidata.mdm.rest.system.util.OpenApiMetadataFactory;
import org.unidata.mdm.rest.v1.data.service.DataRestApplication;
import org.unidata.mdm.rest.v1.data.service.atomic.DataAtomicRestService;
import org.unidata.mdm.rest.v1.data.service.favorite.FavoriteEtalonsRestService;
import org.unidata.mdm.rest.v1.data.service.lob.LargeObjectRestService;
import org.unidata.mdm.rest.v1.data.service.merge.DataMergeRestService;
import org.unidata.mdm.rest.v1.data.service.records.DataRecordsRestService;
import org.unidata.mdm.rest.v1.data.service.relations.DataRelationsRestService;
import org.unidata.mdm.rest.v1.data.service.restore.RestoreRestService;

/**
 * @author Alexander Malyshev
 */
@Configuration
public class DataRestConfiguration {

    @Bean
    public Application dataRestApplication() {
        return new DataRestApplication();
    }

    @Bean
    public DataRecordsRestService entityRestService() {
        return new DataRecordsRestService();
    }

    @Bean
    public DataAtomicRestService atomicRestService() {
        return new DataAtomicRestService();
    }

    @Bean
    public LargeObjectRestService largeObjectRestService() {
        return new LargeObjectRestService();
    }

    @Bean
    public DataMergeRestService dataMergeRestService() {
        return new DataMergeRestService();
    }

    @Bean
    public DataRelationsRestService dataRelationsRestService() {
        return new DataRelationsRestService();
    }

    @Bean
    public RestoreRestService restoreRestService() {
        return new RestoreRestService();
    }

    @Bean
    public FavoriteEtalonsRestService favoriteEtalonsRestService() {
        return new FavoriteEtalonsRestService();
    }

    @Bean
    public DetailedRestExceptionMapper restExceptionMapper() {
        return new DetailedRestExceptionMapper(() -> "DATA_REST_SERVICE");
    }

    @Bean
    public Server server(
        final Bus cxf,
        final Application dataRestApplication,
        final JacksonJaxbJsonProvider jacksonJaxbJsonProvider,
        final DetailedRestExceptionMapper restExceptionMapper,
        final ReceiveInInterceptor receiveInInterceptor,
        final DataRelationsRestService dataRelationsRestService,
        final RestoreRestService restoreRestService,
        final FavoriteEtalonsRestService favoriteEtalonsRestService,
        final DataRecordsRestService dataRecordRestService,
        final LargeObjectRestService largeObjectRestService,
        final DataMergeRestService dataMergeRestService,
        final DataAtomicRestService atomicRestService
    ) {

        JAXRSServerFactoryBean jaxrsServerFactoryBean = RuntimeDelegate.getInstance()
            .createEndpoint(dataRestApplication, JAXRSServerFactoryBean.class);

        final OpenApiFeature dataOpenApiFeature = OpenApiMetadataFactory.openApiFeature(
            "Unidata data API",
            "Unidata data REST API operations",
            dataRestApplication, cxf,
            "org.unidata.mdm.rest.v1.data.service");

        jaxrsServerFactoryBean.setFeatures(Collections.singletonList(dataOpenApiFeature));
        jaxrsServerFactoryBean.setProviders(Arrays.asList(jacksonJaxbJsonProvider, restExceptionMapper));
        jaxrsServerFactoryBean.setInInterceptors(Collections.singletonList(receiveInInterceptor));
        jaxrsServerFactoryBean.setServiceBeans(Arrays.asList(
            dataRelationsRestService,
            atomicRestService,
            restoreRestService,
            favoriteEtalonsRestService,
            dataRecordRestService,
            largeObjectRestService,
            dataMergeRestService
        ));

        return jaxrsServerFactoryBean.create();
    }
}
