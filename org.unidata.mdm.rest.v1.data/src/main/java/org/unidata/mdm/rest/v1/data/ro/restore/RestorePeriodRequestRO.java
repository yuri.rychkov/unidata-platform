package org.unidata.mdm.rest.v1.data.ro.restore;

import java.time.LocalDateTime;

import org.unidata.mdm.rest.v1.data.ro.records.DataRecordRO;

/**
 * Restore period request
 *
 * @author Alexandr Serov
 * @since 19.10.2020
 **/
public class RestorePeriodRequestRO {

    private Long draftId;
    private String etalonKey;
    private LocalDateTime relationValidFrom;
    private LocalDateTime relationValidTo;
    private LocalDateTime validFrom;
    private LocalDateTime validTo;
    private DataRecordRO record;

    public DataRecordRO getRecord() {
        return record;
    }

    public void setRecord(DataRecordRO record) {
        this.record = record;
    }

    public String getEtalonKey() {
        return etalonKey;
    }

    public void setEtalonKey(String etalonKey) {
        this.etalonKey = etalonKey;
    }

    public LocalDateTime getRelationValidFrom() {
        return relationValidFrom;
    }

    public void setRelationValidFrom(LocalDateTime relationValidFrom) {
        this.relationValidFrom = relationValidFrom;
    }

    public LocalDateTime getRelationValidTo() {
        return relationValidTo;
    }

    public void setRelationValidTo(LocalDateTime relationValidTo) {
        this.relationValidTo = relationValidTo;
    }

    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDateTime validTo) {
        this.validTo = validTo;
    }

    /**
     * @return the draftId
     */
    public Long getDraftId() {
        return draftId;
    }

    /**
     * @param draftId the draftId to set
     */
    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }
}
