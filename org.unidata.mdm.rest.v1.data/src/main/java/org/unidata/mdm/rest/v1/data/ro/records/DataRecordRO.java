package org.unidata.mdm.rest.v1.data.ro.records;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.rest.v1.data.ro.attributes.CodeAttributeRO;
import org.unidata.mdm.rest.v1.data.ro.keys.RecordExternalIdRO;
import org.unidata.mdm.rest.v1.data.ro.keys.ExternalIdSupport;
import org.unidata.mdm.rest.v1.data.ro.keys.LsnRO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Data record rest object
 *
 * @author Alexandr Serov
 * @since 19.11.2020
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataRecordRO extends NestedRecordRO implements ExternalIdSupport  {

    private String etalonId;
    private String entityName;
    private List<CodeAttributeRO> codeAttributes;
    private LocalDateTime validFrom;
    private LocalDateTime validTo;
    private RecordExternalIdRO externalId;
    private LsnRO lsn;

    public List<CodeAttributeRO> getCodeAttributes() {
        return Objects.isNull(codeAttributes) ? Collections.emptyList() : codeAttributes;
    }

    public void setCodeAttributes(List<CodeAttributeRO> codeAttributes) {
        this.codeAttributes = codeAttributes;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEtalonId() {
        return etalonId;
    }

    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }


    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDateTime validTo) {
        this.validTo = validTo;
    }

    @Override
    public RecordExternalIdRO getExternalId() {
        return externalId;
    }

    public void setExternalId(RecordExternalIdRO externalId) {
        this.externalId = externalId;
    }

    public LsnRO getLsn() {
        return lsn;
    }

    public void setLsn(LsnRO lsn) {
        this.lsn = lsn;
    }
}
