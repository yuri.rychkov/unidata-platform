/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 *
 */
package org.unidata.mdm.rest.v1.data.converter;


import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.type.data.impl.SerializableDataRecord;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.data.EtalonRecordInfoSection;
import org.unidata.mdm.data.type.keys.RecordEtalonKey;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.rest.v1.data.ro.keys.LsnRO;
import org.unidata.mdm.rest.v1.data.ro.records.DataRecordRO;
import org.unidata.mdm.rest.v1.data.ro.records.EtalonRecordRO;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Mikhail Mikhailov Golden record to REST golden record converter.
 */
public class DataRecordEtalonConverter {

    /**
     * Constructor.
     */
    private DataRecordEtalonConverter() {
        super();
    }

    /**
     * Converts a golden record to a REST golden record.
     * @param record the record
    //     * @param errors errors occurred, may be null
     * @return REST
     */
    public static EtalonRecordRO to(EtalonRecord record, RecordKeys recordKeys) {
        EtalonRecordRO result = null;
        if (record != null) {
            result = new EtalonRecordRO();
            // Informational
            EtalonRecordInfoSection info = record.getInfoSection();
            if (info != null) {
                // Key
                RecordEtalonKey key = info.getEtalonKey();
                result.setEtalonId(key.getId());
                result.setStatus(info.getStatus() != null ? info.getStatus().name() : RecordStatus.ACTIVE.name());
                result.setValidFrom(ConvertUtils.date2LocalDateTime(info.getValidFrom()));
                result.setValidTo(ConvertUtils.date2LocalDateTime(info.getValidTo()));
                result.setCreateDate(info.getCreateDate());
                result.setUpdateDate(info.getUpdateDate());
                result.setEntityName(info.getEntityName());
                result.setCreatedBy(info.getCreatedBy());
                result.setUpdatedBy(info.getUpdatedBy());
                Long lsn = key.getLsn();
                if (lsn != null) {
                    result.setLsn(new LsnRO(lsn, recordKeys.getShard()));
                }
                result.setOperationType(info.getOperationType() != null ? info.getOperationType().name() : null);
                result.setPublished(recordKeys.isPublished());
            }

            // Attributes
            SimpleAttributeConverter.to(record.getSimpleAttributes(), result.getSimpleAttributes());
            ComplexAttributeConverter.to(record.getComplexAttributes(), result.getComplexAttributes());
            result.getCodeAttributes().addAll(CodeAttributeConverter.to(record.getCodeAttributes()));
            result.getArrayAttributes().addAll(ArrayAttributeConverter.to(record.getArrayAttributes()));
        }
        return result;
    }

    public static DataRecord from(DataRecordRO data) {
        SerializableDataRecord result = null;
        if (data != null) {
            result = new SerializableDataRecord(
                data.getSimpleAttributes().size() +
                    data.getCodeAttributes().size() +
                    data.getArrayAttributes().size() +
                    data.getComplexAttributes().size() + 1);
            result.addAll(SimpleAttributeConverter.from(data.getSimpleAttributes()));
            result.addAll(CodeAttributeConverter.from(data.getCodeAttributes()));
            result.addAll(ArrayAttributeConverter.from(data.getArrayAttributes()));
            result.addAll(ComplexAttributeConverter.from(data.getComplexAttributes()));
        }
        return result;
    }

}
