/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 *
 */
package org.unidata.mdm.rest.search.ro;


import java.util.List;

/**
 * @author Mikhail Mikhailov
 * Hit fields container.
 */
public class SearchResultHitFieldRO {

    /**
     * The field name.
     */
    private final String field;
    /**
     * Field value. First value from the search hits array.
     */
    private final Object value;
    private final List<Object> values;

    private List<SearchResultExtendedValueRO> extendedValues = null;
    /**
     * Constructor.
     */
    public SearchResultHitFieldRO(final String field, final Object value, final List<Object> values) {
        super();
        this.field = field;
        this.value = value;
        this.values = values;
    }

    /**
     * @return the field
     */
    public String getField() {
        return field;
    }

    /**
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * Multiple values.
     */ /**
     * @return the values
     */
    public List<Object> getValues() {
        return values;
    }


    public List<SearchResultExtendedValueRO> getExtendedValues() {
        return extendedValues;
    }

    public void setExtendedValues(List<SearchResultExtendedValueRO> extendedValues) {
        this.extendedValues = extendedValues;
    }
}
