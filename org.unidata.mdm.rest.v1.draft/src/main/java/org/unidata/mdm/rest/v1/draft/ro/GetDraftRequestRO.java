package org.unidata.mdm.rest.v1.draft.ro;

/**
 * TODO:
 *
 * @author Alexandr Serov
 * @link http://jira.taskdata.com/browse/TODO:
 * @since 01.12.2020
 **/
public class GetDraftRequestRO {

    private DraftQueryRO query;
    private Integer offset;
    private Integer limit;

    public DraftQueryRO getQuery() {
        return query;
    }

    public void setQuery(DraftQueryRO query) {
        this.query = query;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
