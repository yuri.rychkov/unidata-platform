/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.dao;

import java.util.List;

import org.unidata.mdm.dq.core.po.DataQualityPO;

/**
 * @author Mikhail Mikhailov on Oct 2, 2020
 * DQ model DAO.
 */
public interface DataQualityDAO {
    /**
     * Gets the latest revision of the model.
     * @param storageId the storage id
     * @return revision
     */
    int latest(String storageId);
    /**
     * Loads current version of DQ model set for a storage id.
     * @param storageId the storage id
     * @return DQ model object or null, if there are no saved enumerations.
     */
    DataQualityPO current(String storageId);
    /**
     * Loads current version of the model.
     * @param storageId the storage id
     * @param revision the revision we're interested in
     * @return model object or null, if there are no saved models.
     */
    DataQualityPO previous(String storageId, int revision);
    /**
     * Loads several DQ model versions sorted by revision / create date.
     * @param storageId the storage id
     * @param from start from
     * @param count return count
     * @param withData load 'content' field or not
     * @return collection of DQ model objects
     */
    List<DataQualityPO> load(String storageId, int from, int count, boolean withData);
    /**
     * Saves a new version of DQ model.
     * @param po the PO
     * @return revision put
     */
    int save(DataQualityPO po);
    /**
     * Deletes a DQ model revision.
     * @param storageId the storage id
     * @param revision the revision
     */
    void remove(String storageId, int revision);
}
