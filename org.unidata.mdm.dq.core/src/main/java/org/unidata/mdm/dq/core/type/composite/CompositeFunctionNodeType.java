/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.composite;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Composite CF node types.
 */
public enum CompositeFunctionNodeType {

    @JsonProperty("Constant")
    CONSTANT("Constant"),

    @JsonProperty("IfThenElse")
    IF_THEN_ELSE("IfThenElse"),

    @JsonProperty("OutputTrue")
    OUTPUT_TRUE("OutputTrue"),

    @JsonProperty("OutputFalse")
    OUTPUT_FALSE("OutputFalse"),

    @JsonProperty("Condition")
    CONDITION("Condition"),

    @JsonProperty("Input")
    INPUT("Input"),

    @JsonProperty("Output")
    OUTPUT("Output"),

    @JsonProperty("InputPorts")
    INPUT_PORTS("InputPorts"),

    @JsonProperty("OutputPorts")
    OUTPUT_PORTS("OutputPorts"),

    @JsonProperty("Function")
    FUNCTION("Function"),

    @JsonProperty("Switch")
    SWITCH("Switch");

    private final String value;

    CompositeFunctionNodeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CompositeFunctionNodeType fromValue(String v) {

        for (CompositeFunctionNodeType c: CompositeFunctionNodeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }

        throw new IllegalArgumentException(v);
    }
}
