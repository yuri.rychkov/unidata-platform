/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.core.type.io;


import java.io.Serializable;
import java.util.Objects;

import org.unidata.mdm.dq.core.type.rule.SeverityIndicator;

/**
 * DQ error type. Created from JAXB artifact.
 */
public class DataQualityError implements Serializable {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 3517375849731520977L;
    /**
     * Rule name.
     */
    private final String ruleName;
    /**
     * The function name.
     */
    private final String functionName;
    /**
     * The message.
     */
    private final String message;
    /**
     * The severity.
     */
    private final SeverityIndicator severity;
    /**
     * Yellow score.
     */
    private final int score;
    /**
     * The category.
     */
    private final String category;
    /**
     * Constructor.
     * @param b the builder
     */
    private DataQualityError(DataQualityErrorBuilder b) {
        super();
        this.ruleName = b.ruleName;
        this.functionName = b.functionName;
        this.message = b.message;
        this.severity = b.severity;
        this.score = b.score;
        this.category = b.category;
    }
    /**
     * Gets the value of the ruleName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRuleName() {
        return ruleName;
    }

    /**
     * @return the functionName
     */
    public String getFunctionName() {
        return functionName;
    }
    /**
     * Gets the value of the message property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMessage() {
        return message;
    }

    /**
     * Gets the value of the severity property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public SeverityIndicator getSeverity() {
        return severity;
    }

    /**
     * @return the score
     */
    public int getScore() {
        return score;
    }
    /**
     * Gets the value of the category property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCategory() {
        return category;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {

        return "DQ error: rule name ["
                + ruleName
                + "], function name ["
                + functionName
                + "], severity ["
                + (severity != null ? severity.name() : "<null>")
                + "], yellow score ["
                + score
                + "], category ["
                + category
                + "], message ["
                + message
                + "]";
    }
    /**
     * New builder,
     * @param error the rror to copy
     * @return new builder
     */
    public static DataQualityErrorBuilder builder(DataQualityError error) {
        return new DataQualityErrorBuilder(error);
    }
    /**
     * New builder.
     * @return new builder
     */
    public static DataQualityErrorBuilder builder() {
        return new DataQualityErrorBuilder();
    }
    /**
     * @author Mikhail Mikhailov
     * Builder type.
     */
    public static class DataQualityErrorBuilder {
        /**
         * Rule name.
         */
        private String ruleName;
        /**
         * The function name.
         */
        private String functionName;
        /**
         * The message.
         */
        private String message;
        /**
         * The severity.
         */
        private SeverityIndicator severity;
        /**
         * Yellow score.
         */
        private int score;
        /**
         * The category.
         */
        private String category;
        /**
         * Constructor.
         */
        private DataQualityErrorBuilder() {
            super();
        }
        /**
         * Constructor.
         */
        private DataQualityErrorBuilder(DataQualityError error) {

            this();
            if (Objects.isNull(error)) {
                return;
            }

            ruleName = error.ruleName;
            message = error.message;
            severity= error.severity;
            category = error.category;
        }
        /**
         * Sets rule name.
         * @param value the value to set
         * @return self
         */
        public DataQualityErrorBuilder ruleName(String value) {
            this.ruleName = value;
            return this;
        }
        /**
         * Sets function name.
         * @param value the value to set
         * @return self
         */
        public DataQualityErrorBuilder functionName(String value) {
            this.functionName = value;
            return this;
        }
        /**
         * Sets message.
         * @param value the value to set
         * @return self
         */
        public DataQualityErrorBuilder message(String value) {
            this.message = value;
            return this;
        }
        /**
         * Sets severity.
         * @param value the value to set
         * @return self
         */
        public DataQualityErrorBuilder severity(SeverityIndicator value) {
            this.severity = value;
            return this;
        }
        /**
         * Sets severity.
         * @param value the value to set
         * @return self
         */
        public DataQualityErrorBuilder severity(String value) {
            this.severity = Objects.nonNull(value) ? SeverityIndicator.valueOf(value) : null;
            return this;
        }
        /**
         * Sets {@link SeverityIndicator#YELLOW} score value.
         * @param value the value to set
         * @return self
         */
        public DataQualityErrorBuilder score(int value) {
            this.score = value;
            return this;
        }
        /**
         * Sets category.
         * @param value the value to set
         * @return self
         */
        public DataQualityErrorBuilder category(String value) {
            this.category = value;
            return this;
        }
        /**
         * Builds the object.
         * @return new error object
         */
        public DataQualityError build() {
            return new DataQualityError(this);
        }
    }
}
