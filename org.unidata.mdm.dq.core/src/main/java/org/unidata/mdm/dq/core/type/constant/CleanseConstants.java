/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.core.type.constant;

/**
 * Class contains constants used across basic cleanse functions implementation.
 * @author ilya.bykov
 */
public class CleanseConstants {
    /**
     * Ino instances constructor.
     */
    private CleanseConstants() {
        super();
    }
    /**
     * Generic port 1.
     */
    public static final String PORT_1 = "port1";
    /**
     * Generic port 2.
     */
    public static final String PORT_2 = "port2";
    /**
     * Generic port 3.
     */
    public static final String PORT_3 = "port3";
    /**
     * Generic port 4.
     */
    public static final String PORT_4 = "port4";
    /**
     * Generic port 5.
     */
    public static final String PORT_5 = "port5";
    /**
     * Generic port 6.
     */
    public static final String PORT_6 = "port6";
    /**
     * Generic port 7.
     */
    public static final String PORT_7 = "port7";
    /**
     * Generic port 8.
     */
    public static final String PORT_8 = "port8";
    /**
     * The Constant INPUT_PORT_1.
     */
    public static final String INPUT_PORT_1 = PORT_1;
    /**
     * The Constant INPUT_PORT_2.
     */
    public static final String INPUT_PORT_2 = PORT_2;
    /**
     * The Constant INPUT_PORT_3.
     */
    public static final String INPUT_PORT_3 = PORT_3;
    /**
     * The Constant INPUT_PORT_4.
     */
    public static final String INPUT_PORT_4 = PORT_4;
    /**
     * The Constant INPUT_PORT_5.
     */
    public static final String INPUT_PORT_5 = PORT_5;
    /**
     * The Constant INPUT_PORT_6.
     */
    public static final String INPUT_PORT_6 = PORT_6;
    /**
     * The Constant INPUT_PORT_7.
     */
    public static final String INPUT_PORT_7 = PORT_7;
    /**
     * The Constant INPUT_PORT_8.
     */
    public static final String INPUT_PORT_8 = PORT_8;
    /**
     * The Constant OUTPUT_PORT_1.
     */
    public static final String OUTPUT_PORT_1 = PORT_1;
    /**
     * The Constant OUTPUT_PORT_2.
     */
    public static final String OUTPUT_PORT_2 = PORT_2;
    /**
     * The Constant OUTPUT_PORT_3.
     */
    public static final String OUTPUT_PORT_3 = PORT_3;
    /**
     * The Constant OUTPUT_PORT_4.
     */
    public static final String OUTPUT_PORT_4 = PORT_4;
    /**
     * The Constant OUTPUT_PORT_5.
     */
    public static final String OUTPUT_PORT_5 = PORT_5;
    /**
     * The Constant OUTPUT_PORT_6.
     */
    public static final String OUTPUT_PORT_6 = PORT_6;
    /**
     * The Constant OUTPUT_PORT_7.
     */
    public static final String OUTPUT_PORT_7 = PORT_7;
    /**
     * The Constant OUTPUT_PORT_8.
     */
    public static final String OUTPUT_PORT_8 = PORT_8;
}
