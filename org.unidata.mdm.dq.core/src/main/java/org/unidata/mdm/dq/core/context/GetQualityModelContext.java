/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.dq.core.context;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.core.context.AbstractModelGetContext;
import org.unidata.mdm.core.service.segments.ModelGetStartExecutor;
import org.unidata.mdm.dq.core.configuration.DataQualityModelIds;
import org.unidata.mdm.dq.core.util.DQUtils;
import org.unidata.mdm.system.context.DraftAwareContext;

/**
 * @author Mikhail Mikhailov on Nov 28, 2019
 */
public class GetQualityModelContext extends AbstractModelGetContext implements DraftAwareContext {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -6245429588435518790L;
    /**
     * A possibly set draft id.
     */
    private final Long draftId;
    /**
     * A possibly set parent draft id.
     */
    private final Long parentDraftId;
    /**
     * Requested enumeration ids.
     */
    private final List<String> cleanseFunctionIds;
    /**
     * Requested enumeration ids.
     */
    private final List<String> functionGroupIds;
    /**
     * Requested rule ids.
     */
    private final List<String> qualityRuleIds;
    /**
     * Requested rule mapping set ids.
     */
    private final List<String> mappingSetIds;
    /**
     * Assignments namespace ids.
     */
    private final List<String> assignmentIds;
    /**
     * All enumerations requested.
     */
    private final boolean allFunctionGroups;
    /**
     * Fetch all cleanse functions source.
     */
    private final boolean allCleanseFunctions;
    /**
     * All rules requested.
     */
    private final boolean allQualityRules;
    /**
     * All rule mapping sets requested.
     */
    private final boolean allMappingSets;
    /**
     * All assignments.
     */
    private final boolean allAssignments;
    /**
     * Constructor.
     * @param b the builder.
     */
    private GetQualityModelContext(GetQualityModelContextBuilder b) {
        super(b);
        this.cleanseFunctionIds = Objects.isNull(b.cleanseFunctionIds) ? Collections.emptyList() : b.cleanseFunctionIds;
        this.allCleanseFunctions = b.allCleanseFunctions;
        this.functionGroupIds = Objects.isNull(b.functionGroupIds) ? Collections.emptyList() : b.functionGroupIds;
        this.allFunctionGroups = b.allFunctionGroups;
        this.qualityRuleIds = b.qualityRuleIds;
        this.allQualityRules = b.allQualityRules;
        this.mappingSetIds = b.mappingSetIds;
        this.allMappingSets = b.allMappingSets;
        this.assignmentIds = b.assignmentIds;
        this.allAssignments = b.allAssignments;
        this.draftId = b.draftId;
        this.parentDraftId = b.parentDraftId;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Long getDraftId() {
        return draftId;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Long getParentDraftId() {
        return parentDraftId;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return DataQualityModelIds.DATA_QUALITY;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        // DQ model is singletons per storage
        return DQUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return ModelGetStartExecutor.SEGMENT_ID;
    }
    /**
     * All enumerations requested?
     * @return request state
     */
    public boolean isAllCleansFunctions() {
        return allCleanseFunctions;
    }
    /**
     * @return the enumerationIds
     */
    public List<String> getCleanseFunctionIds() {
        return cleanseFunctionIds;
    }
    /**
     * @return the allFunctionGroups
     */
    public boolean isAllFunctionGroups() {
        return allFunctionGroups;
    }
    /**
     * @return the functionGroupIds
     */
    public List<String> getFunctionGroupIds() {
        return functionGroupIds;
    }
    /**
     * @return the allQualityRules
     */
    public boolean isAllQualityRules() {
        return allQualityRules;
    }
    /**
     * @return the qualityRuleIds
     */
    public List<String> getQualityRuleIds() {
        return qualityRuleIds;
    }
    /**
     * @return the allMappingSets
     */
    public boolean isAllMappingSets() {
        return allMappingSets;
    }
    /**
     * @return the mappingSetIds
     */
    public List<String> getMappingSetIds() {
        return mappingSetIds;
    }
    /**
     * @return the allAssignments
     */
    public boolean isAllAssignments() {
        return allAssignments;
    }
    /**
     * @return the assignmentIds
     */
    public List<String> getAssignmentIds() {
        return assignmentIds;
    }
    /**
     * Gets a builder instance.
     * @return builder instance
     */
    public static GetQualityModelContextBuilder builder() {
        return new GetQualityModelContextBuilder();
    }
    /**
     * The builder for this context.
     * @author Mikhail Mikhailov on Nov 28, 2019
     */
    public static class GetQualityModelContextBuilder extends AbstractModelGetContextBuilder<GetQualityModelContextBuilder> {
        /**
         * Requested function ids.
         */
        private List<String> cleanseFunctionIds;
        /**
         * All functions requested.
         */
        private boolean allCleanseFunctions;
        /**
         * Requested function group ids.
         */
        private List<String> functionGroupIds;
        /**
         * All functions groups requested.
         */
        private boolean allFunctionGroups;
        /**
         * Requested rule ids.
         */
        private List<String> qualityRuleIds;
        /**
         * All rules requested.
         */
        private boolean allQualityRules;
        /**
         * Requested rule set ids.
         */
        private List<String> mappingSetIds;
        /**
         * All rule sets requested.
         */
        private boolean allMappingSets;
        /**
         * Assignments namespace ids.
         */
        private List<String> assignmentIds;
        /**
         * All assignments.
         */
        private boolean allAssignments;
        /**
         * The draft id.
         */
        private Long draftId;
        /**
         * The parent draft id.
         */
        private Long parentDraftId;
        /**
         * Constructor.
         */
        protected GetQualityModelContextBuilder() {
            super();
        }
        /**
         * Sets draft id
         * @param draftId the draft id
         * @return self
         */
        public GetQualityModelContextBuilder draftId(Long draftId) {
            this.draftId = draftId;
            return self();
        }
        /**
         * Sets parent draft id
         * @param parentDraftId the parent draft id
         * @return self
         */
        public GetQualityModelContextBuilder parentDraftId(Long parentDraftId) {
            this.parentDraftId = parentDraftId;
            return self();
        }
        /**
         * @param cleanseFunctionIds the enumerationIds to set
         */
        public GetQualityModelContextBuilder cleanseFunctionIds(List<String> cleanseFunctionIds) {
            this.cleanseFunctionIds = cleanseFunctionIds;
            return self();
        }
        /**
         * @param allCleanseFunctions the allCleanseFunctions to set
         */
        public GetQualityModelContextBuilder allCleanseFunctions(boolean allCleanseFunctions) {
            this.allCleanseFunctions = allCleanseFunctions;
            return self();
        }
        /**
         * @param functionGroupIds the functionGroupIds to set
         */
        public GetQualityModelContextBuilder functionGroupIds(List<String> functionGroupIds) {
            this.functionGroupIds = functionGroupIds;
            return self();
        }
        /**
         * @param allFunctionGroups the allFunctionGroups to set
         */
        public GetQualityModelContextBuilder allFunctionGroups(boolean allFunctionGroups) {
            this.allFunctionGroups = allFunctionGroups;
            return self();
        }
        /**
         * @param qualityRuleIds the qualityRuleIds to set
         */
        public GetQualityModelContextBuilder qualityRuleIds(List<String> qualityRuleIds) {
            this.qualityRuleIds = qualityRuleIds;
            return self();
        }
        /**
         * @param allQualityRules the allQualityRules to set
         */
        public GetQualityModelContextBuilder allQualityRules(boolean allQualityRules) {
            this.allQualityRules = allQualityRules;
            return self();
        }
        /**
         * @param mappingSetIds the ruleSetIds to set
         */
        public GetQualityModelContextBuilder mappingSetIds(List<String> mappingSetIds) {
            this.mappingSetIds = mappingSetIds;
            return self();
        }
        /**
         * @param allMappingSets the allRuleSets to set
         */
        public GetQualityModelContextBuilder allMappingSets(boolean allMappingSets) {
            this.allMappingSets = allMappingSets;
            return self();
        }
        /**
         * @param assignmentIds the assignmentIds to set
         */
        public GetQualityModelContextBuilder assignmentIds(List<String> assignmentIds) {
            this.assignmentIds = assignmentIds;
            return self();
        }
        /**
         * @param allAssignments the allAssignments to set
         */
        public GetQualityModelContextBuilder allAssignments(boolean allAssignments) {
            this.allAssignments = allAssignments;
            return self();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public GetQualityModelContext build() {
            return new GetQualityModelContext(this);
        }
    }
}
