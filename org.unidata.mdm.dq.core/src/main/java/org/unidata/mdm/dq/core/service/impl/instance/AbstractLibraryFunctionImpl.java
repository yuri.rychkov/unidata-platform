/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.service.impl.instance;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.core.type.model.LibrarySourceElement;
import org.unidata.mdm.dq.core.type.cleanse.CleanseFunctionConfiguration.CleansePortConfiguration;
import org.unidata.mdm.dq.core.type.model.source.AbstractCleanseFunctionSource;
import org.unidata.mdm.dq.core.type.model.source.CleanseFunctionPort;

/**
 * @author Mikhail Mikhailov on Feb 16, 2021
 */
public abstract class AbstractLibraryFunctionImpl<X extends AbstractCleanseFunctionSource<X>>
    extends AbstractCleanseFunctionImpl<X> implements LibrarySourceElement {
    /**
     * Constructor.
     * @param acfs
     */
    protected AbstractLibraryFunctionImpl(X acfs) {
        super(acfs);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getVersion() {
        return source.getLibraryVersion();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getLibrary() {
        return source.getLibraryName();
    }
    /*
     * Supports autoconfiguration.
     * @param source the source ports
     * @return collection of ports in source format
     */
    protected Collection<CleanseFunctionPort> portsToPorts(Collection<CleansePortConfiguration> source) {

        if (CollectionUtils.isEmpty(source)) {
            return Collections.emptyList();
        }

        return source.stream()
            .map(src -> new CleanseFunctionPort()
                        .withName(src.getName())
                        .withDescription(src::getDescription)
                        .withDisplayName(src::getDisplayName)
                        .withFilteringMode(src.getFilteringMode())
                        .withInputTypes(src.getInputTypes())
                        .withValueTypes(src.getValueTypes())
                        .withRequired(src.isRequired()))
            .collect(Collectors.toList());
    }
}
