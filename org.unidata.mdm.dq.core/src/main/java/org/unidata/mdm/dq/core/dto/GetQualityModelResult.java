package org.unidata.mdm.dq.core.dto;

import org.unidata.mdm.core.dto.AbstractModelResult;
import org.unidata.mdm.dq.core.configuration.DataQualityModelIds;
import org.unidata.mdm.dq.core.util.DQUtils;
import org.unidata.mdm.draft.type.DraftPayloadResponse;

/**
 * @author Mikhail Mikhailov on Dec 18, 2020
 */
public class GetQualityModelResult extends AbstractModelResult implements DraftPayloadResponse {
    /**
     * Functions.
     */
    private GetFunctionsModelResult functions;
    /**
     * Function groups.
     */
    private GetGroupsModelResult functionGroups;
    /**
     * Rules.
     */
    private GetRulesModelResult rules;
    /**
     * Sets.
     */
    private GetSetsModelResult sets;
    /**
     * Assignments.
     */
    private GetAssignmentsModelResult assignments;
    /**
     * Constructor.
     */
    public GetQualityModelResult() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        return DQUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return DataQualityModelIds.DATA_QUALITY;
    }
    /**
     * @return the functionGroups
     */
    public GetGroupsModelResult getFunctionGroups() {
        return functionGroups;
    }
    /**
     * @param functionGroups the functionGroups to set
     */
    public void setFunctionGroups(GetGroupsModelResult functionGroups) {
        this.functionGroups = functionGroups;
    }
    /**
     * @return the functions
     */
    public GetFunctionsModelResult getFunctions() {
        return functions;
    }
    /**
     * @param functions the functions to set
     */
    public void setFunctions(GetFunctionsModelResult functions) {
        this.functions = functions;
    }
    /**
     * @return the rules
     */
    public GetRulesModelResult getRules() {
        return rules;
    }
    /**
     * @param rules the rules to set
     */
    public void setRules(GetRulesModelResult rules) {
        this.rules = rules;
    }
    /**
     * @return the sets
     */
    public GetSetsModelResult getSets() {
        return sets;
    }
    /**
     * @param sets the sets to set
     */
    public void setSets(GetSetsModelResult sets) {
        this.sets = sets;
    }
    /**
     * @return the assignments
     */
    public GetAssignmentsModelResult getAssignments() {
        return assignments;
    }
    /**
     * @param assignments the assignments to set
     */
    public void setAssignments(GetAssignmentsModelResult assignments) {
        this.assignments = assignments;
    }
}
