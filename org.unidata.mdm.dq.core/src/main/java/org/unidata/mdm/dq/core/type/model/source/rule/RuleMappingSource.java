/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.source.rule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * @author Mikhail Mikhailov on Mar 4, 2021
 */
public class RuleMappingSource implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -6610725287300286254L;

    private String ruleName;

    @JacksonXmlProperty(isAttribute = true)
    private String localPath;

    @JacksonXmlElementWrapper(useWrapping = false, localName = "input")
    private List<InputPortMapping> inputMappings;

    @JacksonXmlElementWrapper(useWrapping = false, localName = "output")
    private List<OutputPortMapping> outputMappings;

    /**
     * Constructor.
     */
    public RuleMappingSource() {
        super();
    }

    /**
     * @return the ruleName
     */
    public String getRuleName() {
        return ruleName;
    }

    /**
     * @param ruleName the ruleName to set
     */
    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    /**
     * Gets the value of the executionContextPath property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLocalPath() {
        return localPath;
    }

    /**
     * Sets the value of the executionContextPath property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLocalPath(String value) {
        this.localPath = value;
    }

    /**
     * Gets the value of the inputMappings property.
     */
    public List<InputPortMapping> getInputMappings() {
        if (inputMappings == null) {
            inputMappings = new ArrayList<>();
        }
        return this.inputMappings;
    }

    /**
     * Gets the value of the outputMappings property.
     */
    public List<OutputPortMapping> getOutputMappings() {
        if (outputMappings == null) {
            outputMappings = new ArrayList<>();
        }
        return this.outputMappings;
    }

    public RuleMappingSource withRuleName(String value) {
        setRuleName(value);
        return this;
    }

    public RuleMappingSource withExecutionPath(String value) {
        setLocalPath(value);
        return this;
    }

    public RuleMappingSource withInputMappings(InputPortMapping... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withInputMappings(Arrays.asList(values));
        }
        return this;
    }

    public RuleMappingSource withInputMappings(Collection<InputPortMapping> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getInputMappings().addAll(values);
        }
        return this;
    }

    public RuleMappingSource withOutputMappings(OutputPortMapping... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            return withOutputMappings(Arrays.asList(values));
        }
        return this;
    }

    public RuleMappingSource withOutputMappings(Collection<OutputPortMapping> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getOutputMappings().addAll(values);
        }
        return this;
    }
}
