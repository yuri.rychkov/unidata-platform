package org.unidata.mdm.dq.core.service.segments;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.dq.core.context.UpsertQualityModelContext;
import org.unidata.mdm.dq.core.module.DataQualityModule;
import org.unidata.mdm.dq.core.service.impl.DataQualityModelComponent;
import org.unidata.mdm.dq.core.type.draft.DataQualityDraftConstants;
import org.unidata.mdm.dq.core.type.model.instance.DataQualityInstance;
import org.unidata.mdm.dq.core.type.model.source.DataQualityModel;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.dto.DraftUpsertResult;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.variables.Variables;

/**
 * @author mikhail
 * @since  13.01.2020
 */
@Component(QualityDraftUpsertFinishExecutor.SEGMENT_ID)
public class QualityDraftUpsertFinishExecutor extends Finish<DraftUpsertContext, DraftUpsertResult> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataQualityModule.MODULE_ID + "[QUALITY_DRAFT_UPSERT_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataQualityModule.MODULE_ID + ".dq.draft.upsert.finish.description";
    /**
     * The DQMC.
     */
    @Autowired
    private DataQualityModelComponent qualityModelComponent;
    /**
     * Constructor.
     */
    public QualityDraftUpsertFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftUpsertResult.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DraftUpsertResult finish(DraftUpsertContext ctx) {

        Draft draft = ctx.currentDraft();
        DraftUpsertResult result = new DraftUpsertResult(true);

        // 1. Set draft to result
        result.setDraft(draft);

        // 2. Take snapshot of current upon draft creation
        if (!draft.isExisting()) {

            String storageId = ctx.hasParameter(DataQualityDraftConstants.STORAGE_ID)
                    ? ctx.getParameter(DataQualityDraftConstants.STORAGE_ID)
                    : SecurityUtils.getCurrentUserStorageId();

            DataQualityInstance i = qualityModelComponent.instance(storageId, null);

            result.setVariables(variables(i));
            result.setEdition(snapshot(i));
        }

        // 3. The following stuff runs only, if some data were supplied
        if (ctx.hasPayload()) {
            result.setEdition(edit(draft, ctx.getPayload()));
        }

        return result;
    }

    protected Edition snapshot(DataQualityInstance i) {

        Edition next = new Edition();
        next.setContent(i.toSource());
        next.setCreateDate(new Date(System.currentTimeMillis()));
        next.setCreatedBy(SecurityUtils.getCurrentUserName());

        return next;
    }

    protected Edition edit(Draft draft, UpsertQualityModelContext input) {

        input.currentDraft(draft);
        DataQualityModel changes = qualityModelComponent.assemble(input);

        Edition next = new Edition();
        next.setContent(changes);
        next.setCreateDate(new Date(System.currentTimeMillis()));
        next.setCreatedBy(SecurityUtils.getCurrentUserName());

        return next;
    }

    protected Variables variables(DataQualityInstance i) {

        return new Variables()
                .add(DataQualityDraftConstants.DRAFT_INITIATOR, SecurityUtils.getCurrentUserName())
                .add(DataQualityDraftConstants.STORAGE_ID, i.getStorageId())
                .add(DataQualityDraftConstants.DRAFT_START_VERSION, i.getVersion());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return DraftUpsertContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
