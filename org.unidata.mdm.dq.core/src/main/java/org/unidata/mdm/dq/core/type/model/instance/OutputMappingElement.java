/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.type.model.instance;

import org.unidata.mdm.core.type.upath.UPath;

/**
 * @author Mikhail Mikhailov on Feb 28, 2021
 * Output mapping.
 */
public interface OutputMappingElement extends PortMappingElement {
    /**
     * Returns true, if this element is an output path element.
     * @return true, if this element is an output path element
     */
    boolean isPath();
    /**
     * Gets compiled output path.
     * @return compiled output path
     */
    UPath getOutputPath();
    /**
     * Returns true, if this element is a constant element.
     * @return true, if this element is a constant element
     */
    boolean isConstant();
    /**
     * Gets constant view for this element.
     * @return constant view for this element
     */
    ConstantValueElement getConstant();
}
