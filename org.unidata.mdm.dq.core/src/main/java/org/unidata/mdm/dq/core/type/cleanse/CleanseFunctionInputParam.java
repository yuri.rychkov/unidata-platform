/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.core.type.cleanse;

import java.util.Collections;
import java.util.List;

import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.upath.UPathIncompletePath;
import org.unidata.mdm.core.type.upath.UPathResult;

/**
 * @author Mikhail Mikhailov
 * Input param type.
 */
public class CleanseFunctionInputParam extends CleanseFunctionParam {
    /**
     * List of incomplete filtering attempts.
     */
    private List<UPathIncompletePath> incomplete;
    /**
     * @return the values
     */
    public List<Attribute> getAttributes() {
        return values;
    }
    /**
     * Creates input param.
     * @param portName the name of the port
     * @param value singleton attribute value
     * @return param
     */
    public static CleanseFunctionInputParam of(String portName, Attribute value) {
        return of(portName, Collections.singletonList(value));
    }
    /**
     * Creates input param.
     * @param portName the name of the port
     * @param values attributes value
     * @return param
     */
    public static CleanseFunctionInputParam of(String portName, List<Attribute> values) {
        return of(portName, values, null);
    }
    /**
     * Creates input param.
     * @param portName the name of the port
     * @param values attributes value
     * @return param
     */
    public static CleanseFunctionInputParam of(String portName, List<Attribute> values, List<UPathIncompletePath> incomplete) {
        CleanseFunctionInputParam param = new CleanseFunctionInputParam(portName, values);
        param.incomplete = incomplete;
        return param;
    }
    /**
     * Creates input param.
     * @param portName the name of the port
     * @param upathResult UPath execution result
     * @return param
     */
    public static CleanseFunctionInputParam of(String portName, UPathResult upathResult) {
        return of(portName, upathResult.getAttributes(), upathResult.getIncomplete());
    }
    /**
     * Constructor.
     * @param portName the name of the port
     * @param values the values to hold
     */
    private CleanseFunctionInputParam(String portName, List<Attribute> values) {
        super(ParamType.INPUT, portName, values);
    }
    /**
     * @return the incomplete
     */
    public List<UPathIncompletePath> getIncomplete() {
        return incomplete == null ? Collections.emptyList() : incomplete;
    }
}
