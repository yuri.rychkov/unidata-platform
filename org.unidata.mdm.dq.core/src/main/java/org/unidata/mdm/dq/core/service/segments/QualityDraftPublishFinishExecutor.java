/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.dq.core.service.segments;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.context.ModelChangeContext.ModelChangeType;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.service.ModelIdentitySupport;
import org.unidata.mdm.dq.core.context.UpsertQualityModelContext;
import org.unidata.mdm.dq.core.module.DataQualityModule;
import org.unidata.mdm.dq.core.type.draft.DataQualityDraftConstants;
import org.unidata.mdm.dq.core.type.model.source.AbstractCleanseFunctionSource;
import org.unidata.mdm.dq.core.type.model.source.DataQualityModel;
import org.unidata.mdm.draft.context.DraftPublishContext;
import org.unidata.mdm.draft.dto.DraftPublishResult;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author mikhail
 * @since  18.12.2019
 */
@Component(QualityDraftPublishFinishExecutor.SEGMENT_ID)
public class QualityDraftPublishFinishExecutor extends Finish<DraftPublishContext, DraftPublishResult> implements ModelIdentitySupport {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataQualityModule.MODULE_ID + "[QUALITY_DRAFT_PUBLISH_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataQualityModule.MODULE_ID + ".dq.draft.publish.finish.description";
    /**
     * MMS instance.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * Constructor.
     */
    public QualityDraftPublishFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftPublishResult.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DraftPublishResult finish(DraftPublishContext ctx) {
        DraftPublishResult result = new DraftPublishResult(publish(ctx));
        result.setDraft(ctx.currentDraft());
        return result;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return DraftPublishContext.class.isAssignableFrom(start.getInputTypeClass());
    }

    protected boolean publish(DraftPublishContext ctx) {

        Draft draft = ctx.currentDraft();
        Edition edition = ctx.currentEdition();

        // If no data created by the draft just say it's ok
        if (Objects.nonNull(edition) && Objects.nonNull(edition.getContent())) {

            String storageId = draft.getVariables().valueGet(DataQualityDraftConstants.STORAGE_ID);
            Integer version = draft.getVariables().valueGet(DataQualityDraftConstants.DRAFT_START_VERSION);

            DataQualityModel result = edition.getContent();
            List<AbstractCleanseFunctionSource<?>> functions = new ArrayList<>(
                    + result.getCompositeFunctions().size()
                    + result.getJavaFunctions().size()
                    + result.getGroovyFunctions().size()
                    + result.getPythonFunctions().size());

            functions.addAll(result.getCompositeFunctions());
            functions.addAll(result.getJavaFunctions());
            functions.addAll(result.getGroovyFunctions());
            functions.addAll(result.getPythonFunctions());

            UpsertQualityModelContext uCtx = UpsertQualityModelContext.builder()
                .description(result.getDescription())
                .displayName(result.getDisplayName())
                .name(result.getName())
                .functionsUpdate(functions)
                .groupsUpdate(result.getFunctionGroup())
                .rulesUpdate(result.getRules())
                .setsUpdate(result.getSets())
                .assignmentsUpdate(result.getAssignments())
                .upsertType(ModelChangeType.FULL)
                .storageId(storageId)
                .version(version + 1)
                .force(ctx.isForce())
                .waitForFinish(true)
                .postponeRefresh(true)
                .build();

            metaModelService.upsert(uCtx);

            ctx.afterTransaction(uCtx.afterTransaction());
        }

        return true;
    }
}
