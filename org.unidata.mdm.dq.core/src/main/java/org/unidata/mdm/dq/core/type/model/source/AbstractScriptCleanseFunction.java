package org.unidata.mdm.dq.core.type.model.source;

/**
 * @author Mikhail Mikhailov on Jan 23, 2021
 */
public abstract class AbstractScriptCleanseFunction<X extends AbstractScriptCleanseFunction<X>> extends AbstractCleanseFunctionSource<X> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 3210965310306889426L;
    /**
     * Version is set by the user.
     */
    private String version;

    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    public X withVersion(String version) {
        setVersion(version);
        return self();
    }
}
