/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.core.service.impl.instance;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.type.model.instance.AbstractNamedDisplayableImpl;
import org.unidata.mdm.dq.core.type.model.instance.CleanseFunctionElement;
import org.unidata.mdm.dq.core.type.model.instance.DataQualityInstance;
import org.unidata.mdm.dq.core.type.model.instance.FunctionGroupElement;
import org.unidata.mdm.dq.core.type.model.source.CleanseFunctionGroup;

/**
 * @author Mikhail Mikhailov on Jan 28, 2021
 */
public class FunctionGroupImpl extends AbstractNamedDisplayableImpl implements FunctionGroupElement {
    /**
     * The group source.
     */
    private final CleanseFunctionGroup source;
    /**
     * Children groups.
     */
    private final List<FunctionGroupElement> children;
    /**
     * Cleans functions, contained by this group.
     */
    private final List<CleanseFunctionElement> functions = new ArrayList<>();
    /**
     * This group unique path.
     */
    private final String path;
    /**
     * Constructor.
     */
    FunctionGroupImpl(String path, CleanseFunctionGroup group, DataQualityInstance dqi) {
        super(group.getName(), group.getDisplayName(), group.getDescription());
        this.source = group;
        this.path = StringUtils.isBlank(path) ? group.getName() : path + '.' + group.getName();
        this.children = group.getGroups().stream()
            .map(v -> new FunctionGroupImpl(this.path, v, dqi))
            .collect(Collectors.toCollection(ArrayList<FunctionGroupElement>::new));

    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getPath() {
        return path;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public OffsetDateTime getCreateDate() {
        return source.getCreateDate();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getCreatedBy() {
        return source.getCreatedBy();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public OffsetDateTime getUpdateDate() {
        return source.getUpdateDate();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getUpdatedBy() {
        return source.getUpdatedBy();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<CleanseFunctionElement> getFunctions() {
        return functions;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<FunctionGroupElement> getChildren() {
        return children;
    }

    public CleanseFunctionGroup getSource() {
        return source;
    }

    void addFunction(CleanseFunctionElement cfe) {
        functions.add(cfe);
    }
}
