/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.context;

import org.unidata.mdm.system.context.DraftAwareContext;
import org.unidata.mdm.system.context.SetupAwareContext;

/**
 * @author Alexander Malyshev
 */
public class DraftGetContext extends AbstractDraftDataContext implements SetupAwareContext {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 6416674923103891012L;
    /**
     * Load edition objects for this draft.
     */
    private final boolean withEditions;
    /**
     * Load current data view of the draft object.
     */
    private final boolean withData;
    /**
     * The payload to process by the callee.
     */
    private final transient DraftAwareContext payload;
    /**
     * Constructor.
     * @param b the builder
     */
    private DraftGetContext(final DraftGetContextBuilder b) {
        super(b);
        this.withEditions = b.withEditions;
        this.withData = b.withData;
        this.payload = b.payload;
    }
    /**
     * @return the payload
     */
    @SuppressWarnings("unchecked")
    public<T extends DraftAwareContext> T getPayload() {
        return (T) payload;
    }
    /**
     * @return the withEditions
     */
    public boolean withEditions() {
        return withEditions;
    }
    /**
     * @return the withData
     */
    public boolean withData() {
        return withData;
    }
    /**
     * Builder method.
     * @return builder instance
     */
    public static DraftGetContextBuilder builder() {
        return new DraftGetContextBuilder();
    }
    /**
     * @author Mikhail Mikhailov on Sep 16, 2020
     * Builder class.
     */
    public static class DraftGetContextBuilder extends AbstractDraftBaseContextBuilder<DraftGetContextBuilder> {
        /**
         * Load edition objects for this draft.
         */
        private boolean withEditions;
        /**
         * Load current data view of the draft object.
         */
        private boolean withData;
        /**
         * The payload to save.
         */
        private DraftAwareContext payload;
        /**
         * Constructor.
         */
        private DraftGetContextBuilder() {
            super();
        }
        /**
         * Load data or not.
         * @param withData load data or not
         * @return self
         */
        public DraftGetContextBuilder withData(boolean withData) {
            this.withData = withData;
            return self();
        }
        /**
         * Load editions or not.
         * @param withEditions load editions or not
         * @return self
         */
        public DraftGetContextBuilder withEditions(boolean withEditions) {
            this.withEditions = withEditions;
            return self();
        }
        /**
         * Sets payload.
         * @param payload the payload
         * @return self
         */
        public<T extends DraftAwareContext> DraftGetContextBuilder payload(T payload) {
            this.payload = payload;
            return self();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public DraftGetContext build() {
            return new DraftGetContext(this);
        }
    }
}
