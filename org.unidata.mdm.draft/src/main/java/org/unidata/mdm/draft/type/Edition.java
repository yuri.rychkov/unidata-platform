package org.unidata.mdm.draft.type;

import java.util.Date;

/**
 * @author Mikhail Mikhailov on Sep 15, 2020
 * Draft revision with raw payload.
 */
public class Edition {
    /**
     * The draft ID.
     */
    private long draftId;
    /**
     * Revision.
     */
    private int revision;
    /**
     * Data.
     */
    private Object content;
    /**
     * Creator's user name.
     */
    private String createdBy;
    /**
     * Create date.
     */
    private Date createDate;
    /**
     * Constructor.
     */
    public Edition() {
        super();
    }
    /**
     * @return the draftId
     */
    public long getDraftId() {
        return draftId;
    }
    /**
     * @param draftId the draftId to set
     */
    public void setDraftId(long draftId) {
        this.draftId = draftId;
    }
    /**
     * @return the revision
     */
    public int getRevision() {
        return revision;
    }
    /**
     * @param revision the revision to set
     */
    public void setRevision(int revision) {
        this.revision = revision;
    }
    /**
     * @return the content
     */
    @SuppressWarnings("unchecked")
    public<X> X getContent() {
        return (X) content;
    }
    /**
     * @param content the content to set
     */
    public<X> void setContent(X data) {
        this.content = data;
    }
    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }
    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }
    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
