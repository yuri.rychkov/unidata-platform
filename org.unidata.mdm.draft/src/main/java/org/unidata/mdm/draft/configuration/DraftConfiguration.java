/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.unidata.mdm.draft.module.DraftModule;
import org.unidata.mdm.draft.service.impl.job.CleanUnusedDraftsJob;
import org.unidata.mdm.system.configuration.AbstractConfiguration;
import org.unidata.mdm.system.util.DataSourceUtils;

/**
 * @author Alexander Malyshev
 */
@Configuration
@EnableTransactionManagement
public class DraftConfiguration extends AbstractConfiguration {

    private static final ConfigurationId ID = () -> DraftModule.MODULE_ID;

    @Override
    protected ConfigurationId getId() {
        return ID;
    }

    @Bean(name = "draftDataSource")
    public DataSource draftDataSource() {
        Properties properties = getAllPropertiesWithPrefix(DraftConfigurationConstants.PROPERTY_DRAFT_DATASOURCE_PREFIX, true);
        return DataSourceUtils.newPoolingXADataSource(properties);
    }

    @Bean("drafts-sql")
    public PropertiesFactoryBean draftsSql() {
        final PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/db/drafts-sql.xml"));
        return propertiesFactoryBean;
    }

    // System jobs
    /**
     * CUB job details.
     * @return CUB job details
     */
    @Bean
    public JobDetailFactoryBean cleanUnusedDraftsJobDetail() {

        JobDetailFactoryBean jdfb = new JobDetailFactoryBean();

        jdfb.setJobClass(CleanUnusedDraftsJob.class);
        jdfb.setName(DraftConfigurationConstants.DRAFT_JOB_CLEAN_UNUSED_DRAFTS);
        jdfb.setGroup(DraftConfigurationConstants.DRAFT_JOB_GROUP_NAME);
        jdfb.setDurability(true);

        return jdfb;
    }
    /**
     * CUB job trigger.
     * @return CUB job trigger
     */
    @Bean
    public CronTriggerFactoryBean cleanUnusedDraftsJobTrigger(
            JobDetail cleanUnusedDraftsJobDetail,
            @Value("${" + DraftConfigurationConstants.PROPERTY_JOB_CLEAN_DRAFTS_CRONEX + ":0 0 0/1 * * ?}") String cronExpression) {

        CronTriggerFactoryBean ctfb = new CronTriggerFactoryBean();

        ctfb.setJobDetail(cleanUnusedDraftsJobDetail);
        ctfb.setGroup(DraftConfigurationConstants.DRAFT_JOB_GROUP_NAME);
        ctfb.setCronExpression(cronExpression);

        return ctfb;
    }
}
