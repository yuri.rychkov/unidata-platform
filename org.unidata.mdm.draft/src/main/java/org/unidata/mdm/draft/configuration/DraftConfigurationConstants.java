package org.unidata.mdm.draft.configuration;

import org.unidata.mdm.draft.module.DraftModule;

/**
 * @author Mikhail Mikhailov on Sep 18, 2020
 * This module constants.
 */
public final class DraftConfigurationConstants {
    /**
     * Core data source properties prefix. A non - property, not managed.
     */
    public static final String PROPERTY_DRAFT_DATASOURCE_PREFIX = DraftModule.MODULE_ID + ".datasource.";
    /**
     * This module schema name.
     */
    public static final String DRAFT_SCHEMA_NAME = "org_unidata_mdm_draft";
    /**
     * This module log name.
     */
    public static final String DRAFT_LOG_NAME = "change_log";
    /**
     * Draft id locking map.
     */
    public static final String DRAFT_ID_LOCK_MAP_NAME = "draft-id-lock-map";
    // Properties:
    /**
     * System jobs group.
     */
    public static final String PROPERTY_SYSTEM_JOBS_GROUP = DraftModule.MODULE_ID + ".group.system.jobs";
    /**
     * Clean unused drafts job 'disabled' state.
     */
    public static final String PROPERTY_JOB_CLEAN_DRAFTS_DISABLED = DraftModule.MODULE_ID + ".job.clean.drafts.disabled";
    /**
     * Clean drafts lifetime inteval.
     */
    public static final String PROPERTY_JOB_CLEAN_DRAFTS_LIFETIME = DraftModule.MODULE_ID + ".job.clean.drafts.lifetime";
    /**
     * Clean drafts cron expression. Read only.
     */
    public static final String PROPERTY_JOB_CLEAN_DRAFTS_CRONEX = DraftModule.MODULE_ID + ".job.clean.drafts.cronex";
    /**
     * General settings group.
     */
    public static final String PROPERTY_GENERAL_SETTINGS_GROUP = DraftModule.MODULE_ID + ".group.general.settings";
    /**
     * Clean drafts cron expression. Read only.
     */
    public static final String PROPERTY_ID_LOCK_ACQUISITION_TIMEOUT = DraftModule.MODULE_ID + ".id.lock.acquisition.timeout";
    // System jobs:
    /**
     * Draft system jobs group name.
     */
    public static final String DRAFT_JOB_GROUP_NAME = "draft-job-group";
    /**
     * Draft system job - Clean Unused drafts.
     */
    public static final String DRAFT_JOB_CLEAN_UNUSED_DRAFTS = "draft-job-clean-unused-drafts";
    /**
     * Constructor.
     */
    private DraftConfigurationConstants() {
        super();
    }
}
