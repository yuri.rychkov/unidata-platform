/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.exception;

import org.unidata.mdm.system.exception.ExceptionId;

/**
 * @author Alexander Malyshev
 */
public final class DraftExceptionIds {
    /**
     * Constructor.
     */
    private DraftExceptionIds() { }
    /**
     * Reflection failed.
     */
    public static final ExceptionId EX_DRAFT_CURRENT_USER_NAME_METHOD =
            new ExceptionId("EX_DRAFT_CURRENT_USER_NAME_METHOD", "app.draft.current.user.name.method");
    /**
     * Draft object not found by id.
     */
    public static final ExceptionId EX_DRAFT_NOT_FOUND_BY_ID =
            new ExceptionId("EX_DRAFT_NOT_FOUND_BY_ID", "app.draft.not.found.by.id");
    /**
     * Draft provider not found by id.
     */
    public static final ExceptionId EX_DRAFT_PROVIDER_NOT_FOUND_BY_ID =
            new ExceptionId("EX_DRAFT_PROVIDER_NOT_FOUND_BY_ID", "app.draft.provider.not.found.by.id");
    /**
     * Provider field is not set.
     */
    public static final ExceptionId EX_DRAFT_EMPTY_PROVIDER =
            new ExceptionId("EX_DRAFT_EMPTY_PROVIDER", "app.draft.empty.provider");
    /**
     * Draft ID field is not set.
     */
    public static final ExceptionId EX_DRAFT_EMPTY_DRAFT_ID =
            new ExceptionId("EX_DRAFT_EMPTY_DRAFT_ID", "app.draft.empty.draft.id");
    /**
     * Draft ID lock timeout.
     */
    public static final ExceptionId EX_DRAFT_ID_LOCK_TIMEOUT =
            new ExceptionId("EX_DRAFT_ID_LOCK_TIMEOUT", "ex.draft.id.lock.timeout");
}
