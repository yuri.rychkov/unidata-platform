package org.unidata.mdm.draft.dto;

import java.util.Objects;

import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.system.dto.AbstractCompositeResult;
import org.unidata.mdm.system.type.pipeline.PipelineOutput;

/**
 * @author Mikhail Mikhailov on Sep 17, 2020
 */
public class DraftPublishResult extends AbstractCompositeResult implements PipelineOutput {
    /**
     * The draft.
     */
    private Draft draft;
    /**
     * This is used to update draft records,
     * which will not be removed and initially were not associated with a subject,
     * but will have a subject after successful publishing.
     */
    private String subjectId;
    /**
     * Operation success mark.
     */
    private boolean success;
    /**
     * Don't publish children tree further after this publication succeseeds.
     */
    private boolean stop;
    /**
     * Constructor.
     */
    public DraftPublishResult(boolean success) {
        super();
        this.success = success;
    }
    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }
    /**
     * @param success the success to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }
    /**
     * Gets the draft object.
     * @return the draft
     */
    public Draft getDraft() {
        return draft;
    }
    /**
     * Sets the draft object.
     * @param draft the draft to set
     */
    public void setDraft(Draft draft) {
        this.draft = draft;
    }
    /**
     * Gets a new subject id.
     * @return new subject id
     */
    public String getSubjectId() {
        return subjectId;
    }
    /**
     * Sets a new subject id to be saved
     * @param subjectId the subject id to set
     */
    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }
    /**
     * Has draft set.
     * @return true if set, false otherwise
     */
    public boolean hasDraft() {
        return Objects.nonNull(draft);
    }
    /**
     * Don't publish children tree further after this publication succeseeds, if this method returns true.
     * @return the stop
     */
    public boolean isStop() {
        return stop;
    }
    /**
     * Don't publish children tree further after this publication succeseeds.
     * @param stop the stop to set
     */
    public void setStop(boolean stop) {
        this.stop = stop;
    }
}
