package org.unidata.mdm.draft.dao;

import java.util.List;

import javax.annotation.Nullable;

import org.unidata.mdm.draft.po.DraftPO;
import org.unidata.mdm.draft.po.EditionPO;

/**
 * @author Mikhail Mikhailov on Sep 9, 2020
 * The drafts DAO interface.
 */
public interface DraftsDAO {
    /**
     * Count drafts for the given provider ID and subject.
     * @param parentDraftId the parent draft id
     * @param providerId the provider ID
     * @param subjectId the draft subject ID
     * @param owner draft owner user name
     * @param tags the tags for filtering
     * @return count of draft objects
     */
    long countDrafts(Long parentDraftId, String providerId, String subjectId, String owner, String[] tags);
    /**
     * Loads drafts collection for the given provider ID and owner.
     * @param parentDraftId the parent draft id
     * @param providerId the provider ID
     * @param subjectId the draft subject ID
     * @param owner draft owner user name
     * @param tags the tags for filtering
     * @param limit the limit
     * @param offset the offset
     * @return collection of draft objects
     */
    List<DraftPO> loadDrafts(Long parentDraftId, String providerId, String subjectId, String owner, String[] tags, Integer limit, Integer offset);
    /**
     * Loads a draft object by id.
     * @param id the draft id
     * @return draft object or null
     */
    @Nullable
    DraftPO loadDraft(long id);
    /**
     * Loads "current" (the youngest) subject by draft id.
     * @param id the draft id
     * @param withData load data bytes
     * @return subject or null
     */
    @Nullable
    EditionPO loadCurrentEdition(long id, boolean withData);
    /**
     * Loads all subjects by draft id.
     * @param id the draft id
     * @param withData load data bytes
     * @return subject collection
     */
    @Nullable
    List<EditionPO> loadEditions(long id, boolean withData);
    /**
     * Puts a new draft object to DB.
     * @param draft the draft object
     * @return new draft id
     */
    long putDraft(DraftPO draft);
    /**
     * Puts a new subject to DB.
     * @param subject the subject
     * @return new subject revision number
     */
    int putEdition(EditionPO subject);
    /**
     * Updates properties for an existing draft id.
     * @param draft fresh properties
     */
    void putDraftProperties(DraftPO draft);
    /**
     * Updates subject id for given draft id.
     * @param draftId the draft id
     * @param subjectId the new subject id
     */
    void putSubject(long draftId, String subjectId);
    /**
     * Wipes a draft object by id.
     * @param draftId the draft id
     * @return count of wiped objects
     */
    int wipeDraft(long draftId);
    /**
     * Wipes all drafts by provider ID, owner user name and subject ID.
     * @param parentDraftId the parent draft ID
     * @param providerId the provider ID
     * @param subjectId the subject id
     * @param owner the owner user name
     * @param tags the tags for filtering
     * @return count of wiped objects
     */
    int wipeDrafts(Long parentDraftId, String providerId, String subjectId, String owner, String[] tags);
    /**
     * Cleans drafts without subject id, that live longer than given max lifetime.
     * @param maxLifetime maximum life time
     * @return number of deleted drafts
     */
    long cleanUnusedDrafts(long maxLifetime);
}
