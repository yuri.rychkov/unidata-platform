/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.configuration;

import org.unidata.mdm.draft.module.DraftModule;
import org.unidata.mdm.system.type.configuration.ConfigurationProperty;

/**
 * @author Mikhail Mikhailov on May 26, 2021
 */
public final class DraftConfigurationProperty {
    /**
     * Constructor.
     */
    private DraftConfigurationProperty() {
        super();
    }
    /**
     * Job is disabled.
     */
    public static final ConfigurationProperty<Boolean> JOB_CLEAN_DRAFTS_DISABLED = ConfigurationProperty.bool()
            .key(DraftConfigurationConstants.PROPERTY_JOB_CLEAN_DRAFTS_DISABLED)
            .groupKey(DraftConfigurationConstants.PROPERTY_SYSTEM_JOBS_GROUP)
            .moduleId(DraftModule.MODULE_ID)
            .defaultValue(Boolean.FALSE)
            .required(true)
            .readOnly(false)
            .build();
    /**
     * Lifetime duration of a draft without subject id, before it gets collected.
     */
    public static final ConfigurationProperty<Long> JOB_CLEAN_DRAFTS_LIFETIME = ConfigurationProperty.integer()
            .key(DraftConfigurationConstants.PROPERTY_JOB_CLEAN_DRAFTS_LIFETIME)
            .groupKey(DraftConfigurationConstants.PROPERTY_SYSTEM_JOBS_GROUP)
            .moduleId(DraftModule.MODULE_ID)
            .defaultValue(Long.valueOf(10080L))
            .required(true)
            .readOnly(false)
            .build();
    /**
     * Job startup CRON expression.
     */
    public static final ConfigurationProperty<String> JOB_CLEAN_DRAFTS_CRONEX = ConfigurationProperty.string()
            .key(DraftConfigurationConstants.PROPERTY_JOB_CLEAN_DRAFTS_CRONEX)
            .groupKey(DraftConfigurationConstants.PROPERTY_SYSTEM_JOBS_GROUP)
            .moduleId(DraftModule.MODULE_ID)
            .defaultValue("0 0 0/1 * * ?")
            .required(true)
            .readOnly(true)
            .build();
    /**
     * Draft id lock acquisition timeout.
     */
    public static final ConfigurationProperty<Long> DRAFT_ID_LOCK_ACQUISITION_TIMEOUT = ConfigurationProperty.integer()
        .key(DraftConfigurationConstants.PROPERTY_ID_LOCK_ACQUISITION_TIMEOUT)
        .groupKey(DraftConfigurationConstants.PROPERTY_GENERAL_SETTINGS_GROUP)
        .defaultValue(Long.valueOf(3000L))
        .moduleId(DraftModule.MODULE_ID)
        .readOnly(false)
        .required(false)
        .build();
    /**
     * Values as array.
     */
    private static final ConfigurationProperty<?>[] VALUES = {
            JOB_CLEAN_DRAFTS_DISABLED,
            JOB_CLEAN_DRAFTS_LIFETIME,
            JOB_CLEAN_DRAFTS_CRONEX,
            DRAFT_ID_LOCK_ACQUISITION_TIMEOUT
    };
    /**
     * Enum like array accessor.
     * @return array of values.
     */
    public static ConfigurationProperty<?>[] values() {
        return VALUES;
    }
}
