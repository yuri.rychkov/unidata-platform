/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.context;

import java.util.Objects;

import org.unidata.mdm.draft.type.DraftPublicationOrder;
import org.unidata.mdm.system.context.SetupAwareContext;

/**
 * @author Alexander Malyshev
 */
public class DraftUpsertContext extends AbstractDraftFieldsContext implements SetupAwareContext {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 8533452394991174449L;
    /**
     * General purpose description.
     */
    private final String description;
    /**
     * The payload to save.
     */
    private final transient Object payload;
    /**
     * Publication order.
     */
    private final DraftPublicationOrder publicationOrder;
    /**
     * Constructor.
     * @param b the builder
     */
    public DraftUpsertContext(final DraftUpsertContextBuilder b) {
        super(b);
        this.description = b.description;
        this.payload = b.payload;
        this.publicationOrder = b.publicationOrder;
    }
    /**
     * Gets a builder instance
     * @return builder instance
     */
    public static DraftUpsertContextBuilder builder() {
        return new DraftUpsertContextBuilder();
    }
    /**
     * Gets a general purpose draft description.
     * @return description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @return the payload
     */
    @SuppressWarnings("unchecked")
    public<T> T getPayload() {
        return (T) payload;
    }
    /**
     * Returns true, if some payload is set.
     * @return true, if set
     */
    public boolean hasPayload() {
        return Objects.nonNull(payload);
    }
    /**
     * Returns publication order.
     * @return the publicationOrder
     */
    public DraftPublicationOrder getPublicationOrder() {
        return publicationOrder;
    }
    /**
     * @author Mikhail Mikhailov on Sep 13, 2020
     * Simple builder.
     */
    public static class DraftUpsertContextBuilder extends AbstractDraftFieldsContextBuilder<DraftUpsertContextBuilder> {
        /**
         * The payload to save.
         */
        private Object payload;
        /**
         * Description to save/update.
         */
        private String description;
        /**
         * Publication order.
         */
        private DraftPublicationOrder publicationOrder;
        /**
         * Sets decsription.
         * @param description the description
         * @return self
         */
        public DraftUpsertContextBuilder description(String description) {
            this.description = description;
            return self();
        }
        /**
         * Sets payload.
         * @param payload the payload
         * @return self
         */
        public<T> DraftUpsertContextBuilder payload(T payload) {
            this.payload = payload;
            return self();
        }
        /**
         * Sets publication order.
         * @param publicationOrder the publication order
         * @return self
         */
        public DraftUpsertContextBuilder publicationOrder(DraftPublicationOrder publicationOrder) {
            this.publicationOrder = publicationOrder;
            return self();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public DraftUpsertContext build() {
            return new DraftUpsertContext(this);
        }
    }
}
