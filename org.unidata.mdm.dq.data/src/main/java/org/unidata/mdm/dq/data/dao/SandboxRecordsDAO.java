/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.data.dao;

import java.util.List;

import org.unidata.mdm.dq.data.dao.po.SandboxRecordPO;
import org.unidata.mdm.system.dao.BaseDAO;

public interface SandboxRecordsDAO extends BaseDAO {

    long save(SandboxRecordPO sandboxRecord);

    SandboxRecordPO findRecordById(long recordId);

    void deleteByIds(List<Long> recordsIds);

    void deleteByEntityName(String entityName);

    List<SandboxRecordPO> find(String entity, int page, int count);

    List<SandboxRecordPO> findByRecordsIds(List<Long> collect);

    long count(final String entityName);
}
