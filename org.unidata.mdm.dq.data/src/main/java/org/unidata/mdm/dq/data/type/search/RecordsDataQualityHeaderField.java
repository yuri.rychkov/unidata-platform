/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.dq.data.type.search;

import org.unidata.mdm.search.type.FieldType;
import org.unidata.mdm.search.type.IndexField;

/**
 * Data Quality records fields (implemented as nested records).
 */
public enum RecordsDataQualityHeaderField implements IndexField {
    /**
     * List of data quality validation errors.
     */
    FIELD_QUALITY_ERRORS("$quality_errors", FieldType.COMPOSITE),
    /**
     * The name of the rule set.
     */
    FIELD_SET_NAME("$set_name", FieldType.STRING),
    /**
     * The display name of the rule set.
     */
    FIELD_SET_DISPLAY_NAME("$set_display_name", FieldType.STRING) {
        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isAnalyzed() {
            return true;
        }
    },
    /**
     * The name of the rule.
     */
    FIELD_RULE_NAME("$rule_name", FieldType.STRING),
    /**
     * The display name of the rule.
     */
    FIELD_RULE_DISPLAY_NAME("$rule_display_name", FieldType.STRING) {
        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isAnalyzed() {
            return true;
        }
    },
    /**
     * The name of the function.
     */
    FIELD_FUNCTION_NAME("$function_name", FieldType.STRING),
    /**
     * The error message(s) from all errors,
     * indexed analyzed for searches by error content.
     */
    FIELD_MESSAGE("$message", FieldType.STRING) {
        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isAnalyzed() {
            return true;
        }
    },
    /**
     * Severity indicator(s) from all errors.
     */
    FIELD_SEVERITY("$severity", FieldType.STRING),
    /**
     * Individual DQRE score.
     */
    FIELD_SCORE("$score", FieldType.INTEGER),
    /**
     * Overall score (sum of all scores from all errors).
     */
    FIELD_TOTAL_SCORE("$total_score", FieldType.INTEGER),
    /**
     * Category/is from all errors.
     */
    FIELD_CATEGORY("$category", FieldType.STRING),
    /**
     * NOT indexed original spot(s) content.
     */
    FIELD_SPOT("$spot", FieldType.STRING),
    /**
     * NOT indexed original error(s) message(es) content.
     */
    FIELD_STORE("$store", FieldType.STRING);
    /**
     * The field name.
     */
    private String field;
    /**
     * The type.
     */
    private final FieldType type;
    /**
     * Constructor.
     * @param field the field name
     * @param type field type
     */
    RecordsDataQualityHeaderField(String field, FieldType type) {
        this.field = field;
        this.type = type;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return field;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getPath() {
        return this == FIELD_QUALITY_ERRORS ? getName() : FIELD_QUALITY_ERRORS.getPath() + "." + getName();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public FieldType getFieldType() {
        return type;
    }
}
