/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.dq.data.context;

import java.util.Objects;

import org.unidata.mdm.data.context.AbstractRecordIdentityContext;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.dq.core.type.model.instance.NamespaceAssignmentElement;
import org.unidata.mdm.dq.data.service.segments.records.upsert.RecordUpsertQualityStartExecutor;
import org.unidata.mdm.system.context.CommonRequestContext;

/**
 * @author Mikhail Mikhailov on Apr 9, 2021
 */
public class RecordQualityContext extends CommonRequestContext implements DataQualityCalculationContext<OriginRecord> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 7313559019906240513L;
    /**
     * Assignment.
     */
    private final NamespaceAssignmentElement assignment;
    /**
     * The acrual context, being processed.
     */
    private final AbstractRecordIdentityContext payload;
    /**
     * Constructor.
     * @param b
     */
    private RecordQualityContext(RecordQualityContextBuilder b) {
        super(b);
        this.assignment = b.assignment;
        this.payload = b.payload;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return RecordUpsertQualityStartExecutor.SEGMENT_ID;
    }
    /**
     * Returns true, if the context contains assignment.
     * @return true, if the context contains assignment
     */
    public boolean hasAssignment() {
        return Objects.nonNull(assignment);
    }
    /**
     * @return the assignment
     */
    public NamespaceAssignmentElement getAssignment() {
        return assignment;
    }
    /**
     * @return the payload
     */
    @SuppressWarnings("unchecked")
    public<X extends AbstractRecordIdentityContext> X getPayload() {
        return (X) payload;
    }
    /**
     * Builder shorthand.
     * @return builder
     */
    public static RecordQualityContextBuilder builder() {
        return new RecordQualityContextBuilder();
    }
    /**
     * @author Mikhail Mikhailov
     * Builder for the context.
     */
    public static class RecordQualityContextBuilder extends CommonRequestContextBuilder<RecordQualityContextBuilder> {
        /**
         * Assignment.
         */
        private NamespaceAssignmentElement assignment;
        /**
         * The acrual context, being processed.
         */
        private AbstractRecordIdentityContext payload;
        /**
         * Sets assignment.
         * @param assignment the assignment
         * @return self
         */
        public RecordQualityContextBuilder assignment(NamespaceAssignmentElement assignment) {
            this.assignment = assignment;
            return this;
        }
        /**
         * Sets assignment.
         * @param assignment the assignment
         * @return self
         */
        public<X extends AbstractRecordIdentityContext> RecordQualityContextBuilder payload(X payload) {
            this.payload = payload;
            return this;
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public RecordQualityContext build() {
            return new RecordQualityContext(this);
        }
    }
}
