/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.core.service;

import static org.unidata.mdm.rest.core.converter.UsersConverter.convertUserDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.unidata.mdm.core.dto.UserWithPasswordDTO;
import org.unidata.mdm.core.service.RoleService;
import org.unidata.mdm.core.service.SecurityService;
import org.unidata.mdm.core.service.UserService;
import org.unidata.mdm.core.type.security.AuthenticationSystemParameter;
import org.unidata.mdm.core.type.security.EndpointType;
import org.unidata.mdm.core.type.security.SecurityToken;
import org.unidata.mdm.core.type.security.User;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.rest.core.converter.RoleRoConverter;
import org.unidata.mdm.rest.core.converter.UsersConverter;
import org.unidata.mdm.rest.core.exception.CoreRestExceptionIds;
import org.unidata.mdm.rest.core.ro.LoginRequest;
import org.unidata.mdm.rest.core.ro.LoginResponse;
import org.unidata.mdm.rest.core.ro.SetPasswordRequest;
import org.unidata.mdm.rest.core.ro.UserRO;
import org.unidata.mdm.rest.system.ro.ErrorInfo;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.rest.system.ro.UpdateResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.exception.PlatformBusinessException;
import org.unidata.mdm.system.type.runtime.MeasurementContextName;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;
import org.unidata.mdm.system.util.TextUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * The Class AuthenticationRestService.
 */
@Path("/authentication")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class LoginRestService extends AbstractRestService {

    /** The security service. */
    @Autowired
    private SecurityService securityService;

    /** The user service. */
    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    /**
     * Activate temporary user password, after password reset
     * @param activationCode
     * @return
     * @throws Exception
     */
    @GET
    @Path("/activate-password")
    @Operation(
        description = "Activate temporary user password, after password reset.",
        method = "GET",
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "401"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response activate(@Parameter(description = "Activation code") @QueryParam("activationCode") String activationCode) {
        userService.activatePassword(activationCode);
        return ok(new RestResponse<>());
    }

    /**
     * Login.
     *
     * @param loginRequest
     *            the login request
     * @return the response
     * @throws Exception
     *             the exception
     */
    @POST
    @Path(value = "/login")
    @Operation(
        description = "Создание нового токена.",
        method = "POST",
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = LoginResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "401"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response login(final LoginRequest loginRequest){
        MeasurementPoint.init(MeasurementContextName.MEASURE_UI_AUTH);
        MeasurementPoint.start();


        LoginResponse loginResponse = new LoginResponse();
        String login = loginRequest.getUserName();
        Map<AuthenticationSystemParameter, Object> params = new EnumMap<>(AuthenticationSystemParameter.class);
        params.put(AuthenticationSystemParameter.PARAM_USER_NAME, login);
        params.put(AuthenticationSystemParameter.PARAM_USER_PASSWORD, loginRequest.getPassword());
        params.put(AuthenticationSystemParameter.PARAM_USER_LOCALE, loginRequest.getLocale());
        params.put(AuthenticationSystemParameter.PARAM_CLIENT_IP, getClientIp());
        params.put(AuthenticationSystemParameter.PARAM_SERVER_IP, getServerIp());
        params.put(AuthenticationSystemParameter.PARAM_ENDPOINT, EndpointType.REST);
        params.put(AuthenticationSystemParameter.PARAM_HTTP_SERVLET_REQUEST, getHSR());

        try {
            final SecurityToken token = securityService.login(params);

            loginResponse.setToken(token.getToken());
            final UserWithPasswordDTO user = userService.getUserByName(token.getUser().getLogin());
            if (loginRequest.getLocale() != null) {
                Locale newLocale = new Locale(loginRequest.getLocale());
                userService.updateUserLocale(login, newLocale);
                user.setLocale(newLocale);
            }
            final UserRO userInfo = convertUserDTO(user);
            userInfo.setSecurityLabels(
                    UsersConverter.convertSecurityLabelDTOs(
                            SecurityUtils.mergeSecurityLabels(
                                    user.getSecurityLabels(),
                                    user.getRoles().stream()
                                            .flatMap(r -> r.getSecurityLabels().stream())
                                            .collect(Collectors.toList())
                            )
                    )
            );
            userInfo.setRolesData(
                    RoleRoConverter.convertRoleDTOs(roleService.loadRolesData(userInfo.getRoles()))
            );

            loginResponse.setUserInfo(userInfo);
            loginResponse.setRights(new ArrayList<>(token.getRightsMap().values()));
            loginResponse.setTokenTTL(securityService.getTokenTTL());
            loginResponse.setForcePasswordChange(token.getUser().getForcePasswordChangeFlag());

            final String buildVersion = TextUtils.getText("app.version") +
                    "." +
                    TextUtils.getText("app.svn_revision") +
                    "." +
                    TextUtils.getText("app.build_number") +
                    " " +
                    TextUtils.getText("app.build_date");
            loginResponse.setBuildVersion(buildVersion);

            return ok(new RestResponse<>(loginResponse));
        } catch (org.springframework.transaction.CannotCreateTransactionException|org.springframework.transaction.TransactionSystemException e) {
            //UN-3952
            throw new PlatformBusinessException("Cannot connect to database.", CoreRestExceptionIds.EX_SYSTEM_DATABASE_CANNOT_CONNECT);
        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * Returns info about current user.
     *
     * @return Information about current logged in user.
     * @throws Exception
     *             In case of some technical problem.
     */
    @GET
    @Path(value = "/get-current-user")
    @Operation(
        description = "Возвращает информацию о текущем пользователе.",
        method = "GET",
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = LoginResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "401"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response getCurrentUserInfo(){

        LoginResponse loginResponse = new LoginResponse();
        String tokenString = null;
        if (SecurityContextHolder.getContext() != null && SecurityContextHolder.getContext().getAuthentication() != null
                && SecurityContextHolder.getContext().getAuthentication().getPrincipal() != null) {
            tokenString = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }

        if (tokenString == null) {
            return notAuthorized(loginResponse);
        }

        final SecurityToken token = securityService.getTokenObjectByToken(tokenString);
        loginResponse.setForcePasswordChange(token.getUser().getForcePasswordChangeFlag());
        loginResponse.setToken(tokenString);
        loginResponse.setUserInfo(convertUserDTO(userService.getUserByName(token.getUser().getLogin())));
        loginResponse.setRights(new ArrayList<>(token.getRightsMap().values()));
        loginResponse.setTokenTTL(securityService.getTokenTTL());

        String buildVersion = TextUtils.getText("app.version") +
                "." +
                TextUtils.getText("app.svn_revision") +
                "." +
                TextUtils.getText("app.build_number") +
                " " +
                TextUtils.getText("app.build_date");
        loginResponse.setBuildVersion(buildVersion);

        return ok(loginResponse);
    }

    /**
     * Set password.
     *
     * @param setPasswordRequest
     *            the set password request
     * @return the response
     * @throws Exception
     *             the exception
     */
    @POST
    @Path(value = "/setpassword")
    @Operation(
        description = "Задание нового пароля пользователя.",
        method = "POST",
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "401"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response setPassword(@Parameter(description = "Set password request") SetPasswordRequest setPasswordRequest) {

        try {
            securityService.updatePassword(setPasswordRequest.getPassword(), setPasswordRequest.getOldPassword());
        }catch(PlatformBusinessException e){
            ErrorInfo error = new ErrorInfo();
            error.setSeverity(ErrorInfo.Severity.LOW);
            error.setInternalMessage(e.getMessage());
            error.setUserMessage(TextUtils.getText(e.getId().code(), e.getArgs()));
            RestResponse<?> restResponse = new RestResponse<>(false);
            restResponse.setErrors(Collections.singletonList(error));
            return error(restResponse);
        }
        return ok(new RestResponse<>(new UpdateResponse(true, setPasswordRequest.getUserName())));
    }

    /**
     * Logout.
     *
     * @param tokenString
     *            the token string
     * @return the response
     * @throws Exception
     *             the exception
     */
    @POST
    @Path(value = "/logout")
    @Operation(
        description = "Инвалидация токена.",
        method = "POST",
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "202"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "401"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response logout(
            @Parameter(description = "Authorization token", in = ParameterIn.HEADER) @HeaderParam(value = HttpHeaders.AUTHORIZATION) String tokenString) {

        User user = securityService.getUserByToken(tokenString);
        String userName = user != null ? user.getLogin() : null;
        Map<AuthenticationSystemParameter, Object> params = new EnumMap<>(AuthenticationSystemParameter.class);
        params.put(AuthenticationSystemParameter.PARAM_USER_NAME, userName);
        params.put(AuthenticationSystemParameter.PARAM_CLIENT_IP, getClientIp());
        params.put(AuthenticationSystemParameter.PARAM_SERVER_IP, getServerIp());
        params.put(AuthenticationSystemParameter.PARAM_ENDPOINT, EndpointType.REST);
        params.put(AuthenticationSystemParameter.PARAM_HTTP_SERVLET_REQUEST, getHSR());
        params.put(AuthenticationSystemParameter.PARAM_DETAILS,
                TextUtils.getTextWithLocaleAndDefault(user == null ?
                                TextUtils.getDefaultLocale() :
                                user.getLocale(),
                        "app.audit.record.operation.logout.userLogout", null));
        securityService.logout(tokenString, params);
        return Response.accepted().build();
    }
}
