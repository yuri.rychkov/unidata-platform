/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.job.JobExecution;
import org.unidata.mdm.rest.core.ro.job.execution.JobExecutionRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on Jul 6, 2021
 * The
 */
@Component
public class JobExecutionConverter extends Converter<JobExecution, JobExecutionRO> {

    @Autowired
    private ExecutionStateConverter executionStateConverter;

    @Autowired
    private StepExecutionConverter stepExecutionConverter;

    /**
     * Constructor.
     */
    public JobExecutionConverter() {
        super();
        this.to = this::convert;
    }

    private JobExecutionRO convert(JobExecution source) {

        JobExecutionRO target = new JobExecutionRO();

        target.setId(source.getId());
        target.setJobDefinitionId(source.getJobDefinitionId());
        target.setJobName(source.getJobName());
        target.setStartTime(source.getStartTime());
        target.setEndTime(source.getEndTime());
        target.setState(executionStateConverter.to(source.getState()));
        target.setStepExecutions(stepExecutionConverter.to(source.getJobExecutionSteps()));

        return target;
    }
}
