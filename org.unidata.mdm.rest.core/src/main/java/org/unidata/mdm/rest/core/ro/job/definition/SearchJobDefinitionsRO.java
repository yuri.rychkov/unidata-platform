/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.core.ro.job.definition;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * JobSearchRO.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchJobDefinitionsRO {
    /**
     * The job definition name.
     */
    private String name;
    /**
     * The job ID.
     */
    private String jobName;
    /**
     * Return active only.
     */
    private boolean activeOnly;
    /**
     * Return inactive only.
     */
    private boolean inactiveOnly;
    /**
     * The last execution status.
     */
    private String lastExecutionStatus;
    /**
     * The tags.
     */
    private List<String> tags;
    /**
     * Created by.
     */
    private String createdBy;
    /**
     * The count.
     */
    private int count;
    /**
     * The start ID.
     */
    private long start;
    /**
     * The field.
     */
    @Schema(description = "Accepted sort fields. Case doesn't matter.", allowableValues = { "ID", "NAME", "JOB_NAME"})
    private String sortField;
    /**
     * The sort order.
     */
    @Schema(description = "Accepted sort order. Case doen't matter.", allowableValues = { "ASC", "DESC"})
    private String sortOrder;
    /**
     * Gets the text.
     *
     * @return the text
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the text.
     *
     * @param text
     *            the new text
     */
    public void setName(String text) {
        this.name = text;
    }
    /**
     * @return the jobName
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * @param jobName the jobName to set
     */
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    /**
     * @return the activeOnly
     */
    public boolean isActiveOnly() {
        return activeOnly;
    }

    /**
     * @param activeOnly the activeOnly to set
     */
    public void setActiveOnly(boolean activeOnly) {
        this.activeOnly = activeOnly;
    }

    /**
     * @return the inactiveOnly
     */
    public boolean isInactiveOnly() {
        return inactiveOnly;
    }

    /**
     * @param inactiveOnly the inactiveOnly to set
     */
    public void setInactiveOnly(boolean inactiveOnly) {
        this.inactiveOnly = inactiveOnly;
    }

    /**
     * Gets the last execution status.
     *
     * @return the last execution status
     */
    public String getLastExecutionStatus() {
        return lastExecutionStatus;
    }
    /**
     * Sets the last execution status.
     *
     * @param lastExecutionStatus
     *            the new last execution status
     */
    public void setLastExecutionStatus(String lastExecutionStatus) {
        this.lastExecutionStatus = lastExecutionStatus;
    }

    /**
     * Gets the tags.
     *
     * @return the tags
     */
    public List<String> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     *
     * @param tags the new tags
     */
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Gets the count.
     *
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * Sets the count.
     *
     * @param count
     *            the new count
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Gets the start.
     *
     * @return the start
     */
    public long getStart() {
        return start;
    }

    /**
     * Sets the start.
     *
     * @param start
     *            the new start
     */
    public void setStart(long start) {
        this.start = start;
    }

    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }

    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    /**
     * @return the sortOrder
     */
    public String getSortOrder() {
        return sortOrder;
    }

    /**
     * @param sortOrder the sortOrder to set
     */
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }
}
