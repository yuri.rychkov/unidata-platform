/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro.touch;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Mikhail Mikhailov on Jun 18, 2021
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TouchRO {
    /**
     * The name of the touch.
     */
    private String touchName;
    /**
     * The output type, this touch expects.
     */
    private String outputType;
    /**
     * Param types this touch supplies.
     */
    private Map<String, String> paramTypes;
    /**
     * Constructor.
     */
    public TouchRO() {
        super();
    }
    /**
     * @return the touchName
     */
    public String getTouchName() {
        return touchName;
    }
    /**
     * @param touchName the touchName to set
     */
    public void setTouchName(String touchName) {
        this.touchName = touchName;
    }
    /**
     * @return the outputType
     */
    public String getOutputType() {
        return outputType;
    }
    /**
     * @param outputType the outputType to set
     */
    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }
    /**
     * @return the paramTypes
     */
    public Map<String, String> getParamTypes() {
        return paramTypes;
    }
    /**
     * @param paramTypes the paramTypes to set
     */
    public void setParamTypes(Map<String, String> paramTypes) {
        this.paramTypes = paramTypes;
    }

}
