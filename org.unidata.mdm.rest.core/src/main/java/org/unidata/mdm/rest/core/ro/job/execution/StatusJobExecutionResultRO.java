/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro.job.execution;
/**
 * @author Mikhail Mikhailov on Jul 8, 2021
 */
public class StatusJobExecutionResultRO extends GetJobExecutionResultRO {
    /**
     * Progress mark.
     */
    private double progress;
    /**
     * Constructor.
     */
    public StatusJobExecutionResultRO() {
        super();
    }
    /**
     * Constructor.
     * @param execution
     */
    public StatusJobExecutionResultRO(JobExecutionRO execution) {
        super(execution);
    }
    /**
     * Constructor.
     * @param execution
     */
    public StatusJobExecutionResultRO(JobExecutionRO execution, double progress) {
        this(execution);
        this.progress = progress;
    }
    /**
     * @return the progress
     */
    public double getProgress() {
        return progress;
    }
    /**
     * @param progress the progress to set
     */
    public void setProgress(double progress) {
        this.progress = progress;
    }
}
