/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.job.JobDescriptor;
import org.unidata.mdm.rest.core.ro.job.descriptor.JobDesriptorRO;
import org.unidata.mdm.system.convert.Converter;

/**
 * @author Mikhail Mikhailov on Jun 18, 2021
 */
@Component
public class JobDescriptorConverter extends Converter<JobDescriptor, JobDesriptorRO> {

    @Autowired
    private TouchConverter touchConverter;

    @Autowired
    private JobParameterDescriptorConverter jobParameterDescriptorConverter;

    @Autowired
    private JobFractionConverter jobFractionConverter;

    /**
     * Constructor.
     */
    public JobDescriptorConverter() {
        super();
        this.to = this::convert;
    }

    private JobDesriptorRO convert(JobDescriptor source) {

        JobDesriptorRO target = new JobDesriptorRO();

        target.setJobName(source.getJobName());
        target.setDisplayName(source.getDisplayName());
        target.setDescription(source.getDescription());
        target.setModular(source.isModular());
        target.setSystem(source.isSystem());
        target.setTouches(touchConverter.to(source.getTouches()));
        target.setParameters(jobParameterDescriptorConverter.to(source.getParametersCollection()));
        target.setFractions(jobFractionConverter.to(source.getFractions()));

        return target;
    }

}
