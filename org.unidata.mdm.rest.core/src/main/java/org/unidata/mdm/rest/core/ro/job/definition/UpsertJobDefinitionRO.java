/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro.job.definition;

/**
 * @author Mikhail Mikhailov on Jul 3, 2021
 */
public class UpsertJobDefinitionRO {
    /**
     * The definition.
     */
    private JobDefinitionRO definition;
    /**
     * Skip cron warnings about suspicious execution spects.
     */
    private boolean skipCronWarnings;
    /**
     * Constructor.
     */
    public UpsertJobDefinitionRO() {
        super();
    }
    /**
     * @return the definition
     */
    public JobDefinitionRO getDefinition() {
        return definition;
    }
    /**
     * @param definition the definition to set
     */
    public void setDefinition(JobDefinitionRO definition) {
        this.definition = definition;
    }
    /**
     * @return the skipCronWarnings
     */
    public boolean isSkipCronWarnings() {
        return skipCronWarnings;
    }
    /**
     * @param skipCronWarnings the skipCronWarnings to set
     */
    public void setSkipCronWarnings(boolean skipCronWarnings) {
        this.skipCronWarnings = skipCronWarnings;
    }
}
