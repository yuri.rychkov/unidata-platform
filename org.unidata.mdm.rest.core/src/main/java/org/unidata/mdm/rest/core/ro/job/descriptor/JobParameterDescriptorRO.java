package org.unidata.mdm.rest.core.ro.job.descriptor;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author Mikhail Mikhailov on Jun 18, 2021
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JobParameterDescriptorRO {
    /**
     * Param name.
     */
    private String name;
    /**
     * Display name.
     */
    private String displayName;
    /**
     * Display description supplier (can be used for i18n purposes).
     */
    private String description;
    /**
     * Outer type.
     */
    @Schema(description = "The value type.",
            allowableValues = { "STRING", "BOOLEAN", "INTEGER", "NUMBER", "CLOB", "DATE", "TIME", "TIMESTAMP", "INSTANT", "CUSTOM" })
    private String type;
    /**
     * Layout kind.
     */
    @Schema(description = "Layout kind.",
            allowableValues = { "SINGLE", "COLLECTION", "MAP", "CUSTOM" })
    private String kind;
    /**
     * Is the parameter required.
     */
    private boolean required;
    /**
     * The parameter is hidden.
     */
    private boolean hidden;
    /**
     * The parameter is read only.
     */
    private boolean readOnly;
    /**
     * Default value, if set.
     */
    private Object defaultValue;
    /**
     * Combo/multi box support.
     */
    private Map<Object, String> selector;
    /**
     * Constructor.
     */
    public JobParameterDescriptorRO() {
        super();
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }
    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the type
     */
    public String getType() {
        return type;
    }
    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * @return the kind
     */
    public String getKind() {
        return kind;
    }
    /**
     * @param kind the kind to set
     */
    public void setKind(String kind) {
        this.kind = kind;
    }
    /**
     * @return the required
     */
    public boolean isRequired() {
        return required;
    }
    /**
     * @param required the required to set
     */
    public void setRequired(boolean required) {
        this.required = required;
    }
    /**
     * @return the hidden
     */
    public boolean isHidden() {
        return hidden;
    }
    /**
     * @param hidden the hidden to set
     */
    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }
    /**
     * @return the readOnly
     */
    public boolean isReadOnly() {
        return readOnly;
    }
    /**
     * @param readOnly the readOnly to set
     */
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }
    /**
     * @return the defaultValue
     */
    public Object getDefaultValue() {
        return defaultValue;
    }
    /**
     * @param defaultValue the defaultValue to set
     */
    public void setDefaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
    }
    /**
     * @return the selectionSource
     */
    public Map<Object, String> getSelector() {
        return Objects.isNull(selector) ? Collections.emptyMap() : selector;
    }
    /**
     * @param selectionSource the selectionSource to set
     */
    public void setSelector(Map<Object, String> selectionSource) {
        this.selector = selectionSource;
    }
}
