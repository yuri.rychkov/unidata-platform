/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.ro.job.definition;

import org.unidata.mdm.rest.core.ro.job.ExecutionStateRO;

/**
 * @author Mikhail Mikhailov on Jun 29, 2021
 */
public class JobDefinitionWithStateRO {

    private JobDefinitionRO definition;

    private ExecutionStateRO state;
    /**
     * Constructor.
     */
    public JobDefinitionWithStateRO() {
        super();
    }
    /**
     * @return the definition
     */
    public JobDefinitionRO getDefinition() {
        return definition;
    }
    /**
     * @param definition the definition to set
     */
    public void setDefinition(JobDefinitionRO definition) {
        this.definition = definition;
    }
    /**
     * @return the state
     */
    public ExecutionStateRO getState() {
        return state;
    }
    /**
     * @param state the state to set
     */
    public void setState(ExecutionStateRO info) {
        this.state = info;
    }
}
