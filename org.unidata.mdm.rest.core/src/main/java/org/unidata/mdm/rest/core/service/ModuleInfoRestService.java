/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.core.service;

import java.util.Comparator;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.rest.core.ro.ModuleInfoRO;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.service.ModuleService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * @author maria.chistyakova
 * @since 18.03.2020
 */
@Path("/" + ModuleInfoRestService.SERVICE_PATH)
@Consumes({"application/json"})
@Produces({"application/json"})
public class ModuleInfoRestService extends AbstractRestService {
    /**
     * Path.
     */
    public static final String SERVICE_PATH = "module";
    /**
     * Tag.
     */
    public static final String SERVICE_TAG = SERVICE_PATH;
    /**
     * MS.
     */
    @Autowired
    ModuleService moduleService;

    @GET
    @Operation(
        description = "Returns list of modules, known to the system.",
        method = HttpMethod.GET,
        tags = { SERVICE_TAG },
        responses = {
            @ApiResponse(content = @Content(array = @ArraySchema(schema = @Schema(implementation = ModuleInfoRO.class))), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response findAll() {
        return ok(moduleService.getModules().stream()
                    .map(module -> new ModuleInfoRO(module.getId(), module.getName(), module.getDescription(), module.getVersion()))
                    .sorted(Comparator.comparing(ModuleInfoRO::getId))
                    .collect(Collectors.toList()));
    }
}
