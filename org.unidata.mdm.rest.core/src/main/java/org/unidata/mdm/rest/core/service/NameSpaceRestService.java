/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.service;

import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.rest.core.ro.GetNamespacesRO;
import org.unidata.mdm.rest.core.ro.NameSpaceRO;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.service.NameSpaceService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * @author Mikhail Mikhailov on Mar 4, 2021
 */
@Path("/" + NameSpaceRestService.SERVICE_PATH)
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class NameSpaceRestService extends AbstractRestService {
    /**
     * Path.
     */
    public static final String SERVICE_PATH = "namespaces";
    /**
     * Tag.
     */
    public static final String SERVICE_TAG = SERVICE_PATH;
    /**
     * NS SVC.
     */
    @Autowired
    private NameSpaceService nameSpaceService;
    /**
     * Constructor.
     */
    public NameSpaceRestService() {
        super();
    }
    /**
     * Gets NSs.
     * @return NSs
     */
    @GET
    @Operation(
        description = "Returns list of namespaces, known to the system.",
        method = HttpMethod.GET,
        tags = { SERVICE_TAG },
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = GetNamespacesRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "401"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500")
        }
    )
    public Response findAll() {
        return ok(new GetNamespacesRO(nameSpaceService.getNameSpaces().stream()
                .map(ns -> new NameSpaceRO(ns.getId(), ns.getDisplayName(), ns.getDescription(), ns.getModuleId()))
                .collect(Collectors.toList())));
    }
}
