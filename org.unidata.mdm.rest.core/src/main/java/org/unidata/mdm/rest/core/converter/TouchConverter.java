/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.core.converter;

import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.unidata.mdm.rest.core.ro.touch.TouchRO;
import org.unidata.mdm.system.convert.Converter;
import org.unidata.mdm.system.type.touch.Touch;

/**
 * @author Mikhail Mikhailov on Jun 18, 2021
 */
@Component
public class TouchConverter extends Converter<Touch<?>, TouchRO> {
    /**
     * Constructor.
     * @param to
     * @param from
     */
    public TouchConverter() {
        super(TouchConverter::convert, null);
    }

    private static TouchRO convert(Touch<?> source) {

        TouchRO target = new TouchRO();

        target.setTouchName(source.getTouchName());
        target.setOutputType(source.getOutputType().getName());
        target.setParamTypes(source.getParamTypes().entrySet()
                .stream()
                .collect(Collectors.toMap(Entry::getKey, entry -> entry.getValue().getName())));

        return target;
    }
}
