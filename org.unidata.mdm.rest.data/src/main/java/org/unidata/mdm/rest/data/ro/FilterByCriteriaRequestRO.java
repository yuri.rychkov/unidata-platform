/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import org.springframework.format.annotation.DateTimeFormat;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FilterByCriteriaRequestRO {

    private List<String> etalonIds;

    /**
     * Date from.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime validFrom;

    /**
     * Date to.
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime validTo;

    /**
     * FULL | PARTIAL
     */
    private String timeIntervalIntersectType;

    public List<String> getEtalonIds() {
        return etalonIds;
    }

    public FilterByCriteriaRequestRO setEtalonIds(List<String> etalonIds) {
        this.etalonIds = etalonIds;
        return this;
    }

    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public FilterByCriteriaRequestRO setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    public LocalDateTime getValidTo() {
        return validTo;
    }

    public FilterByCriteriaRequestRO setValidTo(LocalDateTime validTo) {
        this.validTo = validTo;
        return this;
    }

    public String getTimeIntervalIntersectType() {
        return timeIntervalIntersectType;
    }

    public FilterByCriteriaRequestRO setTimeIntervalIntersectType(String timeIntervalIntersectType) {
        this.timeIntervalIntersectType = timeIntervalIntersectType;
        return this;
    }
}
