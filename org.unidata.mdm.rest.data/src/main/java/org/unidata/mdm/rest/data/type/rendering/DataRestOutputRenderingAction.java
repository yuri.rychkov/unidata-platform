package org.unidata.mdm.rest.data.type.rendering;

import org.unidata.mdm.system.type.rendering.OutputRenderingAction;

/**
 * @author Mikhail Mikhailov on Jan 30, 2020
 */
public enum DataRestOutputRenderingAction implements OutputRenderingAction {
    /**
     * AU op.
     */
    ATOMIC_UPSERT_OUTPUT,
    /**
     * The old GET meths support
     */
    ATOMIC_GET_OUTPUT,
    /**
     * Delete output op.
     */
    ATOMIC_DELETE_OUTPUT,
    /**
     * The merge output.
     */
    MERGE_OUTPUT,
    /**
     * The preview output.
     */
    PREVIEW_OUTPUT,
    /**
     * Record restore output.
     */
    RECORD_RESTORE_OUTPUT,
    /**
     * Period restore output.
     */
    PERIOD_RESTORE_OUTPUT;
}
