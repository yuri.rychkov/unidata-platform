package org.unidata.mdm.rest.system.ro;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import org.unidata.mdm.system.type.rendering.OutputRenderingAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * The base output composite object.
 * Contains fields for dynamic processing by system modules
 *
 * @author Alexandr Serov
 * @since 28.09.2020
 **/
public abstract class DetailedCompositeOutputRO extends DetailedOutputRO implements RestOutputSink {

    @JsonIgnore
    public abstract OutputRenderingAction getOutputRenderingAction();

    /**
     * Dynamic object fields
     */
    private FragmentsRO payload;

    /**
     * Adds a part to output.
     *
     * @param name the property name
     * @param value the value
     */
    @Override
    // @JsonAnySetter
    @JsonIgnore
    public void setAny(String name, JsonNode value) {

        if (Objects.isNull(payload)) {
            payload = new FragmentsRO();
        }

        payload.setAny(name, value);
    }

    /**
     * @return output parts
     */
    // @JsonAnyGetter
    @JsonIgnore
    public Map<String, JsonNode> getAny() {
        return Objects.nonNull(payload) ? payload.getAny() : Collections.emptyMap();
    }

    /**
     * @return the payload
     */
    public FragmentsRO getPayload() {
        return payload;
    }

    /**
     * @param payload the payload to set
     */
    public void setPayload(FragmentsRO payload) {
        this.payload = payload;
    }
}
