package org.unidata.mdm.meta.type.model;

/**
 * Model element with display order
 *
 * @author Alexandr Serov
 * @since 26.08.2020
 **/
public interface OrderedElement {

    int getOrder();

    void setOrder(int order);

}
