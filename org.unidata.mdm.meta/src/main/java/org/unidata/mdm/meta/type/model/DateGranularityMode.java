package org.unidata.mdm.meta.type.model;

public enum DateGranularityMode {

    DATE,
    DATETIME,
    DATETIMEMILLIS;

    public String value() {
        return name();
    }

    public static DateGranularityMode fromValue(String v) {
        return valueOf(v);
    }

}
