/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.service.impl.instance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.unidata.mdm.core.type.model.ReferencePresentationElement;
/**
 * @author Mikhail Mikhailov on Oct 16, 2020
 * General purpose reference presentation info.
 */
public class ReferencePresentationImpl implements ReferencePresentationElement {
    /**
     * Display attributes.
     */
    private final List<String> displayAttributes = new ArrayList<>();
    /**
     * Search attributes.
     */
    private final List<String> searchAttributes = new ArrayList<>();
    /**
     * Show names flag.
     */
    private final boolean showAttributeNames;
    /**
     * Constructor.
     * @param display
     * @param search
     * @param showAttributeNames
     */
    public ReferencePresentationImpl(Collection<String> display, Collection<String> search, boolean showAttributeNames) {
        super();
        this.displayAttributes.addAll(display);
        this.searchAttributes.addAll(search);
        this.showAttributeNames = showAttributeNames;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<String> getDisplayAttributes() {
        return displayAttributes;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<String> getSearchAttributes() {
        return searchAttributes;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean showAttributeNames() {
        return showAttributeNames;
    }
}
