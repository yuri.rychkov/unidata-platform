package org.unidata.mdm.meta.type.model.strategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategyType;


/**
 * Value generation strategy abstract implementation
 *
 * @author Alexandr Serov
 * @see ValueGenerationStrategyType
 * @since 25.08.2020
 **/
public abstract class AbstractValueGenerationStrategy implements ValueGenerationStrategy {

    private final static long serialVersionUID = 987654321L;

//    @JacksonXmlProperty(isAttribute = true)
    @JsonIgnore
    protected final ValueGenerationStrategyType strategyType;

    protected AbstractValueGenerationStrategy(ValueGenerationStrategyType strategyType) {
        this.strategyType = strategyType;
    }

    @Override
    public ValueGenerationStrategyType getStrategyType() {
        return strategyType;
    }

}
