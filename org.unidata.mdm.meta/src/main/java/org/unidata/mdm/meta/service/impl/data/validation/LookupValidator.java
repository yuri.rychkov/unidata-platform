/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service.impl.data.validation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.stereotype.Component;
import org.unidata.mdm.core.service.CustomPropertiesSupport;
import org.unidata.mdm.meta.type.model.attributes.CodeMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.SearchableMetaModelAttribute;
import org.unidata.mdm.meta.type.model.entities.LookupEntity;
import org.unidata.mdm.system.exception.ValidationResult;

@Component
public class LookupValidator extends AbstractDataModelElementValidator<LookupEntity> implements CustomPropertiesSupport {

    private static final String ENTITY_CODE_ATTRIBUTE_IS_ABSENT = "app.meta.entity.code.attribute.absent";

    private static final String ENTITY_CODE_ATTRIBUTE_IS_INCORRECT = "app.meta.entity.code.attribute.incorrect";

    @Nonnull
    @Override
    public DataModelElementType getSupportedElementType() {
        return DataModelElementType.LOOKUP;
    }

    @Override
    public Collection<ValidationResult> checkElement(LookupEntity el) {

        Collection<ValidationResult> errors = new ArrayList<>();

        errors.addAll(super.checkElement(el));
        errors.addAll(super.checkEntitiesGroupValue(el.getGroupName(), el.getDisplayName()));
        errors.addAll(super.checkTopLevelNameStopList(el.getName(), el.getDisplayName()));
        errors.addAll(super.checkValidityPeriod(el.getValidityPeriod(), el.getDisplayName()));

        // 1. Check code attrs
        errors.addAll(checkCodeAttribute(el.getCodeAttribute(), el.getDisplayName(), false));
        el.getAliasCodeAttributes().forEach(code -> errors.addAll(checkCodeAttribute(code, el.getDisplayName(), true)));

        // 2. Check simple attrs
        el.getSimpleAttribute().forEach(attr-> errors.addAll(checkSimpleAttribute(attr, el.getDisplayName())));

        boolean mainIsSimple = el.getSimpleAttribute().stream().anyMatch(SearchableMetaModelAttribute::isMainDisplayable);
        boolean mainIsCode = el.getAliasCodeAttributes().stream().anyMatch(SearchableMetaModelAttribute::isMainDisplayable);
        boolean mainDisplayableSet = mainIsSimple || mainIsCode || el.getCodeAttribute().isMainDisplayable();

        errors.addAll(super.checkMainDisplayableState(mainDisplayableSet, el.getDisplayName()));
        errors.addAll(super.checkValidityPeriod(el.getValidityPeriod(), el.getDisplayName()));
        errors.addAll(validateCustomProperties(el.getDisplayName(), el.getCustomProperties()));

        return errors;
    }

    private Collection<ValidationResult> checkCodeAttribute(CodeMetaModelAttribute codeAttr, String entityName, boolean isAlternative) {

        if (Objects.isNull(codeAttr)) {
            final String message = "Lookup element [{}] has invalid value for a code attribute.";
            return Collections.singleton(new ValidationResult(message, ENTITY_CODE_ATTRIBUTE_IS_ABSENT, entityName));
        }

        boolean isCorrectCodeAttr = (!codeAttr.isNullable() || isAlternative)
                && (!codeAttr.isReadOnly() || isAlternative)
                && codeAttr.isUnique()
                && codeAttr.isDisplayable()
                && codeAttr.isSearchable()
                && !codeAttr.isHidden();

        if (!isCorrectCodeAttr) {
            final String message = "Lookup element [{}] has invalid settings for a code attribute [{}].";
            return Collections.singleton(new ValidationResult(message, ENTITY_CODE_ATTRIBUTE_IS_INCORRECT, entityName, codeAttr.getDisplayName()));
        }

        return Collections.emptyList();
    }

    @Nullable
    @Override
    public String getModelElementId(@Nonnull LookupEntity modelElement) {
        return modelElement.getName();
    }
}
