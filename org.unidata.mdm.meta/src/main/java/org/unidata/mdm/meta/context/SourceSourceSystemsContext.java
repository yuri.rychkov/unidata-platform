/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.context;

import org.unidata.mdm.core.context.AbstractModelSourceContext;
import org.unidata.mdm.core.service.segments.ModelGetStartExecutor;
import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.type.model.sourcesystem.SourceSystemsModel;
import org.unidata.mdm.meta.util.ModelUtils;

/**
 * @author Mikhail Mikhailov on Nov 28, 2019
 */
public class SourceSourceSystemsContext extends AbstractModelSourceContext<SourceSystemsModel> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -6800333071676718225L;
    /**
     * Constructor.
     * @param b the builder.
     */
    private SourceSourceSystemsContext(SourceSourceSystemsContextBuilder b) {
        super(b);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return TypeIds.SOURCE_SYSTEMS_MODEL;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        // Source systems models are singletons per storage
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return ModelGetStartExecutor.SEGMENT_ID;
    }
    /**
     * Gets a builder instance.
     * @return builder instance
     */
    public static SourceSourceSystemsContextBuilder builder() {
        return new SourceSourceSystemsContextBuilder();
    }
    /**
     * The builder for this context.
     * @author Mikhail Mikhailov on Nov 28, 2019
     */
    public static class SourceSourceSystemsContextBuilder
        extends AbstractModelSourceContextBuilder<SourceSystemsModel, SourceSourceSystemsContextBuilder> {
        /**
         * Constructor.
         */
        protected SourceSourceSystemsContextBuilder() {
            super();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public SourceSourceSystemsContext build() {
            return new SourceSourceSystemsContext(this);
        }
    }
}
