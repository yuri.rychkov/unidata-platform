/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.context;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.core.context.AbstractModelGetContext;
import org.unidata.mdm.core.service.segments.ModelGetStartExecutor;
import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.util.ModelUtils;

/**
 * @author Mikhail Mikhailov on Nov 28, 2019
 */
public class GetEnumerationsContext extends AbstractModelGetContext {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -6245429588435518790L;
    /**
     * Requested enumeration ids.
     */
    private final List<String> enumerationIds;
    /**
     * Constructor.
     * @param b the builder.
     */
    private GetEnumerationsContext(GetEnumerationsContextBuilder b) {
        super(b);
        this.enumerationIds = Objects.isNull(b.enumerationIds) ? Collections.emptyList() : b.enumerationIds;

        setFlag(MetaContextFlags.FLAG_ALL_ENUMERATIONS, b.allEnumerations);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return TypeIds.ENUMERATIONS_MODEL;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        // Enumerations models are singletons per storage
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return ModelGetStartExecutor.SEGMENT_ID;
    }
    /**
     * All enumerations requested?
     * @return request state
     */
    public boolean isAllEnumerations() {
        return getFlag(MetaContextFlags.FLAG_ALL_ENUMERATIONS);
    }
    /**
     * @return the enumerationIds
     */
    public List<String> getEnumerationIds() {
        return enumerationIds;
    }
    /**
     * Gets a builder instance.
     * @return builder instance
     */
    public static GetEnumerationsContextBuilder builder() {
        return new GetEnumerationsContextBuilder();
    }
    /**
     * The builder for this context.
     * @author Mikhail Mikhailov on Nov 28, 2019
     */
    public static class GetEnumerationsContextBuilder extends AbstractModelGetContextBuilder<GetEnumerationsContextBuilder> {
        /**
         * Requested enumeration ids.
         */
        private List<String> enumerationIds;
        /**
         * All enumerations requested.
         */
        private boolean allEnumerations;
        /**
         * Constructor.
         */
        protected GetEnumerationsContextBuilder() {
            super();
        }
        /**
         * @param enumerationIds the enumerationIds to set
         */
        public GetEnumerationsContextBuilder enumerationIds(List<String> enumerationIds) {
            this.enumerationIds = enumerationIds;
            return self();
        }
        /**
         * @param allEnumerations the allEnumerations to set
         */
        public GetEnumerationsContextBuilder allEnumerations(boolean allEnumerations) {
            this.allEnumerations = allEnumerations;
            return self();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public GetEnumerationsContext build() {
            return new GetEnumerationsContext(this);
        }
    }
}
