package org.unidata.mdm.meta.type.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.unidata.mdm.meta.type.model.strategy.ConcatenatedValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.strategy.CustomValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.strategy.RandomValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.strategy.SequenceValueGenerationStrategy;

/**
 * Value generation strategy
 * <p>
 * Defines the rules by which the value will be created
 *
 * @author Alexandr Serov
 * @see ValueGenerationStrategyType
 * @since 25.08.2020
 **/
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = RandomValueGenerationStrategy.class, name = "RandomValueGenerationStrategyDefImpl"),
    @JsonSubTypes.Type(value = ConcatenatedValueGenerationStrategy.class, name = "ConcatenatedValueGenerationStrategyDefImpl"),
    @JsonSubTypes.Type(value = SequenceValueGenerationStrategy.class, name = "SequenceValueGenerationStrategyDefImpl"),
    @JsonSubTypes.Type(value = CustomValueGenerationStrategy.class, name = "CustomValueGenerationStrategyDefImpl")
})
public interface ValueGenerationStrategy extends Serializable {

    ValueGenerationStrategyType getStrategyType();

}
