package org.unidata.mdm.meta.type.model.entities;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.meta.type.model.MetaModelEntity;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.attributes.CodeMetaModelAttribute;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class LookupEntity extends AbstractEntity<LookupEntity> implements MetaModelEntity {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 7496714751084265889L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private ValueGenerationStrategy externalIdGenerationStrategy;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private CodeMetaModelAttribute codeAttribute;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private List<CodeMetaModelAttribute> aliasCodeAttributes;

    @JacksonXmlProperty(isAttribute = true)
    private boolean dashboardVisible;

    @JacksonXmlProperty(isAttribute = true)
    private String groupName;

    public ValueGenerationStrategy getExternalIdGenerationStrategy() {
        return externalIdGenerationStrategy;
    }

    public void setExternalIdGenerationStrategy(ValueGenerationStrategy value) {
        this.externalIdGenerationStrategy = value;
    }

    public CodeMetaModelAttribute getCodeAttribute() {
        return codeAttribute;
    }

    public void setCodeAttribute(CodeMetaModelAttribute value) {
        this.codeAttribute = value;
    }

    public List<CodeMetaModelAttribute> getAliasCodeAttributes() {
        if (aliasCodeAttributes == null) {
            aliasCodeAttributes = new ArrayList<>();
        }
        return this.aliasCodeAttributes;
    }

    public boolean isDashboardVisible() {
        return dashboardVisible;
    }

    public void setDashboardVisible(Boolean value) {
        this.dashboardVisible = value;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String value) {
        this.groupName = value;
    }

    public LookupEntity withDashboardVisible(Boolean value) {
        setDashboardVisible(value);
        return this;
    }

    public LookupEntity withGroupName(String value) {
        setGroupName(value);
        return this;
    }

    public LookupEntity withExternalIdGenerationStrategy(ValueGenerationStrategy value) {
        setExternalIdGenerationStrategy(value);
        return this;
    }

    public LookupEntity withCodeAttribute(CodeMetaModelAttribute value) {
        setCodeAttribute(value);
        return this;
    }

    public LookupEntity withAliasCodeAttributes(CodeMetaModelAttribute... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            Collections.addAll(getAliasCodeAttributes(), values);
        }
        return self();
    }

    public LookupEntity withAliasCodeAttributes(Collection<CodeMetaModelAttribute> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getAliasCodeAttributes().addAll(values);
        }
        return self();
    }
}
