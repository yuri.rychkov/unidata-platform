package org.unidata.mdm.meta.type.model.attributes;

import org.unidata.mdm.meta.type.model.OrderedElement;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class ComplexMetaModelAttribute extends AbstractMetaModelAttribute<ComplexMetaModelAttribute> implements OrderedElement {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -494314636605780026L;

    @JacksonXmlProperty(isAttribute = true, localName = "nestedEntityName")
    protected String nestedEntityName;

    @JacksonXmlProperty(isAttribute = true, localName = "nestedEntityKeyAttribute")
    protected String nestedEntityKeyAttribute;

    @JacksonXmlProperty(isAttribute = true)
    protected Integer minCount;

    @JacksonXmlProperty(isAttribute = true)
    protected Integer maxCount;

    @JacksonXmlProperty(isAttribute = true)
    protected int order;

    public String getNestedEntityName() {
        return nestedEntityName;
    }

    public void setNestedEntityName(String value) {
        this.nestedEntityName = value;
    }

    public String getNestedEntityKeyAttribute() {
        return nestedEntityKeyAttribute;
    }

    public void setNestedEntityKeyAttribute(String value) {
        this.nestedEntityKeyAttribute = value;
    }

    public Integer getMinCount() {
        return minCount;
    }

    public void setMinCount(Integer value) {
        this.minCount = value;
    }

    public Integer getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(Integer value) {
        this.maxCount = value;
    }

    @Override
    public int getOrder() {
        return order;
    }

    @Override
    public void setOrder(int value) {
        this.order = value;
    }

    public ComplexMetaModelAttribute withNestedEntityName(String value) {
        setNestedEntityName(value);
        return this;
    }

    public ComplexMetaModelAttribute withNestedEntityKeyAttribute(String value) {
        setNestedEntityKeyAttribute(value);
        return this;
    }

    public ComplexMetaModelAttribute withMinCount(Integer value) {
        setMinCount(value);
        return this;
    }

    public ComplexMetaModelAttribute withMaxCount(Integer value) {
        setMaxCount(value);
        return this;
    }

    public ComplexMetaModelAttribute withOrder(int value) {
        setOrder(value);
        return this;
    }
}
