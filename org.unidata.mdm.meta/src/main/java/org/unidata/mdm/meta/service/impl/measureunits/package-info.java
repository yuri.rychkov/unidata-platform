/**
 * @author Mikhail Mikhailov on Dec 1, 2020
 * Measurement units model types.
 */
package org.unidata.mdm.meta.service.impl.measureunits;