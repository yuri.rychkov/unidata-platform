/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.dao;

import java.util.List;

import org.unidata.mdm.meta.po.MeasurementUnitsPO;

/**
 * @author Mikhail Mikhailov on Oct 2, 2020
 * Measurement units DAO.
 */
public interface MeasurementUnitsDAO {
    /**
     * Loads current version of measurement units set for a storage id.
     * @param storageId the storage id
     * @return measurement units object or null, if there are no saved measurement units.
     */
    MeasurementUnitsPO current(String storageId);
    /**
     * Loads several measurement units versions sorted by revision / create date.
     * @param storageId the storage id
     * @param from start from
     * @param count return count
     * @param withData load 'content' field or not
     * @return collection of measurement units objects
     */
    List<MeasurementUnitsPO> load(String storageId, int from, int count, boolean withData);
    /**
     * Saves a new version of measurement units set.
     * @param po the PO
     * @return revision put
     */
    int save(MeasurementUnitsPO po);
    /**
     * Deletes a measurement units revision.
     * @param storageId the storage id
     * @param revision the revision
     */
    void remove(String storageId, int revision);
}
