/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.dto;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.core.dto.AbstractModelResult;
import org.unidata.mdm.draft.type.DraftPayloadResponse;
import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.util.ModelUtils;

/**
 * @author Mikhail Mikhailov on Nov 28, 2019
 */
public class GetModelDTO extends AbstractModelResult implements DraftPayloadResponse {
    /**
     * Entities.
     */
    private List<GetEntityDTO> entities;
    /**
     * Nested entities.
     */
    private List<GetNestedEntitiesResult> nested;
    /**
     * Entities.
     */
    private List<GetLookupDTO> lookups;
    /**
     * Relations.
     */
    private List<GetModelRelationDTO> relations;
    /**
     * EGR.
     */
    private GetEntitiesGroupsDTO entityGroups;
    /**
     * Filtered by side result
     */
    private GetEntitiesWithRelationsDTO relationsBySide;
    /**
     * Constructor.
     */
    public GetModelDTO() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return TypeIds.DATA_MODEL;
    }
    /**
     * @return the entities
     */
    public List<GetEntityDTO> getEntities() {
        return Objects.isNull(entities) ? Collections.emptyList() : entities;
    }
    /**
     * @param entities the entities to set
     */
    public void setEntities(List<GetEntityDTO> entities) {
        this.entities = entities;
    }
    /**
     * @return the nested
     */
    public List<GetNestedEntitiesResult> getNested() {
        return Objects.isNull(nested) ? Collections.emptyList() : nested;
    }
    /**
     * @param nested the nested to set
     */
    public void setNested(List<GetNestedEntitiesResult> nested) {
        this.nested = nested;
    }
    /**
     * @return the lookups
     */
    public List<GetLookupDTO> getLookups() {
        return Objects.isNull(lookups) ? Collections.emptyList() : lookups;
    }
    /**
     * @param lookups the lookups to set
     */
    public void setLookups(List<GetLookupDTO> lookups) {
        this.lookups = lookups;
    }
    /**
     * @return the relations
     */
    public List<GetModelRelationDTO> getRelations() {
        return Objects.isNull(relations) ? Collections.emptyList() : relations;
    }
    /**
     * @param relations the relations to set
     */
    public void setRelations(List<GetModelRelationDTO> relations) {
        this.relations = relations;
    }
    /**
     * @return the relationsBySide
     */
    public GetEntitiesWithRelationsDTO getRelationsBySide() {
        return relationsBySide;
    }
    /**
     * @param relationsBySide the relationsBySide to set
     */
    public void setRelationsBySide(GetEntitiesWithRelationsDTO relationsBySide) {
        this.relationsBySide = relationsBySide;
    }
    /**
     * @return the entityGroups
     */
    public GetEntitiesGroupsDTO getEntityGroups() {
        return entityGroups;
    }
    /**
     * @param entityGroups the entityGroups to set
     */
    public void setEntityGroups(GetEntitiesGroupsDTO entityGroups) {
        this.entityGroups = entityGroups;
    }
}
