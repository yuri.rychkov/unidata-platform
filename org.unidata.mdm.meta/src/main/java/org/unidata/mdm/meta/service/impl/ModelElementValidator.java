/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service.impl;

import java.io.Serializable;
import java.util.Collection;

import org.unidata.mdm.meta.service.impl.data.validation.DataModelElementType;
import org.unidata.mdm.system.exception.ValidationResult;

/**
 * This interface can be implemented, if one wish to verify a particular model element.
 * This is for basic validations only. Overall model consistency is not checked.
 *
 * @param <V> marker param , which show that all top level meta model elements allow version control
 */
public interface ModelElementValidator<V extends Serializable> {
    /**
     * Verify inner state of model element
     *
     * @param modelElement top level meta model element for verifying.
     * @return validation result
     */
    Collection<ValidationResult> checkElement(V modelElement);
    /**
     * Gets the supported type enum label.
     * @return type
     */
    DataModelElementType getSupportedElementType();
    /**
     * Cast self to spcifically parameterized type.
     * @param <X> the type
     * @return self
     */
    @SuppressWarnings("unchecked")
    default <X extends Serializable> ModelElementValidator<X> narrow() {
        return (ModelElementValidator<X>) this;
    }
}
