/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *
 */
package org.unidata.mdm.meta.service.impl.data.validation;

/**
 * @author Mikhail Mikhailov
 *         Types of the model objects.
 */
public enum DataModelElementType {
    /**
     * Entities group.
     * Group tree is stored separately from entities and lookup entities.
     */
    ENTITIES_GROUP("entitiesGroup"),
    /**
     * Lookup entity.
     */
    LOOKUP("lookup"),
    /**
     * Nested entity.
     */
    NESTED("nested"),
    /**
     * Top level entity.
     */
    REGISTER("register"),
    /**
     * Relation.
     */
    RELATION("relation");
    /**
     * Tag and also an element name.
     */
    private final String tag;
    /**
     * Constructor.
     *
     * @param tag name of the tag
     */
    private DataModelElementType(String tag) {
        this.tag = tag;
    }
    /**
     * @return the tag
     */
    public String getTag() {
        return tag;
    }
}
