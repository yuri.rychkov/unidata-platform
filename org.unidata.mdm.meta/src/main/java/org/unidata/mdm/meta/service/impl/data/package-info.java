/**
 * @author Mikhail Mikhailov on Dec 1, 2020
 * Types related to DATA subsystem.
 */
package org.unidata.mdm.meta.service.impl.data;