package org.unidata.mdm.meta.dto;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.core.dto.AbstractModelResult;
import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.util.ModelUtils;

/**
 * @author Mikhail Mikhailov on Dec 18, 2020
 */
public class GetEnumerationsResult extends AbstractModelResult {
    /**
     * Enumerations.
     */
    private List<GetEnumerationResult> enumerations;
    /**
     * Constructor.
     */
    public GetEnumerationsResult() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return TypeIds.ENUMERATIONS_MODEL;
    }
    /**
     * @return the enumerations
     */
    public List<GetEnumerationResult> getEnumerations() {
        return Objects.isNull(enumerations) ? Collections.emptyList() : enumerations;
    }
    /**
     * @param enumerations the enumerations to set
     */
    public void setEnumerations(List<GetEnumerationResult> enumerations) {
        this.enumerations = enumerations;
    }
}
