package org.unidata.mdm.meta.type.model.enumeration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.core.type.model.source.AbstractCustomPropertiesHolder;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
/**
 * @author Mikhail Mikhailov on Oct 6, 2020
 * 'Type' is here just to prevent clashes with JDK {@link Enumeration} type.
 */
public class EnumerationType extends AbstractCustomPropertiesHolder<EnumerationType> implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 3686433617834019278L;

    @JacksonXmlProperty(localName = "value")
    protected List<EnumerationValue> values;

    @JacksonXmlProperty(isAttribute = true)
    protected String name;

    @JacksonXmlProperty(isAttribute = true)
    protected String displayName;

    public List<EnumerationValue> getValues() {
        if (values == null) {
            values = new ArrayList<>();
        }
        return this.values;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }

    public EnumerationType withValues(EnumerationValue... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            Collections.addAll(getValues(), values);
        }
        return this;
    }

    public EnumerationType withValues(Collection<EnumerationValue> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getValues().addAll(values);
        }
        return this;
    }

    public EnumerationType withName(String value) {
        setName(value);
        return this;
    }

    public EnumerationType withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }
}
