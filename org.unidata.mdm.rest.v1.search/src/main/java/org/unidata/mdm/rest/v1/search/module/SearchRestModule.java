package org.unidata.mdm.rest.v1.search.module;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.unidata.mdm.rest.v1.search.type.rendering.SearchRestInputRenderingAction;
import org.unidata.mdm.rest.v1.search.type.rendering.SearchRestOutputRenderingAction;
import org.unidata.mdm.system.type.module.AbstractModule;
import org.unidata.mdm.system.type.module.Dependency;
import org.unidata.mdm.system.type.rendering.RenderingAction;

/**
 * @author Mikhail Mikhailov on Mar 5, 2020
 */
public class SearchRestModule extends AbstractModule {

    public static final String MODULE_ID = "org.unidata.mdm.rest.v1.search";

    private static final Collection<Dependency> DEPENDENCIES = Arrays.asList(
            new Dependency("org.unidata.mdm.core", "6.0"),
            new Dependency("org.unidata.mdm.search", "6.0"),
            new Dependency("org.unidata.mdm.rest.system", "6.0"));

    private static final List<RenderingAction> RENDERING_ACTIONS
        = ListUtils.union(
                Arrays.asList(SearchRestInputRenderingAction.values()),
                Arrays.asList(SearchRestOutputRenderingAction.values()));

    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return MODULE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getVersion() {
        return "6.0";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "Search REST";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "Search REST interfaces.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Dependency> getDependencies() {
        return DEPENDENCIES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<RenderingAction> getRenderingActions() {
        return RENDERING_ACTIONS;
    }
}
