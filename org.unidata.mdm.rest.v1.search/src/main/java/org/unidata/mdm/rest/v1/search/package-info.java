/**
 * REST search api
 *
 * @author Alexandr Serov
 * @since 02.12.2020
 **/
package org.unidata.mdm.rest.v1.search;