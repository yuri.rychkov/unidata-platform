/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.search.type.mapping.impl;

/**
 * @author Mikhail Mikhailov on Oct 9, 2019
 */
public abstract class AbstractValueMappingField<X extends AbstractValueMappingField<X>> extends AbstractMappingField<X> {
    /**
     * Index as doc value.
     */
    private boolean docValue;
    /**
     * The 'null' value.
     */
    private Object defaultValue;
    /**
     * Store, but don't index the field, if false.
     * True by default.
     */
    private boolean indexed = true;
    /**
     * Constructor.
     * @param name
     */
    protected AbstractValueMappingField(String name) {
        super(name);
    }
    /**
     * @return the docValue
     */
    public boolean isDocValue() {
        return docValue;
    }
    /**
     * @param docValue the docValue to set
     */
    public void setDocValue(boolean docValue) {
        this.docValue = docValue;
    }
    /**
     * @return the defaultValue
     */
    public Object getDefaultValue() {
        return defaultValue;
    }
    /**
     * @param defaultValue the defaultValue to set
     */
    public void setDefaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
    }
    /**
     * @return the indexed
     */
    public boolean isIndexed() {
        return indexed;
    }
    /**
     * @param indexed the indexed to set
     */
    public void setIndexed(boolean indexed) {
        this.indexed = indexed;
    }
    /**
     * Sets field's storage to doc value.
     * @param docValue the flag
     * @return self
     */
    public X withDocValue(boolean docValue) {
        setDocValue(docValue);
        return self();
    }
    /**
     * @param defaultValue the defaultValue to set
     */
    public X withDefaultValue(Object defaultValue) {
        setDefaultValue(defaultValue);
        return self();
    }
    /**
     * @param value the value to set
     */
    public X withIndexed(boolean value) {
        setIndexed(value);
        return self();
    }
}
