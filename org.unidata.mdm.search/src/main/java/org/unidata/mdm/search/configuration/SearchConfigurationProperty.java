/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.search.configuration;

import org.unidata.mdm.search.module.SearchModule;
import org.unidata.mdm.system.type.configuration.ConfigurationProperty;

/**
 * @author Alexander Malyshev
 */
public final class SearchConfigurationProperty {
    /**
     * No-instance constructor.
     */
    private SearchConfigurationProperty() {
        super();
    }
    /**
     * Search cluster property.
     * Former unidata.search.cluster.name
     */
    public static final ConfigurationProperty<String> SEARCH_CLUSTER_NAME = ConfigurationProperty.string()
            .key(SearchConfigurationConstants.PROPERTY_CLUSTER_NAME)
            .groupKey(SearchConfigurationConstants.PROPERTY_MAIN_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .required(true)
            .readOnly(true)
            .build();
    /**
     * Search node(s) - names or IPs.
     * Former unidata.search.nodes.addresses
     */
    public static final ConfigurationProperty<String> SEARCH_CLUSTER_NODES = ConfigurationProperty.string()
            .key(SearchConfigurationConstants.PROPERTY_CLUSTER_NODES)
            .groupKey(SearchConfigurationConstants.PROPERTY_MAIN_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .required(true)
            .readOnly(true)
            .build();
    /**
     * Index prefix property name.
     * Former unidata.search.index.prefix
     */
    public static final ConfigurationProperty<String> SEARCH_INDEX_PREFIX = ConfigurationProperty.string()
            .key(SearchConfigurationConstants.PROPERTY_INDEX_PREFIX)
            .groupKey(SearchConfigurationConstants.PROPERTY_MAIN_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .defaultValue(SearchConfigurationConstants.DEFAULT_INDEX_PREFIX)
            .required(true)
            .readOnly(true)
            .build();
    /**
     * Admin action timeout.
     * Former unidata.elastic.admin.action.timeout
     */
    public static final ConfigurationProperty<Long> SEARCH_ADMIN_ACTION_TIMEOUT = ConfigurationProperty.integer()
            .key(SearchConfigurationConstants.PROPERTY_ADMIN_ACTION_TIMEOUT)
            .groupKey(SearchConfigurationConstants.PROPERTY_MAIN_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .defaultValue(Long.valueOf(5000L))
            .required(true)
            .readOnly(true)
            .build();
    /**
     * Refresh mode. Not used directly by SearchService.
     * Former unidata.data.refresh.immediate
     */
    public static final ConfigurationProperty<Boolean> SEARCH_REFRESH_IMMEDIATE = ConfigurationProperty.bool()
            .key(SearchConfigurationConstants.PROPERTY_REFRESH_IMMEDIATE)
            .groupKey(SearchConfigurationConstants.PROPERTY_MAIN_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .defaultValue(Boolean.TRUE)
            .required(true)
            .readOnly(false)
            .build();
    /**
     * Number of default shards property name.
     * Former unidata.search.shards.number
     */
    public static final ConfigurationProperty<Long> SEARCH_SHARDS_NUMBER = ConfigurationProperty.integer()
            .key(SearchConfigurationConstants.PROPERTY_SHARDS_NUMBER)
            .groupKey(SearchConfigurationConstants.PROPERTY_MAIN_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .defaultValue(SearchConfigurationConstants.DEFAULT_NUMBER_OF_SHARDS)
            .required(true)
            .readOnly(false)
            .build();
    /**
     * Number of replicas property name.
     * Former unidata.search.replicas.number
     */
    public static final ConfigurationProperty<Long> SEARCH_REPLICAS_NUMBER = ConfigurationProperty.integer()
            .key(SearchConfigurationConstants.PROPERTY_REPLICAS_NUMBER)
            .groupKey(SearchConfigurationConstants.PROPERTY_MAIN_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .defaultValue(SearchConfigurationConstants.DEFAULT_NUMBER_OF_REPLICAS)
            .required(true)
            .readOnly(true)
            .build();
    /**
     * Index prefix property name.
     * Limit number of returned hits to this value.
     * Former unidata.search.total.count.limit
     */
    public static final ConfigurationProperty<Long> SEARCH_HITS_LIMIT = ConfigurationProperty.integer()
            .key(SearchConfigurationConstants.PROPERTY_HITS_LIMIT)
            .groupKey(SearchConfigurationConstants.PROPERTY_MAIN_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .defaultValue(SearchConfigurationConstants.DEFAULT_NUMBER_OF_HITS)
            .required(true)
            .readOnly(false)
            .build();
    /**
     * Number of fields per index property name.
     * Former unidata.search.fields.limit
     */
    public static final ConfigurationProperty<Long> SEARCH_FIELDS_LIMIT = ConfigurationProperty.integer()
            .key(SearchConfigurationConstants.PROPERTY_FIELDS_LIMIT)
            .groupKey(SearchConfigurationConstants.PROPERTY_MAIN_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .defaultValue(SearchConfigurationConstants.DEFAULT_NUMBER_OF_FIELDS)
            .required(true)
            .readOnly(false)
            .build();
    /**
     * Fuzziness flag.
     * Former unidata.search.fuzziness
     */
    public static final ConfigurationProperty<String> SEARCH_FUZZINESS = ConfigurationProperty.string()
            .key(SearchConfigurationConstants.PROPERTY_FUZZINESS)
            .groupKey(SearchConfigurationConstants.PROPERTY_FUZZINESS_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .defaultValue("1")
            .required(true)
            .readOnly(false)
            .build();
    /**
     * Fuzziness prefix length.
     * Former unidata.search.fuzziness.prefix.length
     */
    public static final ConfigurationProperty<Long> SEARCH_FUZZINESS_PREFIX_LENGTH = ConfigurationProperty.integer()
            .key(SearchConfigurationConstants.PROPERTY_FUZZINESS_PREFIX_LENGTH)
            .groupKey(SearchConfigurationConstants.PROPERTY_FUZZINESS_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .defaultValue(Long.valueOf(4L))
            .required(true)
            .readOnly(false)
            .build();
    /**
     * Support fizzeness with wildcard or not
     * Former unidata.search.fuzzy_with_wildcard
     */
    public static final ConfigurationProperty<Boolean> SEARCH_FUZZINESS_WITH_WILDCARD = ConfigurationProperty.bool()
            .key(SearchConfigurationConstants.PROPERTY_FUZZINESS_WITH_WILDCARD)
            .groupKey(SearchConfigurationConstants.PROPERTY_FUZZINESS_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .defaultValue(Boolean.TRUE)
            .required(true)
            .readOnly(false)
            .build();
    /**
     * Default min score value.
     * Former unidata.search.default.min.score
     */
    public static final ConfigurationProperty<Double> SEARCH_DEFAULT_MIN_SCORE = ConfigurationProperty.number()
            .key(SearchConfigurationConstants.PROPERTY_DEFAULT_MIN_SCORE)
            .groupKey(SearchConfigurationConstants.PROPERTY_SCORE_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .defaultValue(Double.valueOf(0))
            .required(true)
            .readOnly(false)
            .build();
    /**
     * Calculate score value.
     * Former unidata.search.calculate.score
     */
    public static final ConfigurationProperty<Boolean> SEARCH_CALCULATE_SCORE = ConfigurationProperty.bool()
            .key(SearchConfigurationConstants.PROPERTY_CALCULATE_SCORE)
            .groupKey(SearchConfigurationConstants.PROPERTY_SCORE_GROUP)
            .moduleId(SearchModule.MODULE_ID)
            .defaultValue(Boolean.TRUE)
            .required(true)
            .readOnly(false)
            .build();
    /**
     * Values as array.
     */
    private static final ConfigurationProperty<?>[] VALUES = {
            SEARCH_ADMIN_ACTION_TIMEOUT,
            SEARCH_CLUSTER_NAME,
            SEARCH_CLUSTER_NODES,
            SEARCH_DEFAULT_MIN_SCORE,
            SEARCH_CALCULATE_SCORE,
            SEARCH_FIELDS_LIMIT,
            SEARCH_FUZZINESS,
            SEARCH_FUZZINESS_PREFIX_LENGTH,
            SEARCH_FUZZINESS_WITH_WILDCARD,
            SEARCH_HITS_LIMIT,
            SEARCH_INDEX_PREFIX,
            SEARCH_REPLICAS_NUMBER,
            SEARCH_SHARDS_NUMBER,
            SEARCH_REFRESH_IMMEDIATE
    };
    /**
     * Enum like array accessor.
     * @return array of values.
     */
    public static ConfigurationProperty<?>[] values() {
        return VALUES;
    }
}
