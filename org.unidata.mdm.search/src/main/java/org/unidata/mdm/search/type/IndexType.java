/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.search.type;

import javax.annotation.Nonnull;

/**
 * Types in index
 */
public interface IndexType {
    /**
     * @return name of type
     */
    @Nonnull
    String getName();
    /**
     * @param searchType - search type
     * @return true, if search types are related
     */
    boolean isRelated(IndexType searchType);
    /**
     * Tells whether this type is a hierarchical one.
     * @return true, if so, false otherwise.
     */
    default boolean isHierarchical() {
        return false;
    }
    /**
     * Casts self to HIT. Provokes CCE, if this type is not a hierarchical one.
     * @return HIT
     */
    default HierarchicalIndexType toHierarchical() {
        return isHierarchical() ? (HierarchicalIndexType) this : null;
    }
    /**
     * Checks this type for being one of the given types.
     * @param types the types to check
     * @return true, if so, false otherwise
     */
    default boolean isOneOf(IndexType... types) {
        for (int i = 0; i < types.length; i++) {
            if (this.equals(types[i])) {
                return true;
            }
        }
        return false;
    }
}
