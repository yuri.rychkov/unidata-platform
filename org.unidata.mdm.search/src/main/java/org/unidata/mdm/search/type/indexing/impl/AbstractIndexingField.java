/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.search.type.indexing.impl;

import org.unidata.mdm.search.type.indexing.IndexingField;

/**
 * @author Mikhail Mikhailov on Oct 7, 2019
 * Name.
 */
public abstract class AbstractIndexingField<X extends AbstractIndexingField<X>> implements IndexingField {
    /**
     * The name.
     */
    private final String name;
    /**
     * Constructor.
     */
    protected AbstractIndexingField(String name) {
        super();
        this.name = name;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return name;
    }
    /**
     * Self cast.
     * @return
     */
    @SuppressWarnings("unchecked")
    protected X self() {
        return (X) this;
    }
}
