/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.search.util;

import java.text.ParseException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.Nullable;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unidata.mdm.search.context.SearchRequestContext;
import org.unidata.mdm.search.dto.SearchResultHitDTO;
import org.unidata.mdm.search.dto.SearchResultHitFieldDTO;
import org.unidata.mdm.search.exception.SearchExceptionIds;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Mikhail Mikhailov
 *         Various constants and utilities.
 */
public class SearchUtils {
    private static final FastDateFormat DEFAULT_TIMESTAMP_WITH_OFFSET
        = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSS", TimeZone.getTimeZone("UTC"));
    /**
     * Date format without milliseconds.
     * Frontend specific.
     * usage DateTimeFormatter DEFAULT_TIMESTAMP_SYSTEM_DEFAULT_NEW
     */
    @Deprecated
    public static final FastDateFormat DEFAULT_TIMESTAMP_SYSTEM_DEFAULT
        = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSS");
    /**
     * Date format without milliseconds.
     * Frontend specific.
     */
    public static final DateTimeFormatter DEFAULT_TIMESTAMP_SYSTEM_DEFAULT_NEW
            = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
    /**
     * File name capable format.
     */
    private static final FastDateFormat DEFAULT_INDEX_FORMAT = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", TimeZone.getTimeZone("UTC"));
    /**
     * Default max expansions value for phrase_prefix queries.
     */
    public static final int DEFAULT_MAX_EXPANSIONS_VALUE = 50;
    /**
     * Default slop value for phrase_prefix queries.
     */
    public static final int DEFAULT_SLOP_VALUE = 10;

    public static final String DEFAULT_TIMESTAMP_TARGET_FORMAT = "strict_date_optional_time||epoch_millis";
    /**
     * JUD min '-292275055-05-16T16:47:04.192Z'
     * Min from.
     */
    public static final String ES_MIN_FROM = "-292275054-01-01T00:00:00.000Z";
    /**
     * JUD max '+292278994-08-17T07:12:55.807Z'
     * Max to.
     */
    public static final String ES_MAX_TO = "+292278993-12-31T23:59:59.999Z";
    /**
     * Elastic min date
     */
    public static final Date ES_MIN_DATE = Date.from(Instant.parse(ES_MIN_FROM));
    /**
     * Elastic max date
     */
    public static final Date ES_MAX_DATE = Date.from(Instant.parse(ES_MAX_TO));
    /**
     * Cluster name setting.
     */
    public static final String ES_CLUSTER_NAME_SETTING = "cluster.name";
    /**
     * Number of shards configuration property.
     */
    public static final String ES_NUMBER_OF_SHARDS_SETTING = "number_of_shards";
    /**
     * Number of replicas configuration property.
     */
    public static final String ES_NUMBER_OF_REPLICAS_SETTING = "number_of_replicas";
    /**
     * Max result window property.
     */
    public static final String ES_MAX_RESULT_WINDOW = "max_result_window";
    /**
     * Number of fields per index.
     */
    public static final String ES_LIMIT_OF_TOTAL_FIELDS = "index.mapping.total_fields.limit";
    /**
     * Comma separator for various fields.
     */
    public static final String COMMA_SEPARATOR = ",";
    /**
     * Colon separator for various fields.
     */
    public static final String COLON_SEPARATOR = ":";
    /**
     * Pipe separator for various fields.
     */
    public static final String PIPE_SEPARATOR = "|";
    /**
     * Fields delimiter.
     */
    public static final String FIELDS_DELIMITER = "\\|";
    /**
     * Fields delimiter.
     */
    public static final String ALL_FIELD = "_all";
    /**
     * Raw, not analyzed, field value.
     */
    public static final String NAN_FIELD = "$nan";
    /**
     * Raw, analyzed field without n-gramm.
     */
    public static final String DEFAULT_SEARCH_ANALIZED_FIELD = "$default";
    /**
     * Morphologically analyzed, field value (string).
     */
    public static final String MORPH_FIELD = "$morph";
    /**
     * Separator for complex type (example: field.$nan)
     */
    public static final String DOT = ".";
    /**
     * $ - Prefix for all system fields
     */
    public static final String DOLLAR = "$";
    /**
     * Underscore - index name parts delimiter.
     */
    public static final String UNDERSCORE = "_";
    /**
     * The "type" filtering field.
     */
    public static final String TYPE_FIELD_NAME = "$t";
    /**
     * Default unidata analyzer name.
     */
    public static final String DEFAULT_STRING_ANALYZER_NAME = "unidata_default_analyzer";
    /**
     * Default morphological analyzer name.
     */
    public static final String MORPH_STRING_ANALYZER_NAME = "unidata_morph_analyzer";

    public static final String LOWERCASE_STRING_NORMALIZER_NAME = "unidata_lowercase_normalizer";

    public static final String SEARCH_STRING_ANALYZER_NAME = "unidata_search_analyzer";
    /**
     * Standard ES analyzer name.
     */
    public static final String STANDARD_STRING_ANALYZER_NAME = "standard";

    public static final String UNIDATA_CUSTOM_SIMILARITY = "default";
    /**
     * No analyzer value.
     */
    public static final String NONE_STRING_ANALYZER_NAME = "not_analyzed";
    /**
     * No indexed name.
     */
    public static final String NO_INDEXED_NAME = "no";
    /**
     * parent field
     */
    public static final String PARENT_FIELD = "_parent";
    /**
     * parent field
     */
    public static final String ID_FIELD = "_id";

    /**
     * Default fields param, if nothing is set ('_all').
     */
    private static final List<String> DEFAULT_FIELDS_VALUE = Arrays.asList(ALL_FIELD);
    /**
     * Default empty facets list.
     */
    private static final List<String> DEFAULT_FACETS_VALUE = new ArrayList<>();
    /**
     * Logger for this class.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchUtils.class);
    /**
     * No instance of this class allowed.
     */
    private SearchUtils() {
        super();
    }

    /**
     * FIXME Remove!
     * Colaesce from date.
     * @param ts the time stamp
     * @return timestamp
     */
    public static Date coalesceFrom(Date ts) {
        return ts != null ? ts : ES_MIN_DATE;
    }
    /**
     * FIXME Remove!
     * Colaesce to date.
     * @param ts the time stamp
     * @return timestamp
     */
    public static Date coalesceTo(Date ts) {
        return ts != null ? ts : ES_MAX_DATE;
    }

    public static String parseForIndex(Object date) {
        if (date == null) {
            return null;
        }
        return DEFAULT_INDEX_FORMAT.format(date);
    }

    public static Date parseFromIndex(Object date) {
        if (date == null) {
            return null;
        }

        try {
            return DEFAULT_INDEX_FORMAT.parse(date.toString());
        } catch (ParseException e) {
            LOGGER.error("Can't parse date", e);
            return null;
        }
    }

    public static boolean isSystemField(String fieldName) {
        return fieldName == null || fieldName.startsWith(DOLLAR);
    }
    /**
     * Extract return fields.
     *
     * @param ctx the context
     * @return fields
     */
    public static String[] extractReturnFields(final SearchRequestContext ctx) {

        if (ctx.isSource() || CollectionUtils.isEmpty(ctx.getReturnFields())) {
            return ArrayUtils.EMPTY_STRING_ARRAY;
        }

        Set<String> result = new HashSet<>(ctx.getReturnFields());
        return result.toArray(new String[result.size()]);
    }

    /**
     * Gets the fields as list, setting _all, if nothing is specified.
     *
     * @param fields fields, delimited by '|' character
     * @return list of fields
     */
    public static List<String> getFields(String fields) {
        if (StringUtils.isBlank(fields)) {
            return SearchUtils.DEFAULT_FIELDS_VALUE;
        }

        return Arrays.asList(fields.split(SearchUtils.FIELDS_DELIMITER));
    }

    /**
     * Gets requested facets as list.
     *
     * @param facets the facets,  delimited by '|' character
     * @return list of facets or null
     */
    public static List<String> getFacets(String facets) {
        if (StringUtils.isBlank(facets)) {
            return SearchUtils.DEFAULT_FACETS_VALUE;
        }

        return Arrays.asList(facets.split(SearchUtils.FIELDS_DELIMITER));
    }

    /**
     * Parses string representation of date according to date format from
     * {@see DEFAULT_TIMESTAMP_NO_OFFSET}.
     *
     * @param dateAsString string representation of date.
     * @return parsed date.
     */
    public static Date parse(String dateAsString) {

        Date result;
        if (ES_MIN_FROM.equals(dateAsString) || ES_MAX_TO.equals(dateAsString)) {
            result = null;
        } else {
            try {
                result = dateAsString != null ? DEFAULT_TIMESTAMP_WITH_OFFSET.parse(dateAsString) : null;
            } catch (ParseException e) {
                throwCannotParseDate(dateAsString);
                // Trick the sonar. Won't ever happen.
                result = null;
            }
        }

        return result;
    }

    /**
     * Can be used after hit modifier to extract correct system date
     * TODO refactoring remove this ulgy method
     * @param dateAsString
     * @return
     */
    public static Date parseWithoutOffset(String dateAsString) {

        Date result;
        if (ES_MIN_FROM.equals(dateAsString) || ES_MAX_TO.equals(dateAsString)) {
            result = null;
        } else {
            try {
                result = dateAsString != null ? DEFAULT_TIMESTAMP_SYSTEM_DEFAULT.parse(dateAsString) : null;
            } catch (ParseException e) {
                throwCannotParseDate(dateAsString);
                // Trick the sonar. Won't ever happen.
                result = null;
            }
        }

        return result;
    }

    /**
     * Parses string representation of date according to date format from
     * {@see DEFAULT_TIMESTAMP_NO_OFFSET}.
     *
     * @param dateAsString string representation of date.
     * @return parsed date.
     */
    public static String formatForUI(String dateAsString) {
        Date result;
        if (ES_MIN_FROM.equals(dateAsString) || ES_MAX_TO.equals(dateAsString)) {
            result = null;
        } else {
            try {
                result = dateAsString != null ? DEFAULT_TIMESTAMP_WITH_OFFSET.parse(dateAsString) : null;
            } catch (ParseException e) {
                throwCannotParseDate(dateAsString);
                // Trick the sonar. Won't ever happen.
                result = null;
            }
        }
        return result == null ? null : DEFAULT_TIMESTAMP_SYSTEM_DEFAULT_NEW.format(ConvertUtils.date2LocalDateTime(result));
    }
    // FIXME Remove!
    @Nullable
    public static Range<Date> getDateRange(SearchResultHitDTO hit, String fieldFrom, String fieldTo) {

        SearchResultHitFieldDTO to = hit.getFieldValue(fieldFrom);
        SearchResultHitFieldDTO from = hit.getFieldValue(fieldTo);

        try {

            Date validTo = to == null || to.isNullField() ? SearchUtils.ES_MAX_DATE : new DateTime(to.getFirstValue().toString()).toDate();
            Date validFrom = from == null || from.isNullField() ? SearchUtils.ES_MIN_DATE : new DateTime(from.getFirstValue().toString()).toDate();
            return Range.between(validFrom, validTo);

        } catch (Exception e) {
            return null;
        }
    }


    public static Date getDateForDisplayAttributes(Date now, Date validFrom, Date validTo) {
        Date result = validFrom;
        if (validFrom != null) {
            if (validTo != null) {
                if (validFrom.getTime() <= now.getTime() && validTo.getTime() >= now.getTime()) {
                    result = now;
                }
            } else if (validFrom.getTime() <= now.getTime()) {
                result = now;
            }
        } else {
            if (validTo != null) {
                if (validTo.getTime() >= now.getTime()) {
                    result = now;
                }
            } else {
                result = now;
            }
        }
        if (result == null) {
            // UN-8625
            result = DateUtils.addDays(validTo, -1);
        }
        return result;
    }

    public static boolean dateInPeriod(Date dateForCheck, Date from, Date to) {
        if (dateForCheck == null) {
            return from == null;
        }
        if (from == null) {
            if (to == null) {
                return true;
            } else {
                return dateForCheck.getTime() <= to.getTime();
            }
        } else {
            if (to == null) {
                return dateForCheck.getTime() >= from.getTime();
            } else {
                return dateForCheck.getTime() <= to.getTime() && dateForCheck.getTime() >= from.getTime();
            }
        }
    }

    private static void throwCannotParseDate(String dateAsString) {
        throw new PlatformFailureException(
                "Incorrect date format found, unable to parse date string!",
                SearchExceptionIds.EX_SEARCH_CANNOT_PARSE_DATE,
                dateAsString);
    }
}
