/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.search.type.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

/**
 * Class responsible for grouping form fields by logical operation.
 * It is used for creation research requests.
 */
public class FieldsGroup {
    /**
     * Collection of form fields
     */
    @Nonnull
    private final Collection<FormField> formFields;
    /**
     * Children.
     */
    private List<FieldsGroup> childGroups;
    /**
     * Type of group
     */
    @Nonnull
    private final GroupType groupType;
    /**
     * Constructor
     *
     * @param joinType
     * @param formFields
     */
    private FieldsGroup(@Nonnull GroupType joinType, @Nonnull Collection<FormField> formFields) {
        this.groupType = joinType;
        this.formFields = formFields;
    }

    /**
     * Create group form field which will be compose over logical 'AND'
     *
     * @param formField - logical group of form fields for searching.
     * @return AND group
     */
    @Nonnull
    public static FieldsGroup and(@Nonnull FormField... formField) {
        Collection<FormField> fieldList = Arrays.stream(formField)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        return new FieldsGroup(GroupType.AND, fieldList);
    }

    /**
     * Create group form field which will be compose over logical 'AND'
     *
     * @param formFields - logical group of form fields for searching.
     * @return AND group
     */
    @Nonnull
    public static FieldsGroup and(@Nonnull Collection<FormField> formFields) {
        Collection<FormField> fieldList = formFields.stream().filter(Objects::nonNull).collect(Collectors.toList());
        return new FieldsGroup(GroupType.AND, fieldList);
    }

    /**
     * Create group form field which will be compose over logical 'AND'
     *
     * @return AND group
     */
    @Nonnull
    public static FieldsGroup and() {
        return new FieldsGroup(GroupType.AND, new ArrayList<>());
    }

    /**
     * Create group form fields which will be compose over logical 'OR'
     *
     * @param formFields - logical group of form fields for searching.
     */
    public static FieldsGroup or(@Nonnull Collection<FormField> formFields) {
        Collection<FormField> fieldList = formFields.stream().filter(Objects::nonNull).collect(Collectors.toList());
        return new FieldsGroup(GroupType.OR, fieldList);
    }

    /**
     * Create group form fields which will be compose over logical 'OR'
     *
     * @param formFields - logical group of form fields for searching.
     */
    public static FieldsGroup or(@Nonnull FormField...formFields) {
        Collection<FormField> fieldList = Arrays.stream(formFields).filter(Objects::nonNull).collect(Collectors.toList());
        return new FieldsGroup(GroupType.OR, fieldList);
    }

    /**
     * @return empty OR group
     */
    public static FieldsGroup or() {
        return new FieldsGroup(GroupType.OR, new ArrayList<>());
    }

    /**
     * @param fields - form field
     * @return - self
     */
    public FieldsGroup add(FormField... fields) {
        for (int i = 0; ArrayUtils.isNotEmpty(fields) && i < fields.length; i++) {
            formFields.add(fields[i]);
        }
        return this;
    }
    /**
     * Adds a collection of fields to this group.
     * @param fields fields collection
     * @return self
     */
    public FieldsGroup addFields(Collection<FormField> fields) {
        if (CollectionUtils.isNotEmpty(fields)) {
            add(fields.toArray(FormField[]::new));
        }
        return this;
    }

    public FieldsGroup add(FieldsGroup... groups) {

        for (int i = 0; ArrayUtils.isNotEmpty(groups) && i < groups.length; i++) {
            if (childGroups == null) {
                childGroups = new ArrayList<>();
            }
            childGroups.add(groups[i]);
        }

        return this;
    }

    public FieldsGroup addGroups(Collection<FieldsGroup> groups) {
        if (CollectionUtils.isNotEmpty(groups)) {
            add(groups.toArray(FieldsGroup[]::new));
        }
        return this;
    }

    /**
     * @return true if group is empty
     */
    public boolean isEmpty() {
        return CollectionUtils.isEmpty(formFields) && CollectionUtils.isEmpty(childGroups);
    }
    /**
     * Gets the fields of this group.
     * @return fields
     */
    @Nonnull
    public Collection<FormField> getFields() {
        return formFields;
    }
    /**
     * Gets children (sub-) groups of this group.
     * @return children
     */
    public List<FieldsGroup> getGroups() {
        return childGroups;
    }
    /**
     * Gets the type of this group - either {@linkplain GroupType#AND} or {@linkplain GroupType#OR}.
     * @return type
     */
    @Nonnull
    public GroupType getType() {
        return groupType;
    }
    /**
     * Group type.
     */
    public enum GroupType {
        OR, AND;
    }

}
