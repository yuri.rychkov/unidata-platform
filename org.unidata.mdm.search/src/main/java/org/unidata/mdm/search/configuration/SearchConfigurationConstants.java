/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.search.configuration;

import org.unidata.mdm.search.module.SearchModule;

/**
 * @author Mikhail Mikhailov on Oct 8, 2019
 * Search config labeles, both dynamic and static.
 */
public final class SearchConfigurationConstants {
    /**
     * Search properties group.
     */
    public static final String PROPERTY_MAIN_GROUP = SearchModule.MODULE_ID + ".main.group";
    // Config properties
    /**
     * Search node(s) - names or IPs.
     * Former unidata.search.nodes.addresses
     */
    public static final String PROPERTY_CLUSTER_NODES = SearchModule.MODULE_ID + ".cluster.nodes";
    /**
     * Search cluster property.
     * Former unidata.search.cluster.name
     */
    public static final String PROPERTY_CLUSTER_NAME = SearchModule.MODULE_ID + ".cluster.name";
    /**
     * Index prefix property name.
     * Former unidata.search.index.prefix
     */
    public static final String PROPERTY_INDEX_PREFIX = SearchModule.MODULE_ID + ".index.prefix";
    /**
     * Number of default shards property name.
     * Former unidata.search.shards.number
     */
    public static final String PROPERTY_SHARDS_NUMBER = SearchModule.MODULE_ID + ".shards.number";
    /**
     * Number of replicas property name.
     * Former unidata.search.replicas.number
     */
    public static final String PROPERTY_REPLICAS_NUMBER = SearchModule.MODULE_ID + ".replicas.number";
    /**
     * Refresh mode. Not used directly by SearchService.
     * Former unidata.data.refresh.immediate
     */
    public static final String PROPERTY_REFRESH_IMMEDIATE = SearchModule.MODULE_ID + ".refresh.immediate";
    /**
     * Index prefix property name.
     * Limit number of returned hits to this value.
     * Former unidata.search.total.count.limit
     */
    public static final String PROPERTY_HITS_LIMIT = SearchModule.MODULE_ID + ".hits.limit";
    /**
     * Number of fields per index property name.
     * Former unidata.search.fields.limit
     */
    public static final String PROPERTY_FIELDS_LIMIT = SearchModule.MODULE_ID + ".fields.limit";
    /**
     * Admin action timeout.
     * Former unidata.elastic.admin.action.timeout
     */
    public static final String PROPERTY_ADMIN_ACTION_TIMEOUT = SearchModule.MODULE_ID + ".admin.action.timeout";
    /**
     * Search fuzziness properties group.
     */
    public static final String PROPERTY_FUZZINESS_GROUP = SearchModule.MODULE_ID + ".fuzziness.group";
    /**
     * Fuzziness flag.
     * Former unidata.search.fuzziness
     */
    public static final String PROPERTY_FUZZINESS = SearchModule.MODULE_ID + ".fuzziness";
    /**
     * Fuzziness prefix length.
     * Former unidata.search.fuzziness.prefix.length
     */
    public static final String PROPERTY_FUZZINESS_PREFIX_LENGTH = SearchModule.MODULE_ID + ".fuzziness.prefix.length";
    /**
     * Support fizzeness with wildcard or not
     * Former unidata.search.fuzzy_with_wildcard
     */
    public static final String PROPERTY_FUZZINESS_WITH_WILDCARD = SearchModule.MODULE_ID + ".fuzziness.with.wildcard";
    /**
     * Search score properties group.
     */
    public static final String PROPERTY_SCORE_GROUP = SearchModule.MODULE_ID + ".score.group";
    /**
     * Default min score value.
     * Former unidata.search.default.min.score
     */
    public static final String PROPERTY_DEFAULT_MIN_SCORE = SearchModule.MODULE_ID + ".default.min.score";
    /**
     * Calculate score or not.
     * Former unidata.search.calculate.score
     */
    public static final String PROPERTY_CALCULATE_SCORE = SearchModule.MODULE_ID + ".calculate.score";
    // END OFConfig properties
    // Defaults
    /**
     * Default prefix.
     */
    public static final String DEFAULT_INDEX_PREFIX = "default";
    /**
     * Default prefix.
     */
    public static final String DEFAULT_INDEX_TYPE = "default";
    /**
     * Default ES port.
     */
    public static final String DEFAULT_PORT_VALUE = "9200";
    /**
     * Default number of shards.
     */
    public static final long DEFAULT_NUMBER_OF_SHARDS = 1L;
    /**
     * Default number of replicas.
     */
    public static final long DEFAULT_NUMBER_OF_REPLICAS = 0;
    /**
     * Default number of fields per index.
     */
    public static final long DEFAULT_NUMBER_OF_FIELDS = 5000L;
    /**
     * Default number of hits to return - the window size.
     */
    public static final long DEFAULT_NUMBER_OF_HITS = 200000L;
    /**
     * Constructor.
     */
    private SearchConfigurationConstants() {
        super();
    }
}
