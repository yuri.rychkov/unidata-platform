/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.search.type.indexing;

import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.search.type.indexing.impl.IndexingRecordImpl;

/**
 * @author Mikhail Mikhailov on Oct 10, 2019
 * The record (fields container) interface.
 */
public interface IndexingRecord {
    /**
     * Checks this record for being empty.
     * @return true, if empty, false otherwise
     */
    default boolean isEmpty() {
        return CollectionUtils.isEmpty(getFields());
    }
    /**
     * @return the fields
     */
    List<IndexingField> getFields();
    /**
     * Tells whether this is a top-level indexing record ({@link Indexing} type).
     * @return true for top level, false otherwise
     */
    default boolean isIndexing() {
        return false;
    }
    /**
     * Gets self as {@link Indexing} view or returns null, if it is not supported.
     * @return {@link Indexing} or null
     */
    @Nullable
    default Indexing toIndexing() {
        return isIndexing() ? (Indexing) this : null;
    }

    static IndexingRecord of(IndexingField... fields) {
        return new IndexingRecordImpl(fields);
    }

    static IndexingRecord of(Collection<IndexingField> fields) {
        return new IndexingRecordImpl(fields);
    }

    static IndexingRecord of(Supplier<Collection<IndexingField>> fields) {
        return new IndexingRecordImpl(fields);
    }
}
