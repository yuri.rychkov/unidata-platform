/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.ro;

import java.util.List;

import org.unidata.mdm.rest.system.ro.SimpleDataType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Michael Yashin. Created on 29.05.2015.
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleAttributeRO extends CodeAttributeRO {
    /**
     * Possible enum type.
     */
    protected String enumDataType;
    /**
     * Possible lookup entity type.
     */
    protected String lookupEntityType;
    /**
     * Type of code attribute values.
     */
    private SimpleDataType lookupEntityCodeAttributeType;
    /**
     * Alternative display attributes
     */
    private List<String> lookupEntityDisplayAttributes;

    /**
     * Dictionary provided values as @String
     */
    private String dictionaryDataType;

    /**
     * Lookup search attributes
     */
    private List<String> lookupEntitySearchAttributes;

    /**
     * Show attr name or not.
     */
    private boolean useAttributeNameForDisplay;
    /**
     * External link template definition.
     */
    protected String linkDataType;
    /**
     * value id
     */
    private String valueId;
    /**
     * default unit id
     */
    private String defaultUnitId;
    /**
     * Attribute order.
     */
    protected int order;
    /**
     * This _STRING_ attribute supports morphological search.
     */
    protected boolean searchMorphologically;
    /**
     * This _STRING_ attribute supports case insensitive search
     */
    protected boolean searchCaseInsensitive;
    /**
     * @return the searchMorphologically
     */
    public boolean isSearchMorphologically() {
        return searchMorphologically;
    }
    /**
     * @param searchMorphologically the searchMorphologically to set
     */
    public void setSearchMorphologically(boolean searchMorphologically) {
        this.searchMorphologically = searchMorphologically;
    }

    @Override
    @JsonProperty()
    public SimpleDataType getSimpleDataType() {
        return super.getSimpleDataType();
    }

    @Override
    @JsonProperty()
    public void setSimpleDataType(SimpleDataType simpleDataType) {
        super.setSimpleDataType(simpleDataType);
    }

    public String getEnumDataType() {
        return enumDataType;
    }

    public void setEnumDataType(String enumDataType) {
        this.enumDataType = enumDataType;
    }

    public String getLookupEntityType() {
        return lookupEntityType;
    }

    public void setLookupEntityType(String lookupEntityType) {
        this.lookupEntityType = lookupEntityType;
    }

    public String getDictionaryDataType() {
        return dictionaryDataType;
    }

    public void setDictionaryDataType(String dictionaryDataType) {
        this.dictionaryDataType = dictionaryDataType;
    }

    /**
     * @return the lookupEntityCodeAttributeType
     */
    public SimpleDataType getLookupEntityCodeAttributeType() {
        return lookupEntityCodeAttributeType;
    }

    /**
     * @param lookupEntityCodeAttributeType the lookupEntityCodeAttributeType to set
     */
    public void setLookupEntityCodeAttributeType(SimpleDataType lookupEntityCodeAttributeType) {
        this.lookupEntityCodeAttributeType = lookupEntityCodeAttributeType;
    }

    /**
     * @return the linkDataType
     */
    public String getLinkDataType() {
        return linkDataType;
    }

    /**
     * @param linkDataType the linkDataType to set
     */
    public void setLinkDataType(String linkDataType) {
        this.linkDataType = linkDataType;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getValueId() {
        return valueId;
    }

    public void setValueId(String valueId) {
        this.valueId = valueId;
    }

    public String getDefaultUnitId() {
        return defaultUnitId;
    }

    public void setDefaultUnitId(String defaultUnitId) {
        this.defaultUnitId = defaultUnitId;
    }

    public List<String> getLookupEntityDisplayAttributes() {
        return lookupEntityDisplayAttributes;
    }

    public void setLookupEntityDisplayAttributes(List<String> lookupEntityDisplayAttributes) {
        this.lookupEntityDisplayAttributes = lookupEntityDisplayAttributes;
    }

    public List<String> getLookupEntitySearchAttributes() {
        return lookupEntitySearchAttributes;
    }

    public void setLookupEntitySearchAttributes(List<String> lookupEntitySearchAttributes) {
        this.lookupEntitySearchAttributes = lookupEntitySearchAttributes;
    }

    /**
     * @return the useAttributeNameForDisplay
     */
    public boolean isUseAttributeNameForDisplay() {
        return useAttributeNameForDisplay;
    }

    /**
     * @param useAttributeNameForDisplay the useAttributeNameForDisplay to set
     */
    public void setUseAttributeNameForDisplay(boolean useAttributeNameForDisplay) {
        this.useAttributeNameForDisplay = useAttributeNameForDisplay;
    }


    public boolean isSearchCaseInsensitive() {
        return searchCaseInsensitive;
    }

    public void setSearchCaseInsensitive(boolean searchCaseInsensitive) {
        this.searchCaseInsensitive = searchCaseInsensitive;
    }
}
