/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.rest.meta.converter;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.rest.meta.ro.NestedEntityRO;

/**
 * @author Mikhail Mikhailov on Oct 22, 2020
 */
public class NestedEntityConverter extends AbstractEntityDefinitionConverter {
    /**
     * Constructor.
     */
    private NestedEntityConverter() {
        super();
    }

    public static NestedEntityRO to(NestedEntity source) {

        if (Objects.isNull(source)) {
            return null;
        }

        NestedEntityRO target = new NestedEntityRO();

        toAbstractEntityData(source, target);
        target.getSimpleAttributes().addAll(toSimpleAttrs(source.getSimpleAttribute(), source.getName()));
        target.getArrayAttributes().addAll(toArrayAttrs(source.getArrayAttribute(), source.getName()));
        target.getComplexAttributes().addAll(to(source.getComplexAttribute(), Collections.emptyList(), source.getName()));

        return target;
    }

    public static NestedEntity from(NestedEntityRO source) {

        if (Objects.isNull(source)) {
            return null;
        }

        NestedEntity target = new NestedEntity();
        Map<String, NestedEntity> nestedEntities = new HashMap<>();

        fromAbstractEntityData(source, target);

        SimpleAttributeDefConverter.copySimpleAttributeDataList(source.getSimpleAttributes(), target.getSimpleAttribute());
        ArrayAttributeDefConverter.copySimpleAttributeDataList(source.getArrayAttributes(), target.getArrayAttribute());
        AbstractEntityDefinitionConverter.fromComplexAttributeDataList(source.getComplexAttributes(), target.getComplexAttribute(), nestedEntities);

        return target;
    }
}
