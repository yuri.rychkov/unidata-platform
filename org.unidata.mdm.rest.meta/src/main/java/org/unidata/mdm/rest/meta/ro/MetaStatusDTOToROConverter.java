package org.unidata.mdm.rest.meta.ro;


import org.unidata.mdm.meta.type.input.meta.MetaStatus;

/**
 * The Class MetaStatusDTOToROConverter.
 * 
 * @author ilya.bykov
 */
public class MetaStatusDTOToROConverter {

	private MetaStatusDTOToROConverter() {
	}

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the meta status RO
	 */
	public static MetaStatusRO convert(MetaStatus source) {
		if (source == null) {
			return null;
		}
		return MetaStatusRO.valueOf(source.name());
	}
}
