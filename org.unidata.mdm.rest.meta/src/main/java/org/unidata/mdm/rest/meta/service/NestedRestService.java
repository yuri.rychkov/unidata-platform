/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.service;

import java.util.Collections;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.meta.context.GetDataModelContext;
import org.unidata.mdm.meta.context.UpsertDataModelContext;
import org.unidata.mdm.meta.dto.GetModelDTO;
import org.unidata.mdm.meta.dto.GetNestedEntitiesResult;
import org.unidata.mdm.rest.meta.converter.NestedEntityConverter;
import org.unidata.mdm.rest.meta.ro.NestedEntityRO;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.UpdateResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * Relation rest service
 */
@Path(NestedRestService.SERVICE_PATH)
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class NestedRestService extends AbstractRestService {
    /**
     * Service path.
     */
    public static final String SERVICE_PATH = "nested";
    /**
     * The meta model service.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * Find all.
     *
     * @return the response
     */
    @GET
    @Operation(
        description = "Gets nested list.",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(array = @ArraySchema(schema = @Schema(implementation = NestedEntityRO.class))), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response findAll(
            @Parameter(description = "The draft id for drafts. Optional.", in = ParameterIn.QUERY) @QueryParam("draftId") @DefaultValue("0") long draftId) {

        GetModelDTO result = metaModelService.get(GetDataModelContext.builder()
                .allNestedEntities(true)
                .draftId(draftId > 0 ? draftId : null)
                .build());

        return ok(result.getNested().stream()
                .map(GetNestedEntitiesResult::getNested)
                .map(NestedEntityConverter::to)
                .collect(Collectors.toList()));
    }
    /**
     * Find a particular nested element.
     *
     * @return the response
     */
    @GET
    @Path("{name}")
    @Operation(
        description = "Gets nested entity element by name.",
        method = HttpMethod.GET,
        parameters = {
                @Parameter(name = "name", description = "Nested entity name. Required.", in = ParameterIn.PATH),
                @Parameter(name = "draftId", description = "Draft id. Optional.", in = ParameterIn.QUERY)
        },
        responses = {
            @ApiResponse(content = @Content(array = @ArraySchema(schema = @Schema(implementation = NestedEntityRO.class))), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response find(@PathParam("name") String name, @QueryParam("draftId") @DefaultValue("0") long draftId) {

        GetModelDTO result = metaModelService.get(GetDataModelContext.builder()
                .nestedEntityIds(Collections.singletonList(name))
                .draftId(draftId > 0 ? draftId : null)
                .build());

        NestedEntityRO retval = null;
        if (CollectionUtils.isNotEmpty(result.getNested()) && result.getNested().size() == 1) {
            retval = NestedEntityConverter.to(result.getNested().get(0).getNested());
        }

        return ok(retval);
    }
    /**
     * Upsert.
     *
     * @param relations
     *            the relations
     * @param name
     *            the name
     * @return the response
     */
    @PUT
    @Path("{name}")
    @Operation(
        description = "Add new/update existing nested entity.",
        method = HttpMethod.PUT,
        parameters = {
                @Parameter(name = "name", description = "Relation name.", in = ParameterIn.PATH),
                @Parameter(name = "draftId", description = "Draft id. Optional.", in = ParameterIn.QUERY)
        },
        requestBody = @RequestBody(content = @Content(array = @ArraySchema(schema = @Schema(implementation = NestedEntityRO.class))), description = "Upsert request."),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response upsert(NestedEntityRO payload, @PathParam("name") String name, @QueryParam("draftId") @DefaultValue("0") long draftId) {

        metaModelService.upsert(UpsertDataModelContext.builder()
                .nestedEntitiesUpdate(NestedEntityConverter.from(payload))
                .draftId(draftId > 0 ? draftId : null)
                .build());

        return ok(new UpdateResponse(true, name));
    }

    /**
     * Delete.
     *
     * @param name
     *            the name
     * @return the response
     */
    @DELETE
    @Path("{name}")
    @Operation(
        description = "Remove nested entity.",
        method = HttpMethod.DELETE,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = String.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response delete(
            @Parameter(in = ParameterIn.PATH) @PathParam("name") String name,
            @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY) @QueryParam("draftId") @DefaultValue("0") long draftId) {

        metaModelService.upsert(UpsertDataModelContext.builder()
                .nestedEntitiesDelete(name)
                .draftId(draftId > 0 ? draftId : null)
                .build());

        return ok(true);
    }
}
