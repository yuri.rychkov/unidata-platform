/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter.graph;

import org.unidata.mdm.meta.type.input.meta.MetaGraph;

import org.unidata.mdm.rest.meta.ro.MetaGraphRO;

/**
 * The Class MetaGraphDTOToROConverter.
 * @author ilya.bykov
 */
public class MetaGraphDTOToROConverter {

    private MetaGraphDTOToROConverter() {
    }

    /**
     * Convert.
     *
     * @param source the source
     * @return the meta graph RO
     */
    public static MetaGraphRO convert(MetaGraph source) {
        if (source == null) {
            return null;
        }
        MetaGraphRO target = new MetaGraphRO(
                source.getId(),
                source.getFileName(),
                MetaVertexDTOToROConverter.convert(source.vertexSet()),
                MetaEdgeDTOToROConverter.convert(source.edgeSet()),
                source.isImportRoles(),
                source.isImportUsers()
        );
        target.setOverride(source.isOverride());
        return target;
    }
}
