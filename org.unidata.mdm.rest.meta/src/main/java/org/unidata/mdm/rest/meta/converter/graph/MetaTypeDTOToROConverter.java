/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter.graph;

import org.unidata.mdm.meta.type.input.meta.MetaType;

import org.unidata.mdm.rest.meta.ro.MetaTypeRO;

/**
 * The Class MetaTypeDTOToROConverter.
 * 
 * @author ilya.bykov
 */
public class MetaTypeDTOToROConverter {

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the meta type RO
	 */
	public static MetaTypeRO convert(MetaType source) {
		if (source == null) {
			return null;
		}
		MetaTypeRO target = MetaTypeRO.valueOf(source.name());
		return target;
	}

}
