package org.unidata.mdm.rest.meta.ro.references;

/**
 * @author Mikhail Mikhailov on Jun 3, 2020
 */
public class ReferenceType {
    /**
     * The name.
     */
    private String name;
    /**
     * And the description.
     */
    private String description;
    /**
     * Constructor.
     */
    public ReferenceType() {
        super();
    }
    /**
     * Constructor.
     */
    public ReferenceType(String name, String description) {
        super();
        this.name = name;
        this.description = description;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the decsription
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param decsription the decsription to set
     */
    public void setDescription(String decsription) {
        this.description = decsription;
    }
}
