package org.unidata.mdm.rest.meta.ro;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.unidata.mdm.meta.type.input.meta.MetaType;


/**
 * The Class MetaTypeROToDTOConverter.
 * 
 * @author ilya.bykov
 */
public class MetaTypeROToDTOConverter {

	private MetaTypeROToDTOConverter() {
	}

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the list
	 */
	public static Set<MetaType> convert(List<MetaTypeRO> source) {
		if (source == null) {
			return null;
		}
		Set<MetaType> target = new HashSet<>();
		for (MetaTypeRO metaTypeRo : source) {
			target.add(convert(metaTypeRo));
		}
		return target;
	}

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the meta type
	 */
	public static MetaType convert(MetaTypeRO source) {
		if (source == null) {
			return null;
		}
		return MetaType.valueOf(source.name());
	}
}
