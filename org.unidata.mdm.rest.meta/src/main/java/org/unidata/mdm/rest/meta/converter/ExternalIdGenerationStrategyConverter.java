/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter;

import java.util.Objects;

import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.strategy.ConcatenatedValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.strategy.CustomValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.strategy.RandomValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.strategy.SequenceValueGenerationStrategy;
import org.unidata.mdm.rest.meta.ro.ConcatExternalIdGenerationStrategyRO;
import org.unidata.mdm.rest.meta.ro.CustomExternalIdGenerationStrategyRO;
import org.unidata.mdm.rest.meta.ro.ExternalIdGenerationStrategyRO;
import org.unidata.mdm.rest.meta.ro.RandomExternalIdGenerationStrategyRO;
import org.unidata.mdm.rest.meta.ro.SequenceExternalIdGenerationStrategyRO;

/**
 * @author Mikhail Mikhailov
 * Converter.
 */
public class ExternalIdGenerationStrategyConverter {
    /**
     * Constructor.
     */
    private ExternalIdGenerationStrategyConverter() {
        super();
    }
    /**
     * From JAXB to REST.
     * @param source the source to convert
     * @return REST object
     */
    public static ExternalIdGenerationStrategyRO to(ValueGenerationStrategy source) {

        if (source == null) {
            return null;
        }

        switch (source.getStrategyType()) {
        case CONCAT:
            ConcatenatedValueGenerationStrategy concatSource = (ConcatenatedValueGenerationStrategy) source;
            ConcatExternalIdGenerationStrategyRO concatTarget = new ConcatExternalIdGenerationStrategyRO();
            concatTarget.setSeparator(concatSource.getSeparator());
            concatTarget.setAttributes(concatSource.getAttributes());
            return concatTarget;
        case RANDOM:
            return new RandomExternalIdGenerationStrategyRO();
        case SEQUENCE:
            return new SequenceExternalIdGenerationStrategyRO();
        case CUSTOM:
            CustomExternalIdGenerationStrategyRO strategy = new CustomExternalIdGenerationStrategyRO();
            CustomValueGenerationStrategy customSource = (CustomValueGenerationStrategy) source;
            strategy.setClassName(customSource.getClassName());
            strategy.setStrategyId(customSource.getClassName());
            return strategy;
        default:
            break;
        }

        return null;
    }
    /**
     * From REST to JAXB.
     * @param source the source to convert
     * @return JAXB object
     */
    public static ValueGenerationStrategy from(ExternalIdGenerationStrategyRO source) {

        if (source == null) {
            return null;
        }

        switch (source.getStrategyType()) {
        case CONCAT:
            ConcatenatedValueGenerationStrategy concatTarget = new ConcatenatedValueGenerationStrategy();
//                = MetaJaxbUtils.getMetaObjectFactory().createConcatenatedValueGenerationStrategyDef();
            ConcatExternalIdGenerationStrategyRO concatSource = (ConcatExternalIdGenerationStrategyRO) source;
            concatTarget.setSeparator(concatSource.getSeparator());
            concatTarget.getAttributes().addAll(concatSource.getAttributes());
            return concatTarget;
        case RANDOM:
//            return MetaJaxbUtils.getMetaObjectFactory().createRandomValueGenerationStrategyDef();
            return new RandomValueGenerationStrategy();
        case SEQUENCE:
//            return MetaJaxbUtils.getMetaObjectFactory().createSequenceValueGenerationStrategyDef();
            return new SequenceValueGenerationStrategy();
        case CUSTOM:
            CustomExternalIdGenerationStrategyRO custom = ((CustomExternalIdGenerationStrategyRO) source);
            CustomValueGenerationStrategy customTarget = new CustomValueGenerationStrategy();
            customTarget.setClassName(Objects.isNull(custom.getClassName()) ? custom.getStrategyId() : custom.getClassName());
            return customTarget;
        default:
            break;
        }

        return null;
    }
}
