/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 *
 */
package org.unidata.mdm.rest.meta.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.meta.dto.GetEntitiesWithRelationsDTO;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.rest.meta.ro.EntityRO;
import org.unidata.mdm.rest.meta.ro.RelationRO;

/**
 * @author Mikhail Mikhailov
 *
 */
public class EntitiesDefFilteredByRelationSideConverter {

    /**
     * Constructor.
     */
    private EntitiesDefFilteredByRelationSideConverter() {
        super();
    }

    /**
     * Converter method.
     * @param dto the dto to convert
     * @return list of entity definitions
     */
    public static List<EntityRO> convert(GetEntitiesWithRelationsDTO dto) {

        if (dto == null || MapUtils.isEmpty(dto.getEntities())) {
            return Collections.emptyList();
        }

        List<EntityRO> result = new ArrayList<>();
        for (Entry<Entity, Pair<List<NestedEntity>, List<Relation>>> e
                : dto.getEntities().entrySet()) {

            Entity source = e.getKey();
            EntityRO target = new EntityRO();
            List<NestedEntity> refs = e.getValue().getLeft();

            AbstractEntityDefinitionConverter.toAbstractEntityData(source, target);
            target.getSimpleAttributes().addAll(
                    AbstractEntityDefinitionConverter.toSimpleAttrs(source.getSimpleAttribute(), source.getName()));
            target.getArrayAttributes().addAll(
                    AbstractEntityDefinitionConverter.toArrayAttrs(source.getArrayAttribute(), source.getName()));
            target.getComplexAttributes().addAll(
                    AbstractEntityDefinitionConverter.to(source.getComplexAttribute(), refs, source.getName()));

            target.setDashboardVisible(source.isDashboardVisible());
            target.setMergeSettings(MergeSettingsConverter.to(source.getMergeSettings()));

            List<RelationRO> relations = new ArrayList<>();
            for (Relation relationDef : e.getValue().getRight()) {
                relations.add(RelationDefConverter.convert(relationDef));
            }

            target.setRelations(relations);

            result.add(target);
        }

        return result;
    }
}
