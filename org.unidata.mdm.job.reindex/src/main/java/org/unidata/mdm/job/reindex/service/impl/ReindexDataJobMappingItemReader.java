/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.job.reindex.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Value;
import org.unidata.mdm.core.service.job.JobCommonParameters;

/**
 * @author Mikhail Mikhailov
 * Update data mapping item reader.
 */
@StepScope
public class ReindexDataJobMappingItemReader implements ItemReader<String> {
    /**
     * Entity names.
     */
    @Value("#{stepExecutionContext[" + JobCommonParameters.PARAM_ENTITY_NAME + "]}")
    private String entityNamesAsString;
    /**
     * Work of this reader.
     */
    private List<String> work = new ArrayList<>();
    /**
     * Constructor.
     */
    public ReindexDataJobMappingItemReader() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String read() throws Exception {

        if (work.isEmpty() && StringUtils.isNotBlank(entityNamesAsString)) {
            work.addAll(Arrays.asList(StringUtils.split(entityNamesAsString, "|")));
        }

        if (!work.isEmpty()) {

            String retval = work.remove(work.size() - 1);
            if (work.isEmpty()) {
                entityNamesAsString = null;
            }

            return retval;
        }

        return null;
    }

}
