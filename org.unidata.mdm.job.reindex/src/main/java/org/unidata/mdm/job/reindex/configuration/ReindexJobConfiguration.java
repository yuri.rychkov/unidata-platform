/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.job.reindex.configuration;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

import javax.sql.DataSource;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.partition.PartitionHandler;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.StepLocator;
import org.springframework.batch.integration.partition.BeanFactoryStepLocator;
import org.springframework.batch.integration.partition.StepExecutionRequest;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.support.PassThroughItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.messaging.Message;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.service.impl.job.JobDescriptorFactoryBean;
import org.unidata.mdm.core.service.job.JobCommonParameters;
import org.unidata.mdm.core.service.job.JobFairPartitionHandler;
import org.unidata.mdm.core.service.job.JobStepExecutionRequestHandler;
import org.unidata.mdm.core.type.job.JobParameterDescriptor;
import org.unidata.mdm.core.type.keys.LSN;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.job.reindex.module.ReindexJobModule;
import org.unidata.mdm.job.reindex.service.impl.ReindexDataJobDataItemProcessor;
import org.unidata.mdm.job.reindex.service.impl.ReindexDataJobDataItemReader;
import org.unidata.mdm.job.reindex.service.impl.ReindexDataJobDataItemWriter;
import org.unidata.mdm.job.reindex.service.impl.ReindexDataJobDataPartitioner;
import org.unidata.mdm.job.reindex.service.impl.ReindexDataJobDataStepExecutionListener;
import org.unidata.mdm.job.reindex.service.impl.ReindexDataJobListener;
import org.unidata.mdm.job.reindex.service.impl.ReindexDataJobMappingItemReader;
import org.unidata.mdm.job.reindex.service.impl.ReindexDataJobMappingItemWriter;
import org.unidata.mdm.job.reindex.service.impl.ReindexDataJobMappingPartitioner;
import org.unidata.mdm.job.reindex.service.impl.ReindexDataJobPrepareItemWriter;
import org.unidata.mdm.job.reindex.service.impl.ReindexDataJobPreparePartitioner;
import org.unidata.mdm.job.reindex.service.impl.ReindexDataJobResetItemWriter;
import org.unidata.mdm.job.reindex.service.impl.ReindexDataJobResetPartitioner;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.system.configuration.AbstractConfiguration;
import org.unidata.mdm.system.service.TextService;
import org.unidata.mdm.system.type.batch.BatchSetAccumulator;
import org.unidata.mdm.system.type.pipeline.connection.PipelineConnection;
import org.unidata.mdm.system.type.runtime.MeasurementContextName;
import org.unidata.mdm.system.type.touch.Touch;

import com.hazelcast.collection.IQueue;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.topic.ITopic;

/**
 * @author Mikhail Mikhailov on Dec 17, 2019
 */
@Configuration
public class ReindexJobConfiguration extends AbstractConfiguration {
    /**
     * Connect reindex data pipeline.
     */
    public static final Touch<PipelineConnection> TOUCH_REINDEX_PIPELINE_CONNECT
        = Touch.builder(PipelineConnection.class)
            .touchName("[touch-reindex-pipeline-connect]")
            .paramType("step-name", String.class)
            .paramType("step-execution", StepExecution.class)
            .build();
    /**
     * Has register data.
     */
    public static final Touch<Void> TOUCH_REINDEX_DATA_ACCUMULATE
        = Touch.builder(Void.class)
            .touchName("[touch-reindex-data-accumulate]")
            .paramType("step-name", String.class)
            .paramType("step-execution", StepExecution.class)
            .paramType("batch-set-accumulator", BatchSetAccumulator.class)
            .build();
    /**
     * All selector mark.
     */
    private static final String ALL = "ALL";
    /**
     * Job's display name.
     */
    private static final String REINDEX_JOB_DISPLAY_NAME = ReindexJobModule.MODULE_ID + ".job.display.name";
    /**
     * Job's description.
     */
    private static final String REINDEX_JOB_DESCRIPTION = ReindexJobModule.MODULE_ID + ".job.description";
    /**
     * Param 'jobUser' display name.
     */
    private static final String REINDEX_PARAM_JOB_USER_DISPLAY_NAME = ReindexJobModule.MODULE_ID + ".param.job.user.display.name";
    /**
     * Param 'jobUser' description.
     */
    private static final String REINDEX_PARAM_JOB_USER_DESCRIPTION = ReindexJobModule.MODULE_ID + ".param.job.user.description";
    /**
     * Param 'blockSize' display name.
     */
    private static final String REINDEX_PARAM_BLOCK_SIZE_DISPLAY_NAME = ReindexJobModule.MODULE_ID + ".param.block.size.display.name";
    /**
     * Param 'blockSize' description.
     */
    private static final String REINDEX_PARAM_BLOCK_SIZE_DESCRIPTION = ReindexJobModule.MODULE_ID + ".param.block.size.description";
    /**
     * Param 'reindexTypes' display name.
     */
    private static final String REINDEX_PARAM_REINDEX_TYPES_DISPLAY_NAME = ReindexJobModule.MODULE_ID + ".param.reindex.types.display.name";
    /**
     * Param 'reindexTypes' description.
     */
    private static final String REINDEX_PARAM_REINDEX_TYPES_DESCRIPTION = ReindexJobModule.MODULE_ID + ".param.reindex.types.description";
    /**
     * Param 'updateMappings' display name.
     */
    private static final String REINDEX_PARAM_UPDATE_MAPPINGS_DISPLAY_NAME = ReindexJobModule.MODULE_ID + ".param.update.mappings.display.name";
    /**
     * Param 'updateMappings' description.
     */
    private static final String REINDEX_PARAM_UPDATE_MAPPINGS_DESCRIPTION = ReindexJobModule.MODULE_ID + ".param.update.mappings.description";
    /**
     * Param 'cleanIndexes' display name.
     */
    private static final String REINDEX_PARAM_CLEAN_INDEXES_DISPLAY_NAME = ReindexJobModule.MODULE_ID + ".param.clean.indexes.display.name";
    /**
     * Param 'cleanIndexes' description.
     */
    private static final String REINDEX_PARAM_CLEAN_INDEXES_DESCRIPTION = ReindexJobModule.MODULE_ID + ".param.clean.indexes.description";
    /**
     * Param 'reindexRecords' display name.
     */
    private static final String REINDEX_PARAM_REINDEX_RECORDS_DISPLAY_NAME = ReindexJobModule.MODULE_ID + ".param.reindex.records.name";
    /**
     * Param 'reindexRecords' description.
     */
    private static final String REINDEX_PARAM_REINDEX_RECORDS_DESCRIPTION = ReindexJobModule.MODULE_ID + ".param.reindex.records.description";
    /**
     * Param 'writeIdLog' display name.
     */
    private static final String REINDEX_PARAM_WRITE_ID_LOG_DISPLAY_NAME = ReindexJobModule.MODULE_ID + ".param.write.id.log.name";
    /**
     * Param 'writeIdLog' description.
     */
    private static final String REINDEX_PARAM_WRITE_ID_LOG_DESCRIPTION = ReindexJobModule.MODULE_ID + ".param.write.id.log.description";
    /**
     * Param 'processIdLog' display name.
     */
    private static final String REINDEX_PARAM_PROCESS_ID_LOG_DISPLAY_NAME = ReindexJobModule.MODULE_ID + ".param.process.id.log.name";
    /**
     * Param 'processIdLog' description.
     */
    private static final String REINDEX_PARAM_PROCESS_ID_LOG_DESCRIPTION = ReindexJobModule.MODULE_ID + ".param.process.id.log.description";
    /**
     * Param 'skipDeafultReport' display name.
     */
    private static final String REINDEX_PARAM_SKIP_DEFAULT_REPORT_DISPLAY_NAME = ReindexJobModule.MODULE_ID + ".param.skip.default.report.name";
    /**
     * Param 'skipDeafultReport' description.
     */
    private static final String REINDEX_PARAM_SKIP_DEFAULT_REPORT_DESCRIPTION = ReindexJobModule.MODULE_ID + ".param.skip.default.report.description";
    /**
     * The id.
     */
    private static final ConfigurationId ID = () -> "REINDEX_JOB_CONFIGURATION";

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private MetaModelService metaModelService;

    /**
     * {@inheritDoc}
     */
    @Override
    protected ConfigurationId getId() {
        return ID;
    }

    private Map<String, String> entitiesSelector() {

        Map<String, String> entities = new LinkedHashMap<>();

        entities.put(ALL, "All entities");

        metaModelService.instance(Descriptors.DATA).getRegisters().forEach(el -> entities.put(el.getName(), el.getDisplayName()));
        metaModelService.instance(Descriptors.DATA).getLookups().forEach(el -> entities.put(el.getName(), el.getDisplayName()));

        return entities;
    }

    @Bean("reindexDataJobDescriptor")
    public JobDescriptorFactoryBean reindexDataJobDescriptor(TextService textService) {

        final JobParameterDescriptor<?>[] params = {
                JobParameterDescriptor.string()
                    .name(JobCommonParameters.PARAM_JOB_USER)
                    .displayName(() -> textService.getText(REINDEX_PARAM_JOB_USER_DISPLAY_NAME))
                    .description(() -> textService.getText(REINDEX_PARAM_JOB_USER_DESCRIPTION))
                    .build(),
                JobParameterDescriptor.integer()
                    .name(JobCommonParameters.PARAM_BLOCK_SIZE)
                    .displayName(() -> textService.getText(REINDEX_PARAM_BLOCK_SIZE_DISPLAY_NAME))
                    .description(() -> textService.getText(REINDEX_PARAM_BLOCK_SIZE_DESCRIPTION))
                    .singleValidator(v -> Objects.isNull(v) || v.longValue() > 0)
                    .defaultSingle(Long.valueOf(1024L))
                    .build(),
                JobParameterDescriptor.strings()
                    .name(ReindexJobConfigurationConstants.PARAM_REINDEX_TYPES)
                    .displayName(() -> textService.getText(REINDEX_PARAM_REINDEX_TYPES_DISPLAY_NAME))
                    .description(() -> textService.getText(REINDEX_PARAM_REINDEX_TYPES_DESCRIPTION))
                    .defaultSingle(ALL)
                    .selector(this::entitiesSelector)
                    .build(),
                JobParameterDescriptor.bool()
                    .name(ReindexJobConfigurationConstants.PARAM_UPDATE_MAPPINGS)
                    .displayName(() -> textService.getText(REINDEX_PARAM_UPDATE_MAPPINGS_DISPLAY_NAME))
                    .description(() -> textService.getText(REINDEX_PARAM_UPDATE_MAPPINGS_DESCRIPTION))
                    .defaultSingle(Boolean.FALSE)
                    .build(),
                JobParameterDescriptor.bool()
                    .name(ReindexJobConfigurationConstants.PARAM_CLEAN_INDEXES)
                    .displayName(() -> textService.getText(REINDEX_PARAM_CLEAN_INDEXES_DISPLAY_NAME))
                    .description(() -> textService.getText(REINDEX_PARAM_CLEAN_INDEXES_DESCRIPTION))
                    .defaultSingle(Boolean.FALSE)
                    .build(),
                JobParameterDescriptor.bool()
                    .name(ReindexJobConfigurationConstants.PARAM_REINDEX_RECORDS)
                    .displayName(() -> textService.getText(REINDEX_PARAM_REINDEX_RECORDS_DISPLAY_NAME))
                    .description(() -> textService.getText(REINDEX_PARAM_REINDEX_RECORDS_DESCRIPTION))
                    .defaultSingle(Boolean.TRUE)
                    .build(),
                JobParameterDescriptor.bool()
                    .name(ReindexJobConfigurationConstants.PARAM_WRITE_ID_LOG)
                    .displayName(() -> textService.getText(REINDEX_PARAM_WRITE_ID_LOG_DISPLAY_NAME))
                    .description(() -> textService.getText(REINDEX_PARAM_WRITE_ID_LOG_DESCRIPTION))
                    .defaultSingle(Boolean.FALSE)
                    .build(),
                JobParameterDescriptor.bool()
                    .name(ReindexJobConfigurationConstants.PARAM_PROCESS_ID_LOG)
                    .displayName(() -> textService.getText(REINDEX_PARAM_PROCESS_ID_LOG_DISPLAY_NAME))
                    .description(() -> textService.getText(REINDEX_PARAM_PROCESS_ID_LOG_DESCRIPTION))
                    .defaultSingle(Boolean.FALSE)
                    .build(),
                JobParameterDescriptor.bool()
                    .name(ReindexJobConfigurationConstants.PARAM_SKIP_DEFAULT_REPORT)
                    .displayName(() -> textService.getText(REINDEX_PARAM_SKIP_DEFAULT_REPORT_DISPLAY_NAME))
                    .description(() -> textService.getText(REINDEX_PARAM_SKIP_DEFAULT_REPORT_DESCRIPTION))
                    .defaultSingle(Boolean.FALSE)
                    .build()
        };

        JobDescriptorFactoryBean fb = new JobDescriptorFactoryBean();
        fb.setJobName(ReindexJobConfigurationConstants.JOB_NAME);
        fb.setTouch(TOUCH_REINDEX_PIPELINE_CONNECT, TOUCH_REINDEX_DATA_ACCUMULATE);
        fb.setDisplayName(() -> textService.getText(REINDEX_JOB_DISPLAY_NAME));
        fb.setDescription(() -> textService.getText(REINDEX_JOB_DESCRIPTION));
        fb.setModular(true);
        fb.setParameter(params);

        return fb;
    }

    @Bean
    public StepLocator stepLocator(ApplicationContext applicationContext) {
        BeanFactoryStepLocator bfsl = new BeanFactoryStepLocator();
        bfsl.setBeanFactory(applicationContext);
        return bfsl;
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_DATA_QUEUE)
    public IQueue<Message<StepExecutionRequest>> reindexDataQueue() {
        return hazelcastInstance.getQueue(ReindexJobConfigurationConstants.BEAN_NAME_DATA_QUEUE);
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_MODEL_TOPIC)
    public ITopic<Message<StepExecutionRequest>> reindexMetaTopic() {
        return hazelcastInstance.getTopic(ReindexJobConfigurationConstants.BEAN_NAME_MODEL_TOPIC);
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_MAPPPING_EXECUTOR)
    public SimpleAsyncTaskExecutor
        mappingAsyncTaskExecutor(@Value("${" + ReindexJobConfigurationConstants.PROP_NAME_MAPPING_CONCURRENCY + ":4}") int concurrency) {
        SimpleAsyncTaskExecutor executor = new SimpleAsyncTaskExecutor(ReindexJobConfigurationConstants.BEAN_NAME_MAPPPING_EXECUTOR + "-");
        executor.setConcurrencyLimit(concurrency);
        return executor;
    }

    // -------------------------------------------------------
    // LISTENERS
    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_JOB_EXEC_LISTENER)
    @JobScope
    public JobExecutionListener reindexDataJobListener(
            @Value("${" + ReindexJobConfigurationConstants.PROP_NAME_JOB_DESCRIPTION_CODE + ":Reindex job}") String descriptionCode) {
        ReindexDataJobListener l = new ReindexDataJobListener();
        l.setJobDescription(descriptionCode);
        return l;
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_DATA_STEP_EXEC_LISTENER)
    @StepScope
    public StepExecutionListener reindexDataJobDataStepExecutionListener() {
        ReindexDataJobDataStepExecutionListener l = new ReindexDataJobDataStepExecutionListener();
        l.setContextName(MeasurementContextName.MEASURE_STEP_REINDEX);
        return l;
    }

    // END OF LISTENERS
    // -------------------------------------------------------
    // STEP EXECUTOR
    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_STEP_EXEC_HANDLER)
    public JobStepExecutionRequestHandler reindexRemoteStepExecutionRequestHandler(
            JobExplorer jobExplorer,
            JobRepository jobRepository,
            StepLocator jobStepLocator,
            @Qualifier(ReindexJobConfigurationConstants.BEAN_NAME_DATA_QUEUE) IQueue<Message<StepExecutionRequest>> reindexQueue,
            @Qualifier(ReindexJobConfigurationConstants.BEAN_NAME_MODEL_TOPIC) ITopic<Message<StepExecutionRequest>> reindexTopic,
            @Value("${" + ReindexJobConfigurationConstants.PROP_NAME_DATA_REINDEX_THREADS + ":2}") int concurrency) {

        JobStepExecutionRequestHandler handler = new JobStepExecutionRequestHandler();
        handler.setJobExplorer(jobExplorer);
        handler.setJobRepository(jobRepository);
        handler.setJobName(ReindexJobConfigurationConstants.JOB_NAME);
        handler.setStepLocator(jobStepLocator);
        handler.setQueue(reindexQueue);
        handler.setTopic(reindexTopic);
        handler.setThreadCount(concurrency);
        return handler;
    }
    // END OF STEP EXECUTOR
    // -------------------------------------------------------
    // DATA ITEMS
    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_DATA_PARTITION_HANDLER)
    public PartitionHandler reindexDataJobDataPartitionHandler(
            @Qualifier("coreDataSource") DataSource coreDataSource,
            JobExplorer jobExplorer,
            @Qualifier(ReindexJobConfigurationConstants.BEAN_NAME_DATA_QUEUE) BlockingQueue<Message<StepExecutionRequest>> reindexDataJobDataQueue) {

        JobFairPartitionHandler handler = new JobFairPartitionHandler();
        handler.setStepName(ReindexJobConfigurationConstants.BEAN_NAME_DATA_STEP);
        handler.setDataSource(coreDataSource);
        handler.setJobExplorer(jobExplorer);
        handler.setGridSize(8);
        handler.setMessagingGateway(reindexDataJobDataQueue);
        handler.setGrouped(false);
        return handler;
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_DATA_PARTITIONER)
    @JobScope
    public Partitioner reindexDataJobDataPartitioner() {
        return new ReindexDataJobDataPartitioner();
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_DATA_ITEM_READER)
    @StepScope
    public ReindexDataJobDataItemReader reindexDataJobDataItemReader() {
        return new ReindexDataJobDataItemReader();
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_DATA_ITEM_PROCESSOR)
    @StepScope
    public ReindexDataJobDataItemProcessor reindexDataJobDataItemProcessor() {
        return new ReindexDataJobDataItemProcessor();
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_DATA_ITEM_WRITER)
    @StepScope
    public ReindexDataJobDataItemWriter reindexDataJobDataItemWriter() {
        return new ReindexDataJobDataItemWriter();
    }
    // END OF DATA ITEM HANDLERS
    // -------------------------------------------------------
    // MAPPING ITEM HANDLERS
    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_PREPARE_PARTITIONER)
    @JobScope
    public Partitioner reindexDataJobPreparePartitioner() {
        return new ReindexDataJobPreparePartitioner();
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_RESET_PARTITIONER)
    @JobScope
    public Partitioner reindexDataJobResetPartitioner() {
        return new ReindexDataJobResetPartitioner();
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_MAPPING_PARTITIONER)
    @JobScope
    public Partitioner reindexDataJobMappingPartitioner() {
        return new ReindexDataJobMappingPartitioner();
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_MAPPING_ITEM_READER)
    @StepScope
    public ItemReader<String> reindexDataJobMappingItemReader() {
        return new ReindexDataJobMappingItemReader();
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_MAPPING_ITEM_PROCESSOR)
    @StepScope
    public ItemProcessor<String, String> reindexDataJobMappingItemProcessor() {
        return new PassThroughItemProcessor<>();
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_MAPPING_ITEM_WRITER)
    @StepScope
    public ItemWriter<String> reindexDataJobMappingItemWriter() {
        return new ReindexDataJobMappingItemWriter();
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_PREPARE_ITEM_WRITER)
    @StepScope
    public ItemWriter<String> reindexDataJobPrepareItemWriter() {
        return new ReindexDataJobPrepareItemWriter();
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_RESET_ITEM_WRITER)
    @StepScope
    public ItemWriter<String> reindexDataJobResetItemWriter() {
        return new ReindexDataJobResetItemWriter();
    }

    // END OF MAPPING ITEM HANDLERS
    // -------------------------------------------------------
    // END OF RESET MODEL ITEM HANDLERS
    // -------------------------------------------------------
    // STEPS
    // -------------------------------------------------------
    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_MAPPING_STEP)
    public Step reindexDataJobMappingStep(
            ItemReader<String> reindexDataJobMappingItemReader,
            ItemProcessor<String, String> reindexDataJobMappingItemProcessor,
            ItemWriter<String> reindexDataJobMappingItemWriter) {

        return stepBuilderFactory.get(ReindexJobConfigurationConstants.BEAN_NAME_MAPPING_STEP)
                .<String, String>chunk(1)
                .reader(reindexDataJobMappingItemReader)
                .processor(reindexDataJobMappingItemProcessor)
                .writer(reindexDataJobMappingItemWriter)
                .build();
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_PREPARE_STEP)
    public Step reindexDataJobPrepareStep(
            ItemReader<String> reindexDataJobMappingItemReader,
            ItemProcessor<String, String> reindexDataJobMappingItemProcessor,
            ItemWriter<String> reindexDataJobPrepareItemWriter) {

        return stepBuilderFactory.get(ReindexJobConfigurationConstants.BEAN_NAME_PREPARE_STEP)
                .<String, String>chunk(1)
                .reader(reindexDataJobMappingItemReader)
                .processor(reindexDataJobMappingItemProcessor)
                .writer(reindexDataJobPrepareItemWriter)
                .build();
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_DATA_STEP)
    public Step reindexDataJobDataStep(
            @Value("${" + ReindexJobConfigurationConstants.PROP_NAME_DATA_COMMIT_INTERVAL + ":1000}") int commitInterval,
            StepExecutionListener reindexDataJobDataStepExecutionListener,
            ReindexDataJobDataItemReader reindexDataJobDataItemReader,
            ReindexDataJobDataItemProcessor reindexDataJobDataItemProcessor,
            ReindexDataJobDataItemWriter reindexDataJobDataItemWriter) {

        return stepBuilderFactory.get(ReindexJobConfigurationConstants.BEAN_NAME_DATA_STEP)
                .listener(reindexDataJobDataStepExecutionListener)
                .<Triple<LSN, UUID, String>, UpsertRequestContext>chunk(commitInterval)
                .reader(reindexDataJobDataItemReader)
                .processor(reindexDataJobDataItemProcessor)
                .writer(reindexDataJobDataItemWriter)
                .build();
    }

    @Bean(name = ReindexJobConfigurationConstants.BEAN_NAME_RESET_STEP)
    public Step reindexDataJobResetStep(
            ItemReader<String> reindexDataJobMappingItemReader,
            ItemProcessor<String, String> reindexDataJobMappingItemProcessor,
            ItemWriter<String> reindexDataJobResetItemWriter) {

        return stepBuilderFactory.get(ReindexJobConfigurationConstants.BEAN_NAME_RESET_STEP)
                .<String, String>chunk(1)
                .reader(reindexDataJobMappingItemReader)
                .processor(reindexDataJobMappingItemProcessor)
                .writer(reindexDataJobResetItemWriter)
                .build();
    }
    // -------------------------------------------------------
    // END OF STEPS
    // -------------------------------------------------------
    @Bean(name = ReindexJobConfigurationConstants.JOB_NAME)
    public Job reindexDataJob(
            @Autowired JobRepository jobRepository,
            @Autowired JobExecutionListener reindexDataJobListener,
            @Autowired JobBuilderFactory jobBuilderFactory,
            @Autowired TaskExecutor reindexDataJobMappingTaskExecutor,
            // Ensure mappings
            @Autowired Partitioner reindexDataJobMappingPartitioner,
            @Autowired Step reindexDataJobMappingStep,
            // Prepare indexes
            @Autowired Partitioner reindexDataJobPreparePartitioner,
            @Autowired Step reindexDataJobPrepareStep,
            // Process data
            @Autowired Partitioner reindexDataJobDataPartitioner,
            @Autowired PartitionHandler reindexDataJobDataPartitionHandler,
            // Reset indexes
            @Autowired Partitioner reindexDataJobResetPartitioner,
            @Autowired Step reindexDataJobResetStep) {

        return jobBuilderFactory.get(ReindexJobConfigurationConstants.JOB_NAME)
            .repository(jobRepository)
            .preventRestart()
            .listener(reindexDataJobListener)
            // 2. Ensure mappings exist. Create, if necessary.
            .start(stepBuilderFactory.get("reindexDataJobMapping")
                    .partitioner(ReindexJobConfigurationConstants.BEAN_NAME_MAPPING_STEP, reindexDataJobMappingPartitioner)
                    .step(reindexDataJobMappingStep)
                    .taskExecutor(reindexDataJobMappingTaskExecutor)
                    .build())
            // 3. Apply various index settings (throttling, refresh interval, etc.)
            .next(stepBuilderFactory.get("reindexDataJobPrepare")
                    .partitioner(ReindexJobConfigurationConstants.BEAN_NAME_PREPARE_STEP, reindexDataJobPreparePartitioner)
                    .step(reindexDataJobPrepareStep)
                    .taskExecutor(reindexDataJobMappingTaskExecutor)
                    .build())
            // 4. Reindex data actually
            .next(stepBuilderFactory.get("reindexDataJobData")
                    .partitioner(ReindexJobConfigurationConstants.BEAN_NAME_DATA_STEP, reindexDataJobDataPartitioner)
                    .partitionHandler(reindexDataJobDataPartitionHandler)
                    .build())
            // 5. Put indexes back into normal state (reset throttling, refresh interval, etc.)
            .next(stepBuilderFactory.get("reindexDataJobReset")
                    .partitioner(ReindexJobConfigurationConstants.BEAN_NAME_RESET_STEP, reindexDataJobResetPartitioner)
                    .step(reindexDataJobResetStep)
                    .taskExecutor(reindexDataJobMappingTaskExecutor)
                    .build())
            .build();
    }
}
