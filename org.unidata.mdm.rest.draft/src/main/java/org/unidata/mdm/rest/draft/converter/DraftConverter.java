package org.unidata.mdm.rest.draft.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.core.configuration.CoreConfiguration;
import org.unidata.mdm.core.dto.UserDTO;
import org.unidata.mdm.core.service.UserService;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.rest.draft.ro.DraftResponseRO;

/**
 * @author Mikhail Mikhailov on Sep 11, 2020
 */
public class DraftConverter {

    private static UserService userService;

    /**
     * Constructor.
     */
    private DraftConverter() {
        super();
    }

    public static void init() {
        userService = CoreConfiguration.getBean(UserService.class);
    }

    public static DraftResponseRO from(Draft d) {

        if (Objects.isNull(d)) {
            return null;
        }

        DraftResponseRO result = new DraftResponseRO();
        result.setSubjectId(d.getSubjectId() == null ? null : d.getSubjectId());
        result.setDraftId(d.getDraftId());
        result.setParentDraftId(d.getParentDraftId());
        result.setType(d.getProvider());

        if (Objects.nonNull(d.getOwner())) {

            result.setOwner(d.getOwner());

            UserDTO udto = userService.getUserByName(d.getOwner());
            if (Objects.nonNull(udto)) {
                result.setOwnerFullName(udto.getFullName());
            }
        }

        result.setCreateDate(d.getCreateDate());
        result.setUpdateDate(d.getUpdateDate());
        result.setCreatedBy(d.getCreatedBy());
        result.setUpdatedBy(d.getUpdatedBy());
        result.setDescription(d.getDescription());
        result.setTags(CollectionUtils.isEmpty(d.getTags()) ? Collections.emptyList() : new ArrayList<>(d.getTags()));
        result.setEditionsCount(d.getEditionsCount());

        return result;
    }
}
