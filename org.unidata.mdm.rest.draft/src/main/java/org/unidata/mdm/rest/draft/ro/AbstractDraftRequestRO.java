package org.unidata.mdm.rest.draft.ro;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractDraftRequestRO {
    private String type;
    private String subjectId;
    private Long draftId;
    private Long parentDraftId;
    private String owner;
    private List<String> tags;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String entityId) {
        this.subjectId = entityId;
    }

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

    public Long getParentDraftId() {
        return parentDraftId;
    }

    public void setParentDraftId(Long parentDraftId) {
        this.parentDraftId = parentDraftId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String draftOwner) {
        this.owner = draftOwner;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }
}
