/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.system.service.impl.event;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import org.unidata.mdm.system.type.event.Event;
import org.unidata.mdm.system.type.event.EventReceiver;

import com.hazelcast.core.HazelcastInstance;

/**
 * Simple subscription registry.
 * @author Mikhail Mikhailov on Oct 30, 2019
 */
public class ReceiverRegistry {

    private final Map<Class<? extends Event>, ReceiverAdaptor> classified;

    private final HazelcastInstance hazelcastInstance;

    public ReceiverRegistry(final HazelcastInstance hazelcastInstance) {
        super();
        this.hazelcastInstance = hazelcastInstance;
        this.classified = new ConcurrentHashMap<>();
    }

    public ReceiverAdaptor get(Event event) {
        return classified.computeIfAbsent(event.getClass(), this::push);
    }

    public<T extends Event> void register(EventReceiver subscriber, Class<T> eventType) {

        if (Objects.isNull(subscriber) || Objects.isNull(eventType)) {
            return;
        }

        classified.computeIfAbsent(eventType, this::push)
            .subscribe(subscriber);
    }

    public void register(EventReceiver subscriber) {

        if (Objects.isNull(subscriber)) {
            return;
        }

        classified.keySet().forEach(key -> register(subscriber, key));
    }

    public <T extends Event> void unregister(EventReceiver subscriber, Class<T> eventType) {
        classified.computeIfPresent(eventType, (k, v) -> pop(v, subscriber));
    }

    public void unregister(EventReceiver subscriber) {
        classified.keySet().forEach(key -> unregister(subscriber, key));
    }

    public int receive(Event event, boolean fromLocalMember) {
        ReceiverAdaptor s = get(event);
        return s.receive(event, fromLocalMember);
    }

    private <T extends Event> ReceiverAdaptor push(Class<T> eventClass) {
        // Operate in HZ 'unsafe' mode.
        // This may change in the future.
        return new ReceiverAdaptor(hazelcastInstance.getCPSubsystem()
                .getAtomicLong(eventClass.getName() + "#[RECEIVERS]"));
    }

    private ReceiverAdaptor pop(ReceiverAdaptor receiver, EventReceiver subscriber) {
        receiver.unsubscribe(subscriber);
        return receiver;
    }
}
