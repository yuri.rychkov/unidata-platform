/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.service.impl;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;
import org.unidata.mdm.system.service.NameSpaceService;
import org.unidata.mdm.system.type.module.Module;
import org.unidata.mdm.system.type.namespace.NameSpace;

/**
 * @author Mikhail Mikhailov on Mar 4, 2021
 */
@Service
public class NameSpaceServiceImpl implements NameSpaceService {
    /**
     * Simple storage.
     */
    private final Map<String, NameSpace> namespaces = new ConcurrentHashMap<>();
    /**
     * Constructor.
     */
    public NameSpaceServiceImpl() {
        super();
        namespaces.put(NameSpace.GLOBAL_NAMESPACE.getId(), NameSpace.GLOBAL_NAMESPACE);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public NameSpace getById(String id) {
        return namespaces.get(id);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNameSpace(String id) {
        return namespaces.containsKey(id);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<NameSpace> getNameSpaces() {
        return namespaces.values();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void postProcessBeforeInitialization(Module m) {
        for (NameSpace ns : m.getNameSpaces()) {
            namespaces.putIfAbsent(ns.getId(), ns);
        }
    }
}
