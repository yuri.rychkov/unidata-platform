package org.unidata.mdm.system.type.rendering;
/**
 * Just a marker.
 * @author Mikhail Mikhailov on Jan 15, 2020
 */
public interface InputSource {}
