/**
 * Segments connecting to other pipelines.
 * @author Mikhail Mikhailov on May 25, 2020
 */
package org.unidata.mdm.system.type.pipeline.connection;