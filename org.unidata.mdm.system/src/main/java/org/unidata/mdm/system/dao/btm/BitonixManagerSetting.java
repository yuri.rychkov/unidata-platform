/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.dao.btm;

import org.unidata.mdm.system.dao.TransactionManagerSetting;

/**
 * @author Mikhail Mikhailov on May 16, 2021
 */
public enum BitonixManagerSetting implements TransactionManagerSetting {
    /**
     * The server id.
     */
    SERVER_ID("serverId", "serverId", null),
    /**
     * TX log, part 1.
     */
    LOG_PART_ONE("logPart1Filename", "logPart1Filename", "btm1.tlog"),
    /**
     * TX log, part 2.
     */
    LOG_PART_TWO("logPart2Filename", "logPart2Filename", "btm2.tlog"),
    /**
     * Use async 2PC commit.
     */
    ASYNCHRONOUS_2PC("async", "asynchronous2Pc", Boolean.TRUE),
    /**
     * Warn about zero resource TX.
     */
    WARN_ZERO_RESOURCE_TX("warnAboutZeroResourceTransaction", "warnAboutZeroResourceTransaction", Boolean.TRUE),

    ;
    /**
     * Configuration property name
     */
    private final String configurationName;
    /**
     * Bean property name.
     */
    private final String propertyName;
    /**
     * Default value.
     */
    private final Object defaultValue;
    /**
     * Constructor.
     * @param configurationName
     * @param propertyName
     * @param defaultValue
     */
    private BitonixManagerSetting(String configurationName, String propertyName, Object defaultValue) {
        this.configurationName = configurationName;
        this.propertyName = propertyName;
        this.defaultValue = defaultValue;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getConfigurationName() {
        return configurationName;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getPropertyName() {
        return propertyName;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Object getDefaultValue() {
        return defaultValue;
    }
}
