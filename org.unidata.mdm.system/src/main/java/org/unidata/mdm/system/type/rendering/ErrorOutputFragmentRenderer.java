package org.unidata.mdm.system.type.rendering;

import org.unidata.mdm.system.dto.OutputContainer;
import org.unidata.mdm.system.exception.PlatformRuntimeException;

/**
 * Runs rendering actions upon exceptional executions.
 * @author Alexey Tsarapkin
 */
public interface ErrorOutputFragmentRenderer {
    /**
     * Runs rendering action.
     * @param container the container
     * @param e the exception
     * @param sink the sink
     * @return true, if successful, false otherwise
     */
    boolean onError(OutputContainer container, PlatformRuntimeException e, OutputSink sink);
}
