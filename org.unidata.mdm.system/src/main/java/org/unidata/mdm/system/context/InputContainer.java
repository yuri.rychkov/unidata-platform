package org.unidata.mdm.system.context;

/**
 * The very basic input container element.
 * Just a marker interface for now.
 * @author Mikhail Mikhailov on Feb 21, 2020
 */
public interface InputContainer {}
