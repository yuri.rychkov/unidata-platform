/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.system.context;

import java.util.Objects;

import io.micrometer.core.lang.Nullable;

/**
 * @author Mikhail Mikhailov
 * Throwable cause indicator.
 */
public interface ThrowableCauseAwareContext extends StorageCapableContext {
    /**
     * The throwable cause.
     */
    StorageId SID_THROWABLE_CAUSE = new StorageId("THROWABLE_CAUSE");
    /**
     * Returns true, if this context has throwable cause set and, thus, has performed an operation,
     * that caused a throw.
     * @return true, if has throwable set
     */
    default boolean hasThrown() {
        return Objects.nonNull(throwableCause());
    }
    /**
     * Get throwable cause
     * @return throwable or null
     */
    @Nullable
    default Throwable throwableCause() {
        return getFromStorage(SID_THROWABLE_CAUSE);
    }
    /**
     * Put throwable cause to the context
     * @param throwableCause the cause
     */
    default void throwableCause(Throwable throwableCause) {
        putToStorage(SID_THROWABLE_CAUSE, throwableCause);
    }
}
