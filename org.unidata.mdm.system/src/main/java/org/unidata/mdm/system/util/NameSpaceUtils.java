/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.util;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.system.configuration.SystemConfiguration;
import org.unidata.mdm.system.service.NameSpaceService;
import org.unidata.mdm.system.type.namespace.NameSpace;

/**
 * @author Mikhail Mikhailov on Mar 9, 2021
 */
public final class NameSpaceUtils {
    /**
     * The NSS.
     */
    private static NameSpaceService nameSpaceService;
    /**
     * Constructor.
     */
    private NameSpaceUtils() {
        super();
    }
    /**
     * Semi-private init.
     */
    public static void init() {
        nameSpaceService = SystemConfiguration.getBean(NameSpaceService.class);
    }

    public static String join(NameSpace ns, String typeName) {
        return join(ns == null ? null : ns.getId(), typeName);
    }

    public static String join(String ns, String typeName) {

        final String[] parts = {
                StringUtils.defaultString(ns, NameSpace.GLOBAL_NAMESPACE_ID),
                NameSpace.NAMESPACE_SEPARATOR,
                StringUtils.defaultString(typeName, StringUtils.EMPTY)
        };

        return StringUtils.join(parts);
    }

    public static NameSpace find(String name) {
        return nameSpaceService.getById(name);
    }
}
