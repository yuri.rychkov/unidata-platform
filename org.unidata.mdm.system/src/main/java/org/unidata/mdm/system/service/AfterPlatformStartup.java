package org.unidata.mdm.system.service;

/**
 * Runs methods after {@link org.unidata.mdm.system.type.module.Module#ready()}.
 * @author Mikhail Mikhailov on Apr 1, 2020
 */
public interface AfterPlatformStartup {
    /**
     * Runs method after {@link org.unidata.mdm.system.type.module.Module#ready()}.
     */
    void afterPlatformStartup();
}
