/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.system.migration;

import org.unidata.mdm.system.util.ResourceUtils;

import nl.myndocs.database.migrator.MigrationScript;

/**
 * @author Mikhail Mikhailov on Oct 22, 2019
 */
public final class SystemMigrations {
    /**
     * Migrations so far.
     */
    private static final MigrationScript[] MIGRATIONS = {
            Migrations.of(
                    "UN-12296__InitSystemSchema",
                    "mikhail.mikhailov",
                    "create table modules_info (module_id text primary key, version text not null, tag text, status text)"),
            Migrations.of(
                    "UN-12296__InitializationConfigurationCoreSchema",
                    "maria.chistyakova",
                    ResourceUtils.asString("classpath:/migration/UN-12296-configuration-structure.sql")),
            Migrations.of(
                    "UN-12000-pipelines",
                    "mikhail.mikhailov",
                    "create table if not exists pipelines_info (" +
                            "start_id text not null, " +
                            "subject text, " +
                            "content text not null, " +
                            "constraint pk_pipelines_info_start_id_subject primary key(start_id, subject))"),
            Migrations.of(
                    "UN-16000-pipelines",
                    "mikhail.mikhailov",
                    "alter table if exists pipelines_info " +
                            "add column update_date timestamptz, " +
                            "add column updated_by varchar(256)")
    };
    /**
     * Constructor.
     */
    private SystemMigrations() {
        super();
    }
    /**
     * Makes SONAR happy.
     *
     * @return migrations
     */
    public static MigrationScript[] migrations() {
        return MIGRATIONS;
    }
}
