/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.convert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.data.type.data.EtalonRelation;
import org.unidata.mdm.data.type.data.EtalonRelationInfoSection;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.meta.type.RelativeDirection;
import org.unidata.mdm.meta.type.search.EntityIndexType;
import org.unidata.mdm.meta.type.search.RelationFromIndexId;
import org.unidata.mdm.meta.type.search.RelationHeaderField;
import org.unidata.mdm.meta.type.search.RelationToIndexId;
import org.unidata.mdm.search.type.id.AbstractManagedIndexId;
import org.unidata.mdm.search.type.indexing.Indexing;
import org.unidata.mdm.search.type.indexing.IndexingField;

/**
 * @author Mikhail Mikhailov on Oct 12, 2019
 * Relations
 */
public final class RelationIndexingConverter extends AbstractIndexingConverter {

    /**
     * Constructor.
     */
    private RelationIndexingConverter() {
        super();
    }

    public static List<Indexing> convert(RelationKeys keys, Collection<EtalonRelation> relations) {
        return convert(keys, relations, null);
    }

    public static List<Indexing> convert(RelationKeys keys, Collection<EtalonRelation> relations, RelativeDirection processing) {

        if (CollectionUtils.isEmpty(relations)) {
            return Collections.emptyList();
        }

        List<Indexing> result = new ArrayList<>(relations.size() * 2);
        for (EtalonRelation relation : relations) {

            EtalonRelationInfoSection infoSection = relation.getInfoSection();
            List<IndexingField> fields = new ArrayList<>(RelationHeaderField.values().length + relation.getSize());

            // 1. Common
            fields.add(IndexingField.of(RelationHeaderField.FIELD_RELATION_NAME.getName(), infoSection.getRelationName()));
            fields.add(IndexingField.of(RelationHeaderField.FIELD_RELATION_TYPE.getName(), infoSection.getRelationType().name()));
            fields.add(IndexingField.of(RelationHeaderField.FIELD_ETALON_ID.getName(), infoSection.getRelationEtalonKey()));
            fields.add(IndexingField.of(RelationHeaderField.FIELD_FROM_ETALON_ID.getName(), infoSection.getFromEtalonKey().getId()));
            fields.add(IndexingField.of(RelationHeaderField.FIELD_TO_ETALON_ID.getName(), infoSection.getToEtalonKey().getId()));
            fields.add(IndexingField.of(RelationHeaderField.FIELD_PERIOD_ID.getName(), AbstractManagedIndexId.periodIdValToString(infoSection.getValidTo())));
            fields.add(IndexingField.of(RelationHeaderField.FIELD_DELETED.getName(), keys.getEtalonKey().getStatus() == RecordStatus.INACTIVE));
            fields.add(IndexingField.of(RelationHeaderField.FIELD_INACTIVE.getName(), infoSection.getStatus() == RecordStatus.INACTIVE));
            fields.add(IndexingField.of(RelationHeaderField.FIELD_ORIGINATOR.getName(), infoSection.getUpdatedBy()));
            fields.add(IndexingField.of(RelationHeaderField.FIELD_OPERATION_TYPE.getName(), infoSection.getOperationType().name()));

            fields.add(IndexingField.of(RelationHeaderField.FIELD_FROM.getName(), infoSection.getValidFrom()));
            fields.add(IndexingField.of(RelationHeaderField.FIELD_TO.getName(), infoSection.getValidTo()));

            fields.add(IndexingField.of(RelationHeaderField.FIELD_CREATED_AT.getName(), infoSection.getCreateDate()));
            if (infoSection.getUpdateDate() != null) {
                fields.add(IndexingField.of(RelationHeaderField.FIELD_UPDATED_AT.getName(), infoSection.getUpdateDate()));
            }

            fields.add(IndexingField.of(RelationHeaderField.FIELD_CREATED_BY.getName(), infoSection.getCreatedBy()));
            if (infoSection.getUpdatedBy() != null) {
                fields.add(IndexingField.of(RelationHeaderField.FIELD_UPDATED_BY.getName(), infoSection.getUpdatedBy()));
            }

            // 1.1. save relation attributes data
            if (relation.getSize() > 0) {
                fields.addAll(AbstractIndexingConverter.buildRecord(relation));
            }

            // 2. Append
            append(result, fields, infoSection, processing);
        }

        return result;
    }

    private static void append(List<Indexing> result, List<IndexingField> fields, EtalonRelationInfoSection infoSection,  RelativeDirection processing) {

        // 1. From
        if (processing == null || processing == RelativeDirection.FROM) {

            List<IndexingField> fromFields = new ArrayList<>(fields);
            fromFields.add(IndexingField.of(RelationHeaderField.FIELD_DIRECTION_FROM.getName(), true));

            result.add(new Indexing(EntityIndexType.RELATION,
                    RelationFromIndexId.of(
                            infoSection.getFromEntityName(),
                            infoSection.getRelationName(),
                            infoSection.getFromEtalonKey().getId(),
                            infoSection.getToEtalonKey().getId(),
                            infoSection.getValidTo()))
                        .withFields(fromFields));
        }

        // 2. To
        if (processing == null || processing == RelativeDirection.TO) {

            List<IndexingField> toFields = new ArrayList<>(fields);
            toFields.add(IndexingField.of(RelationHeaderField.FIELD_DIRECTION_FROM.getName(), false));

            result.add(new Indexing(EntityIndexType.RELATION,
                    RelationToIndexId.of(
                            infoSection.getToEntityName(),
                            infoSection.getRelationName(),
                            infoSection.getFromEtalonKey().getId(),
                            infoSection.getToEtalonKey().getId(),
                            infoSection.getValidTo()))
                        .withFields(toFields));
        }
    }
}
