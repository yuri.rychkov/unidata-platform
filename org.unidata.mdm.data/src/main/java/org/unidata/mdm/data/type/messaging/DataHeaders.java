package org.unidata.mdm.data.type.messaging;

import org.unidata.mdm.system.type.messaging.Header;
import org.unidata.mdm.system.type.messaging.Header.HeaderType;

/**
 * @author Mikhail Mikhailov on Jul 15, 2020
 */
public class DataHeaders {
    /**
     * Constructor.
     */
    private DataHeaders() {
        super();
    }
    /**
     * Input context.
     */
    public static final Header CONTEXT = new Header("context", HeaderType.OBJECT);
}
