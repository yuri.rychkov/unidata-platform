/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 * 
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.type.apply.batch.impl;

import org.unidata.mdm.data.dto.DeleteRelationsDTO;

/**
 * @author Mikhail Mikhailov on Dec 16, 2019
 */
public class RelationDeleteBatchSetStatistics extends AbstractBatchSetStatistics<DeleteRelationsDTO> {
    /**
     * Number of deleted.
     */
    private long deleted = 0L;
    /**
     * Constructor.
     */
    public RelationDeleteBatchSetStatistics() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
        super.reset();
        this.deleted = 0L;
    }
    /**
     * @return the deleted
     */
    public long getDeleted() {
        return deleted;
    }
    /**
     * @param deleted the deleted to set
     */
    public void incrementDeleted() {
        incrementDeleted(1L);
    }
    /**
     * @param deleted the deleted to set
     */
    public void incrementDeleted(long deleted) {
        this.deleted += deleted;
    }
}
