/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.relations.restore;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.calculables.CalculableHolder;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.data.context.RelationRestoreContext;
import org.unidata.mdm.data.context.RestoreRecordRequestContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.po.data.RelationEtalonPO;
import org.unidata.mdm.data.po.data.RelationOriginPO;
import org.unidata.mdm.data.po.data.RelationVistoryPO;
import org.unidata.mdm.data.service.RecordChangeSetProcessor;
import org.unidata.mdm.data.service.RelationChangeSetProcessor;
import org.unidata.mdm.data.type.apply.RecordRestoreChangeSet;
import org.unidata.mdm.data.type.apply.RelationRestoreChangeSet;
import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.data.type.data.RelationType;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.system.context.CommonRequestContext;
import org.unidata.mdm.system.service.PlatformConfiguration;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;
import org.unidata.mdm.system.type.support.IdentityHashSet;
import org.unidata.mdm.system.util.IdUtils;

/**
 * @author Mikhail Mikhailov on Dec 9, 2019
 */
@Component(RelationRestorePersistenceExecutor.SEGMENT_ID)
public class RelationRestorePersistenceExecutor extends Point<RelationRestoreContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_RESTORE_PERSISTENCE]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relation.restore.persistence.description";
    /**
     * Rels change set.
     */
    @Autowired
    private RelationChangeSetProcessor relationChangeSetProcessor;
    /**
     * The record set processor.
     */
    @Autowired
    private RecordChangeSetProcessor recordChangeSetProcessor;
    /**
     * Platform configuration.
     */
    @Autowired
    private PlatformConfiguration platformConfiguration;
    /**
     * Constructor.
     */
    public RelationRestorePersistenceExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(RelationRestoreContext ctx) {

        MeasurementPoint.start();
        try {

            // 1. Prepare set
            prepareChangeSet(ctx);

            // 2. Apply changes
            applyChangeSet(ctx);

        } finally {
            MeasurementPoint.stop();
        }
    }

    protected void applyChangeSet(RelationRestoreContext ctx) {

        // Will be applied later in batched fashion.
        if (ctx.isBatchOperation()) {
            return;
        }

        // 2. Apply
        if (ctx.relationType() == RelationType.CONTAINS) {
            RestoreRecordRequestContext uCtx = ctx.containmentContext();
            RecordRestoreChangeSet set = uCtx.changeSet();
            recordChangeSetProcessor.apply(set);
        }

        RelationRestoreChangeSet set = ctx.changeSet();
        relationChangeSetProcessor.apply(set);
    }

    protected void prepareChangeSet(RelationRestoreContext ctx) {

        MeasurementPoint.start();
        try {

            final Timeline<OriginRelation> next = ctx.nextTimeline();
            final RelationKeys keys = next.getKeys();
            final Date ts = ctx.timestamp();
            final String user = SecurityUtils.getCurrentUserName();
            final String operationId = ((CommonRequestContext) ctx).getOperationId();

            RelationRestoreChangeSet set = ctx.changeSet();
            if (ctx.isPeriodRestore() && ctx.relationType() != RelationType.CONTAINS) {

                Set<OriginRelation> records = next.stream()
                    .map(TimeInterval::unlock)
                    .flatMap(mti -> mti.toModifications().values().stream())
                    .flatMap(Collection::stream)
                    .map(CalculableHolder::getValue)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toCollection(IdentityHashSet::new));

                for (OriginRelation or : records) {

                    // Create vistory record and add it to the box.
                    RelationVistoryPO result = new RelationVistoryPO();
                    result.setShard(keys.getShard());
                    result.setCreatedBy(user);
                    result.setStatus(or.getInfoSection().getStatus());
                    result.setShift(or.getInfoSection().getShift());
                    result.setOperationType(or.getInfoSection().getOperationType());
                    result.setData(or);
                    result.setId(IdUtils.v1String());
                    result.setOriginId(or.getInfoSection().getRelationOriginKey().getId());
                    result.setOperationId(operationId);
                    result.setMajor(platformConfiguration.getPlatformMajor());
                    result.setMinor(platformConfiguration.getPlatformMinor());
                    result.setValidFrom(or.getInfoSection().getValidFrom());
                    result.setValidTo(or.getInfoSection().getValidTo());
                    result.setCreateDate(ts);

                    set.getOriginsVistoryRelationsPOs().add(result);
                }
            } else if (!ctx.isPeriodRestore()) {

                RelationEtalonPO update = new RelationEtalonPO();
                update.setId(keys.getEtalonKey().getId());
                update.setStatus(keys.getEtalonKey().getStatus());
                update.setOperationId(operationId);
                update.setShard(keys.getShard());
                update.setUpdateDate(ts);
                update.setUpdatedBy(user);

                set.getEtalonRelationUpdatePOs().add(update);

                List<RelationOriginPO> origins = keys.getSupplementaryKeys().stream()
                    .map(ok -> {
                        RelationOriginPO okpo = new RelationOriginPO();
                        okpo.setId(ok.getId());
                        okpo.setShard(keys.getShard());
                        okpo.setEtalonId(keys.getEtalonKey().getId());
                        okpo.setShard(keys.getShard());
                        okpo.setUpdateDate(ts);
                        okpo.setUpdatedBy(user);
                        okpo.setStatus(ok.getStatus());
                        return okpo;
                    })
                    .collect(Collectors.toList());

                set.getOriginRelationUpdatePOs().addAll(origins);
            }

        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?, ?> start) {
        return RelationRestoreContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
