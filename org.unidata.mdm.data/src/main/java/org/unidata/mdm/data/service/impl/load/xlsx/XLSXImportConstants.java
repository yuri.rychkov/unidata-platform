/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.data.service.impl.load.xlsx;

/**
 * @author Dmitry Kopin on 13.02.2019.
 */
public class XLSXImportConstants {
    /**
     * Disabling constructor.
     */
    private XLSXImportConstants() {
        super();
    }
    /**
     * Result message.
     */
    public static final String MSG_REPORT_HEADER = "app.user.events.import.xlsx.data.result";
    /**
     * Total records message.
     */
    public static final String MSG_REPORT_RECORDS_TOTAL = "app.data.import.stat.records.total";
    /**
     * Details total records message.
     */
    public static final String MSG_REPORT_DETAILS_RECORD_TOTAL = "app.data.import.stat.details.record.total";
    /**
     * Total relations message.
     */
    public static final String MSG_REPORT_RELATIONS_TOTAL = "app.data.import.stat.relations.total";
    /**
     * Details total relations message.
     */
    public static final String MSG_REPORT_DETAILS_RELATION_TOTAL = "app.data.import.stat.details.relation.total";
    /**
     * Total classifiers message.
     */
    public static final String MSG_REPORT_CLASSIFIERS_TOTAL = "app.data.import.stat.classifiers.total";
    /**
     * Total classifiers message.
     */
    public static final String MSG_REPORT_DETAILS_CLASSIFIER_TOTAL = "app.data.import.stat.classifier.details.total";
    /**
     * Total classifiers message.
     */
    public static final String MSG_REPORT_DETAILS_COMPLEX_ATTRIBUTE_TOTAL = "app.data.import.stat.complex.attributes.total";
    /**
     * Inserted count.
     */
    public static final String MSG_REPORT_INSERTED = "app.data.import.stat.inserted";
    /**
     * Updated count.
     */
    public static final String MSG_REPORT_UPDATED = "app.data.import.stat.updated";
    /**
     * Skept count.
     */
    public static final String MSG_REPORT_SKEPT = "app.data.import.stat.skept";
    /**
     * Deleted count.
     */
    public static final String MSG_REPORT_DELETED = "app.data.import.stat.deleted";
    /**
     * Failed count.
     */
    public static final String MSG_REPORT_FAILED = "app.data.import.stat.failed";
}
