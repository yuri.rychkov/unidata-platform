/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.impl.load;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.context.DataImportInputContext;
import org.unidata.mdm.core.context.DataImportTemplateContext;
import org.unidata.mdm.core.dto.DataImportTemplateResult;
import org.unidata.mdm.core.service.DataImportService;
import org.unidata.mdm.core.type.load.DataImportFormat;
import org.unidata.mdm.core.type.load.DataImportHandler;
import org.unidata.mdm.system.service.TextService;

/**
 * @author Mikhail Mikhailov on May 13, 2021
 */
@Component
public class RecordsImportHandlerComponent implements DataImportHandler {
    /**
     * The ID.
     */
    private static final String ID = "records-and-relations";
    /**
     * This draft provider description.
     */
    private static final String DESCRIPTION = "app.data.import.records.and.relations.handler.description";
    /**
     * The DLS.
     */
    private DataImportService dataImportService;
    /**
     * The TS.
     */
    @Autowired
    private TextService textService;
    /**
     * Implementations map.
     */
    private Map<DataImportFormat, RecordsImportImplementation> implementations;
    /**
     * Constructor.
     */
    @Autowired
    public RecordsImportHandlerComponent(DataImportService dataLoadService, List<RecordsImportImplementation> delegates) {
        super();
        this.dataImportService = dataLoadService;
        this.dataImportService.register(this);

        if (CollectionUtils.isEmpty(delegates)) {
            this.implementations = Collections.emptyMap();
        } else {
            this.implementations = new EnumMap<>(DataImportFormat.class);
            this.implementations.putAll(delegates.stream().collect(Collectors.toMap(RecordsImportImplementation::format, Function.identity())));
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return textService.getText(DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<DataImportFormat> getSupported() {
        return implementations.keySet();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void handle(DataImportInputContext ctx) {
        RecordsImportImplementation i = implementations.get(ctx.getFormat());
        i.handle(ctx);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DataImportTemplateResult template(DataImportTemplateContext ctx) {
        RecordsImportImplementation i = implementations.get(ctx.getFormat());
        return i.template(ctx);
    }
}
