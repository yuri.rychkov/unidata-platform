package org.unidata.mdm.data.context;

import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.data.type.merge.MergeRelationMasterState;
import org.unidata.mdm.system.context.SetupAwareContext;

/**
 * Common supertype for FROM/TO relations.
 * @author Mikhail Mikhailov on May 10, 2020
 */
public interface RelationMergeContext extends
    ReadWriteTimelineContext<OriginRelation>,
    ReadWriteDataContext<OriginRelation>,
    MergeItemContext<MergeRelationMasterState>,
    RelationIdentityContext,
    SetupAwareContext,
    AccessRightContext,
    BatchAwareContext,
    ExtendedAttributesAwareContext {}
