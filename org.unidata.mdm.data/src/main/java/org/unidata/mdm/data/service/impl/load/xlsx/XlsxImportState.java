/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.impl.load.xlsx;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.unidata.mdm.core.context.DataImportInputContext;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.model.NestedElement;
import org.unidata.mdm.core.type.model.RelationElement;
import org.unidata.mdm.core.type.security.Right;
import org.unidata.mdm.data.context.DeleteRequestContext;
import org.unidata.mdm.data.context.UpsertRelationsRequestContext;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.exception.DataProcessingException;
import org.unidata.mdm.data.service.impl.load.xlsx.XLSXProcessor.OBJECT_TYPE;
import org.unidata.mdm.data.service.impl.load.xlsx.XLSXProcessor.StatisticSheet;

/**
 * @author Mikhail Mikhailov on May 21, 2021
 * Import state.
 */
public class XlsxImportState {

    private final XLSXImportComponent xComponent;

    private final DataImportInputContext input;

    private Map<OBJECT_TYPE, Map<String, XlsxSheetState>> sheets;

    private final Map<String, List<XlsxImportUpsert>> recordUpserts = new HashMap<>();

    private final List<DeleteRequestContext> recordDeletes = new ArrayList<>();

    private final List<UpsertRelationsRequestContext> relationUpserts = new ArrayList<>();

    private final List<StatisticSheet> statistics = new ArrayList<>(4);

    public XlsxImportState(XLSXImportComponent xComponent, DataImportInputContext input) {
        super();
        this.xComponent = xComponent;
        this.input = input;
    }

    public XlsxSheetState withTopLevel(Workbook workbook, EntityElement el) {

        Sheet sheet = xComponent.identifySheet(workbook, el.getName(), OBJECT_TYPE.ENTITY);
        if (sheet == null) {
            throw new DataProcessingException("Invalid input. Top level sheet for entity [{}] not found.",
                    DataExceptionIds.EX_DATA_XLSX_IMPORT_TOP_LEVEL_SHEET_NOT_FOUND, el.getName());
        }

        // Headers
        List<XLSXHeader> xlsxHeaders = xComponent.createHeaders(el);

        // Make security check and remove non-allowed headers to avoid processing.
        // Make security check only for non-system fields.
        xlsxHeaders.removeIf(hdr -> hdr.getType() != XLSXHeader.TYPE.SYSTEM &&
                // Parent resourceName equals entityName. Check for 'UPDATE' right.
                !XLSXSecurityUtils.isUserAllowed(hdr, el.getName(), Right::isUpdate));

        final Map<String, XLSXHeader> types = xlsxHeaders.stream().collect(Collectors.toMap(XLSXHeader::getSystemHeader, Function.identity()));
        final Map<Integer, String> headers = new HashMap<>();

        Row metadataRow = sheet.getRow(XLSXProcessor.SYSTEM_HEADER_IDX);
        metadataRow.cellIterator().forEachRemaining(cell -> headers.put(cell.getColumnIndex(), cell.getStringCellValue()));

        xlsxHeaders.stream().filter(XLSXHeader::isMandatory).forEach(rem -> {
            if (headers.values().stream().noneMatch(exi -> StringUtils.equals(rem.getSystemHeader(), exi))) {
                throw new DataProcessingException("Required attribute [{}] of entity [{}] is missing in headers!",
                        DataExceptionIds.EX_DATA_XLSX_IMPORT_UNKNOWN_ENTITY, rem.getSystemHeader(), el.getName());
            }
        });

        XlsxSheetState sst = new XlsxSheetState(el, types, headers);
        ensureSheets().computeIfAbsent(OBJECT_TYPE.ENTITY, k -> Map.of(StringUtils.EMPTY, sst));

        return sst;
    }

    @Nullable
    public XlsxSheetState withNested(Workbook workbook, NestedElement el) {

        Sheet sheet = xComponent.identifySheet(workbook, el.getName(), OBJECT_TYPE.NESTED);
        if (sheet == null) {
            return null;
        }

        // Headers
        List<XLSXHeader> xlsxHeaders = xComponent.createNestedEntityHeaders(el);
        final Map<String, XLSXHeader> types = xlsxHeaders.stream().collect(Collectors.toMap(XLSXHeader::getSystemHeader, Function.identity()));
        final Map<Integer, String> headers = new HashMap<>();

        Row metadataRow = sheet.getRow(XLSXProcessor.SYSTEM_HEADER_IDX);
        metadataRow.cellIterator().forEachRemaining(cell -> headers.put(cell.getColumnIndex(), cell.getStringCellValue()));

        XlsxSheetState sst = new XlsxSheetState(el, types, headers);
        ensureSheets().computeIfAbsent(OBJECT_TYPE.NESTED, k -> new HashMap<>()).put(el.getName(), sst);

        return sst;
    }

    @Nullable
    public XlsxSheetState withRelation(Workbook workbook, RelationElement el) {

        Sheet relationSheet = xComponent.identifySheet(workbook, el.getName(), OBJECT_TYPE.RELATION);
        if (relationSheet == null) {
            return null;
        }

        List<XLSXHeader> xlsxHeaders = xComponent.createRelationHeaders(el);
        final Map<String, XLSXHeader> types = xlsxHeaders.stream().collect(Collectors.toMap(XLSXHeader::getSystemHeader, Function.identity()));
        final Map<Integer, String> headers = new HashMap<>();

        Row metadataRow = relationSheet.getRow(XLSXProcessor.SYSTEM_HEADER_IDX);
        metadataRow.cellIterator().forEachRemaining(cell -> headers.put(cell.getColumnIndex(), cell.getStringCellValue()));

        XlsxSheetState sst = new XlsxSheetState(el, types, headers);
        ensureSheets().computeIfAbsent(OBJECT_TYPE.RELATION, k -> new HashMap<>()).put(el.getName(), sst);

        return sst;
    }

    public XlsxSheetState getTopLevel() {
        Map<String, XlsxSheetState> sheet = ensureSheets().get(OBJECT_TYPE.ENTITY);
        return MapUtils.isNotEmpty(sheet) ? sheet.get(StringUtils.EMPTY) : null;
    }

    public XlsxSheetState getNestedSheet(String name) {
        Map<String, XlsxSheetState> sheet = ensureSheets().get(OBJECT_TYPE.NESTED);
        return MapUtils.isNotEmpty(sheet) ? sheet.get(name) : null;
    }

    public XlsxSheetState getRelationSheet(String name) {
        Map<String, XlsxSheetState> sheet = ensureSheets().get(OBJECT_TYPE.RELATION);
        return MapUtils.isNotEmpty(sheet) ? sheet.get(name) : null;
    }
    /**
     * @return the input
     */
    public DataImportInputContext getInput() {
        return input;
    }
    /**
     * Adds an upsert context alone with xlsx (prefixed) id, coordinates and context.
     * @param xlsxId the id
     * @param coordinates the coords
     * @param ctx the context
     */
    public void add(String xlsxId, int coordinates, UpsertRequestContext ctx) {
        Objects.requireNonNull(xlsxId, "XLSX ID must not be null!");
        Objects.requireNonNull(ctx, "XLSX record upsert context must not be null!");
        recordUpserts.computeIfAbsent(xlsxId, k -> new ArrayList<>(2))
            .add(new XlsxImportUpsert(coordinates, ctx));
    }

    public void add(DeleteRequestContext dCtx) {
        Objects.requireNonNull(dCtx, "XLSX record delete context must not be null!");
        recordDeletes.add(dCtx);
    }

    public void add(UpsertRelationsRequestContext uCtx) {
        Objects.requireNonNull(uCtx, "XLSX relation upsert context must not be null!");
        relationUpserts.add(uCtx);
    }

    public void add(StatisticSheet sheet) {
        statistics.add(sheet);
    }

    public List<XlsxImportUpsert> getById(String id) {
        List<XlsxImportUpsert> l = recordUpserts.get(id);
        return Objects.isNull(l) ? Collections.emptyList() : l;
    }

    public List<UpsertRequestContext> toRecordUpserts() {
        return recordUpserts.values().stream()
                .flatMap(Collection::stream)
                .map(XlsxImportUpsert::getContext)
                .collect(Collectors.toList());
    }

    public List<DeleteRequestContext> toRecordDeletes() {
        return recordDeletes;
    }

    public List<UpsertRelationsRequestContext> toRelationUpserts() {
        return relationUpserts;
    }

    public List<StatisticSheet> toStatistics() {
        return statistics;
    }

    private Map<OBJECT_TYPE, Map<String, XlsxSheetState>> ensureSheets() {

        if (this.sheets == null) {
            this.sheets = new EnumMap<>(OBJECT_TYPE.class);
        }

        return this.sheets;
    }

    class XlsxSheetState {
        /**
         * The element imported by this sheet.
         */
        private final EntityElement entity;
        /**
         * Name to XLS column info.
         */
        private final Map<String, XLSXHeader> types;
        /**
         * Position to name info.
         */
        private final Map<Integer, String> headers;
        /**
         * Constructor.
         * @param element
         * @param types
         * @param headers
         */
        XlsxSheetState(EntityElement element, Map<String, XLSXHeader> types, Map<Integer, String> headers) {
            super();
            this.entity = element;
            this.types = types;
            this.headers = headers;
        }
        /**
         * @return the element
         */
        public EntityElement getEntity() {
            return entity;
        }
        /**
         * @return the types
         */
        public Map<String, XLSXHeader> getTypes() {
            return types;
        }
        /**
         * @return the headers
         */
        public Map<Integer, String> getHeaders() {
            return headers;
        }
    }

    class XlsxImportUpsert {

        private final int coordinates;

        private final UpsertRequestContext uCtx;

        private XlsxImportUpsert(int coordinates, UpsertRequestContext uCtxb) {
            this.coordinates = coordinates;
            this.uCtx = uCtxb;
        }

        public UpsertRequestContext getContext() {
            return uCtx;
        }
        /**
         * @return the coordinates
         */
        public int getCoordinates() {
            return coordinates;
        }
    }
}
