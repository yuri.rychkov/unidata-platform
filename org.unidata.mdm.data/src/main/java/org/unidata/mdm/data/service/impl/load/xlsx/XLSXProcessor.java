/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.data.service.impl.load.xlsx;

import java.text.ParseException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.service.MetaModelService;
import org.unidata.mdm.core.type.model.AttributeElement;
import org.unidata.mdm.core.type.model.AttributeElement.AttributeValueType;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.model.MeasurementCategoryElement;
import org.unidata.mdm.core.type.model.NestedElement;
import org.unidata.mdm.core.type.model.RelationElement;
import org.unidata.mdm.core.util.AttributeUtils;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.exception.DataProcessingException;
import org.unidata.mdm.data.service.impl.load.xlsx.XLSXHeader.TYPE;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.type.model.SimpleDataType;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;
import org.unidata.mdm.system.util.ConvertUtils;
import org.unidata.mdm.system.util.TextUtils;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

/**
 * The Class XLSXProcessor.
 */
public abstract class XLSXProcessor {

    /**
     * The Constant FROM.
     */
    protected static final String FROM = "FROM";

    /**
     * The Constant TO.
     */
    protected static final String TO = "TO";

    protected static final String NESTED_ENTITY_TITLE_LABEL = "unidata.xlsx.import_export_tool.nested.title";

    protected static final String CLASSIFIER_TITLE_LABEL = "unidata.xlsx.import_export_tool.classifier.title";

    protected static final String RELATION_TITLE_LABEL = "unidata.xlsx.import_export_tool.relation.title";

    protected static final String ENTITY_TITLE_LABEL = "unidata.xlsx.import_export_tool.entity.title";

    protected static final String LOOKUP_ENTITY_TITLE_LABEL = "unidata.xlsx.import_export_tool.lookup.title";

    protected static final String FROM_COLUMN_LABEL = "unidata.xlsx.import_export_tool.valid_from.title";

    protected static final String TO_COLUMN_LABEL = "unidata.xlsx.import_export_tool.valid_to.title";

    protected static final String IS_ACTIVE_LABEL = "unidata.xlsx.import_export_tool.is_active.title";

    private static final int DEFAULT_TIME_NANOS = (int) TimeUnit.MILLISECONDS.toNanos(999);
    /**
     * The Constant IS_ACTIVE.
     */
    protected static final String IS_ACTIVE = "IS_ACTIVE";

    protected static final String TO_ETALON_ID = "TO_ETALON_ID";

    protected static final String TO_EXTERNAL_ID = "TO_EXTERNAL_ID";

    protected static final String TO_DISPLAY_NAME = "TO_DISPLAY_NAME";

    protected static final String CLASSIFIER_NODE_ID = "CLASSIFIER_NODE_ID";

    protected static final String CLASSIFIER_NODE_NAME = "CLASSIFIER_NODE_NAME";

    protected static final String CLASSIFIER_IS_ACTIVE = "CLASSIFIER_IS_ACTIVE";
    /**
     * The Constant H_R_HEADER_IDX.
     */
    protected static final int TITLE_ROW_IDX = 0;
    /**
     * The Constant H_R_HEADER_IDX.
     */
    protected static final int H_R_HEADER_IDX = 2;

    /**
     * The Constant SYSTEM_HEADER_IDX.
     */
    protected static final int SYSTEM_HEADER_IDX = 1;
    /**
     * Quantity of the system rows.
     */
    protected static final int SYSTEM_ROWS_QTY = 3;

    /**
     * The Constant ID.
     */
    protected static final String ID = "ID";
    /**
     * The Constant ID.
     */
    protected static final String ETALON_ID = "ETALON_ID";
    /**
     * The Constant EXTERNAL ID.
     */
    protected static final String EXTERNAL_ID = "EXTERNAL_ID";
    /**
     * The Constant EXTERNAL KEYS.
     */
    protected static final String EXTERNAL_KEYS = "EXTERNAL_KEYS";
    /**
     * The Constant H_R_PATH_DELIMITER.
     */
    protected static final String H_R_PATH_DELIMITER = " >> ";

    /**
     * The Constant SYSTEM_PATH_DELIMITER.
     */
    protected static final String SYSTEM_PATH_DELIMITER = ".";

    /**
     * The Constant MURMUR.
     */
    protected static final HashFunction MURMUR = Hashing.murmur3_128();

    /**
     * The Constant XLSX_FORMATTER.
     */
    protected static final DataFormatter XLSX_FORMATTER = new DataFormatter();

    /**
     * The date formats.
     */
    private static final String[] DATE_FORMATS = {
            "dd.MM.yyyy HH:mm:ss",
            "dd.MM.yyyy HH:mm:ss.SSS",
            "dd.MM.yyyy HH:mm",
            "dd.MM.yyyy HH",
            "dd.MM.yyyy",
            "HH:mm:ss",
            "HH:mm:ss.SSS",
            "M/dd/yyyy",
            "dd.M.yyyy",
            "M/dd/yyyy HH:mm:ss a",
            "M/dd/yyyy HH:mm:ss.SSS a",
            "dd.M.yyyy HH:mm:ss a",
            "dd.M.yyyy HH:mm:ss.SSS a",
            "dd.MMM.yyyy",
            "dd-MMM-yyyy"
    };
    /**
     * The Constant EXCEL_DATE_FORMAT.
     */
    public static final DateTimeFormatter EXCEL_DATE_FORMAT = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    /**
     * The Constant EXCEL_TIME_FORMAT.
     */
    public static final DateTimeFormatter EXCEL_TIME_FORMAT = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
    /**
     * The Constant EXCEL_DATE_TIME_FORMAT.
     */
    public static final DateTimeFormatter EXCEL_DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss.SSS");

    private static final Pattern DATE_PATTERN = Pattern.compile("^\\d{2}\\D\\d{2}\\D\\d{4}$");
    private static final Pattern DATE_PATTERN_WITH_HH = Pattern.compile("^\\d{2}\\D\\d{2}\\D\\d{4}\\D\\d{2}$");
    private static final Pattern DATE_PATTERN_WITH_HHMM = Pattern.compile("^\\d{2}\\D\\d{2}\\D\\d{4}\\D\\d{2}\\D\\d{2}$");
    private static final Pattern DATE_PATTERN_WITH_HHMMSS = Pattern.compile("^\\d{2}\\D\\d{2}\\D\\d{4}\\D\\d{2}\\D\\d{2}\\D\\d{2}$");
    private static final Pattern DATE_PATTERN_WITH_HHMMSS_SSS = Pattern.compile("^\\d{2}\\D\\d{2}\\D\\d{4}\\D\\d{2}\\D\\d{2}\\D\\d{2}\\D\\d{3}$");

    /**
     * The meta model service.
     */
    @Autowired
    protected MetaModelService metaModelService;
    /**
     * Convert cell.
     *
     * @param cell the cell
     * @param header the type
     * @param evaluator evaluator null
     * @return the object
     */
    protected Object convertCell(Cell cell, XLSXHeader header, FormulaEvaluator evaluator) {

        if (Objects.isNull(cell)) {
            return null;
        }

        try {

            AttributeElement aih = header.getAttributeHolder();
            if (aih == null) {
                return null;
            }
            if (aih.isSimple()) {

                AttributeValueType type = aih.getValueType();
                switch (type) {
                    case BOOLEAN:
                        return convertToBoolean(cell, evaluator);
                    case BLOB:
                    case CLOB:
                    case ANY:
                    case STRING:
                        return convertToString(cell, evaluator);
                    case DATE:
                        return ConvertUtils.date2LocalDate(convertToDate(cell, evaluator, SimpleDataType.DATE));
                    case TIME:
                        return ConvertUtils.date2LocalTime(convertToDate(cell, evaluator, SimpleDataType.TIME));
                    case TIMESTAMP:
                        return ConvertUtils.date2LocalDateTime(convertToDate(cell, evaluator, SimpleDataType.TIMESTAMP));
                    case INTEGER:
                        return convertToLong(cell, evaluator);
                    case NUMBER:
                    case MEASURED:
                        return convertToNumber(cell, evaluator);
                    default:
                        break;
                }
            } else if (aih.isCode()) {
                switch (aih.getValueType()) {
                    case INTEGER:
                        return convertToLong(cell, evaluator);
                    case STRING:
                        return convertToString(cell, evaluator);
                    default:
                        break;
                }
            } else if (aih.isArray()) {
                // Array values are always strings
                String[] cellValue = AttributeUtils.splitArrayValues(convertToString(cell, evaluator), aih.getExchangeSeparator());
                if (cellValue == null) {
                    return cellValue;
                }

                List<Object> result = new ArrayList<>();
                switch (aih.getValueType()) {
                    case STRING:
                        result = Arrays.asList((Object[]) cellValue);
                        break;
                    case NUMBER:
                        result = Arrays.stream(cellValue).map(Double::valueOf).collect(Collectors.toList());
                        break;
                    case INTEGER:
                        result = Arrays.stream(cellValue).map(Long::valueOf).collect(Collectors.toList());
                        break;
                    case DATE:
                        for (String c : cellValue) {
                            result.add(ConvertUtils.date2LocalDate(DateUtils.parseDate(c, DATE_FORMATS)));
                        }
                        break;
                    case TIME:
                        for (String c : cellValue) {
                            result.add(ConvertUtils.date2LocalTime(DateUtils.parseDate(c, DATE_FORMATS)));
                        }
                        break;
                    case TIMESTAMP:
                        for (String c : cellValue) {
                            result.add(ConvertUtils.date2LocalDateTime(DateUtils.parseDate(c, DATE_FORMATS)));
                        }
                        break;
                    default:
                        break;
                }

                return result;
            }
        } catch (ParseException e) {
            throw new DataProcessingException(
                    "Unable to parse incoming import file. Invalid cell format: row " +
                            getExcelCoordinates(cell.getRowIndex()) +
                            ", column " +
                            getExcelCoordinates(cell.getColumnIndex()) +
                            ".",
                    DataExceptionIds.EX_DATA_XLSX_IMPORT_PARSE_FILE_INVALID_CELL_FORMAT,
                    cell.getSheet().getSheetName(),
                    getExcelCoordinates(cell.getRowIndex()),
                    getExcelCoordinates(cell.getColumnIndex()),

                    TextUtils.getText(SimpleDataType.DATE),
                    e);
        }

        return null;
    }

    protected Date convertValidityPeriod(Cell cell, FormulaEvaluator evaluator, boolean periodFrom) {
        try {
            if (cell == null) {
                return null;
            }
            if (cell.getCellType() == CellType.NUMERIC) {
                if (periodFrom) {
                    return cell.getDateCellValue();
                }
                ZonedDateTime i = cell.getDateCellValue().toInstant().atZone(ZoneId.systemDefault());
                return normalizeTo(i, i.getHour(), i.getMinute(), i.getSecond(), DEFAULT_TIME_NANOS);
            }

            String asString = convertToString(cell, evaluator);
            if (StringUtils.isEmpty(asString)) {
                return null;
            }

            return periodFrom ? DateUtils.parseDate(asString, DATE_FORMATS)
                    : normalizeTo(asString.trim(), DateUtils.parseDate(asString, DATE_FORMATS));

        } catch (ParseException e) {
            throw new DataProcessingException(
                    "Unable to parse incoming import file. Invalid cell format: row " +
                            getExcelCoordinates(cell.getRowIndex()) +
                            ", column " +
                            getExcelCoordinates(cell.getColumnIndex()) +
                            ".",
                    DataExceptionIds.EX_DATA_XLSX_IMPORT_PARSE_FILE_INVALID_CELL_FORMAT,
                    cell.getSheet().getSheetName(),
                    getExcelCoordinates(cell.getRowIndex()),
                    getExcelCoordinates(cell.getColumnIndex()),
                    TextUtils.getText(SimpleDataType.DATE),
                    e);

        }
    }

    /**
     * Convert to boolean.
     *
     * @param cell the cell
     * @param evaluator the evaluator
     * @return the boolean
     */
    protected Boolean convertToBooleanWithDefault(Cell cell, FormulaEvaluator evaluator, Boolean defaultValue) {

        if (cell == null) {
            return defaultValue;
        }

        Boolean result = convertToBoolean(cell, evaluator);
        return result == null ? defaultValue : result;
    }

    /**
     * Convert to boolean.
     *
     * @param cell the cell
     * @param evaluator the evaluator
     * @return the boolean
     */
    protected Boolean convertToBoolean(Cell cell, FormulaEvaluator evaluator) {

        if (cell.getCellType() == CellType.BOOLEAN) {
            return cell.getBooleanCellValue();
        }

        String asString = convertToString(cell, evaluator);
        if (StringUtils.isEmpty(asString)) {
            return null;
        }

        if (asString.length() == 1 && asString.charAt(0) == '0') {
            return Boolean.FALSE;
        } else if (asString.length() == 1 && asString.charAt(0) == '1') {
            return Boolean.TRUE;
        }

        Boolean result =  BooleanUtils.toBooleanObject(asString);
        if (result == null) {
            throw new DataProcessingException(
                    "Unable to parse incoming import file. Invalid cell format: row " +
                            getExcelCoordinates(cell.getRowIndex()) +
                            ", column " +
                            getExcelCoordinates(cell.getColumnIndex()) +
                            ".",
                    DataExceptionIds.EX_DATA_XLSX_IMPORT_PARSE_FILE_INVALID_CELL_FORMAT,
                    cell.getSheet().getSheetName(),
                    getExcelCoordinates(cell.getRowIndex()),
                    getExcelCoordinates(cell.getColumnIndex()),
                    TextUtils.getText(SimpleDataType.BOOLEAN));
        }
        return result;
    }

    /**
     * Convert to date.
     *
     * @param cell the cell
     * @param evaluator the evaluator
     * @return the date
     */
    protected Date convertToDate(Cell cell, FormulaEvaluator evaluator, SimpleDataType type) {
        try {
            if (cell == null) {
                return null;
            }
            if (cell.getCellType() == CellType.NUMERIC) {
                return cell.getDateCellValue();
            }

            String asString = convertToString(cell, evaluator);
            if (StringUtils.isEmpty(asString)) {
                return null;
            }
            return DateUtils.parseDate(asString, DATE_FORMATS);
        } catch (ParseException e) {
            throw new DataProcessingException(
                    "Unable to parse incoming import file. Invalid cell format: row " +
                            getExcelCoordinates(cell.getRowIndex()) +
                            ", column " +
                            getExcelCoordinates(cell.getColumnIndex()) +
                            ".",
                    DataExceptionIds.EX_DATA_XLSX_IMPORT_PARSE_FILE_INVALID_CELL_FORMAT,
                    cell.getSheet().getSheetName(),
                    getExcelCoordinates(cell.getRowIndex()),
                    getExcelCoordinates(cell.getColumnIndex()),
                    TextUtils.getText(type),
                    e);

        }
    }

    /**
     * Convert to Long.
     *
     * @param cell the cell
     * @param evaluator the formula evaluator
     * @return the Long
     */
    protected Long convertToLong(Cell cell, FormulaEvaluator evaluator) {

        if (cell.getCellType() == CellType.NUMERIC) {
            return (long) cell.getNumericCellValue();
        }

        String asString = convertToString(cell, evaluator);
        if (StringUtils.isEmpty(asString)) {
            return null;
        }

        asString = asString.trim();
        try {
            return Long.parseLong(asString);
        } catch (NumberFormatException e) {
            throw new DataProcessingException(
                    "Unable to parse incoming import file. Invalid cell format: row " +
                            getExcelCoordinates(cell.getRowIndex()) +
                            ", column " +
                            getExcelCoordinates(cell.getColumnIndex()) +
                            ".",
                    DataExceptionIds.EX_DATA_XLSX_IMPORT_PARSE_FILE_INVALID_CELL_FORMAT,
                    cell.getSheet().getSheetName(),
                    getExcelCoordinates(cell.getRowIndex()),
                    getExcelCoordinates(cell.getColumnIndex()),
                    TextUtils.getText(SimpleDataType.INTEGER),
                    e);
        }
    }

    /**
     * Convert to number.
     *
     * @param cell the cell
     * @param evaluator formula evaluator
     * @return the double
     */
    protected Double convertToNumber(Cell cell, FormulaEvaluator evaluator) {

        if (cell.getCellType() == CellType.NUMERIC) {
            return cell.getNumericCellValue();
        }

        String asString = convertToString(cell, evaluator);
        if (StringUtils.isEmpty(asString)) {
            return null;
        }

        //UN-3841 - we made a decision replace symbols, because a server locale is Russian always.
        // MSK locale for UI.
        asString = asString.replace(",", ".").trim();
        try {
            return Double.parseDouble(asString);
        } catch (NumberFormatException e) {
            throw new DataProcessingException(
                    "Unable to parse incoming import file. Invalid cell format: row " +
                            getExcelCoordinates(cell.getRowIndex()) +
                            ", column " +
                            getExcelCoordinates(cell.getColumnIndex()) +
                            ".",
                    DataExceptionIds.EX_DATA_XLSX_IMPORT_PARSE_FILE_INVALID_CELL_FORMAT,
                    cell.getSheet().getSheetName(),
                    getExcelCoordinates(cell.getRowIndex()),
                    getExcelCoordinates(cell.getColumnIndex()),
                    TextUtils.getText(SimpleDataType.NUMBER),
                    e);
        }
    }

    /**
     * Convert to string.
     *
     * @param cell the cell
     * @param evaluator the evaluator
     * @return string
     */
    protected String convertToString(Cell cell, FormulaEvaluator evaluator) {

        String result = evaluator == null
                ? XLSX_FORMATTER.formatCellValue(cell)
                : XLSX_FORMATTER.formatCellValue(cell, evaluator);

        return "".equals(result) ? null : result;
    }

    /**
     * Checks if is empty row.
     *
     * @param row the row
     * @return true, if is empty row
     */
    protected boolean isEmptyRow(Row row) {
        if (row == null) {
            return true;
        }
        for (int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++) {
            Cell cell = row.getCell(cellNum);
            if (cell != null && cell.getCellType() != CellType.BLANK && StringUtils.isNotBlank(cell.toString())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Creates the headers.
     *
     * @param entityName the entity name
     * @return the list
     */
    protected List<XLSXHeader> createHeaders(EntityElement el) {

        List<XLSXHeader> headers = new ArrayList<>();

        // Add default headers
        // Record ID
        headers.add(new XLSXHeader()
                .withSystemHeader(ID)
                .withHrHeader("")
                .withTypeHeader(AttributeValueType.STRING)
                .withType(TYPE.SYSTEM)
                .withIsMandatory(true));

        // Etalon ID
        headers.add(new XLSXHeader()
                .withSystemHeader(ETALON_ID)
                .withHrHeader("")
                .withTypeHeader(AttributeValueType.STRING)
                .withType(TYPE.SYSTEM)
                .withIsMandatory(true));

        // External Record ID
        headers.add(new XLSXHeader()
                .withSystemHeader(EXTERNAL_ID)
                .withHrHeader("")
                .withTypeHeader(AttributeValueType.STRING)
                .withType(TYPE.SYSTEM)
                .withIsMandatory(true));

        // Left validity period boundary
        headers.add(new XLSXHeader()
                .withSystemHeader(FROM)
                .withHrHeader(TextUtils.getText(FROM_COLUMN_LABEL))
                .withTypeHeader(AttributeValueType.TIMESTAMP)
                .withType(TYPE.SYSTEM)
                .withIsMandatory(true));

        // Right validity period boundary
        headers.add(new XLSXHeader()
                .withSystemHeader(TO)
                .withHrHeader(TextUtils.getText(TO_COLUMN_LABEL))
                .withTypeHeader(AttributeValueType.TIMESTAMP)
                .withType(TYPE.SYSTEM)
                .withIsMandatory(true));

        // Sign of period activity
        headers.add(new XLSXHeader()
                .withSystemHeader(IS_ACTIVE)
                .withHrHeader(TextUtils.getText(IS_ACTIVE_LABEL))
                .withTypeHeader(AttributeValueType.BOOLEAN)
                .withType(TYPE.SYSTEM)
                .withIsMandatory(true));

        // All External Keys (For display)
        headers.add(new XLSXHeader()
                .withSystemHeader(EXTERNAL_KEYS)
                .withHrHeader("")
                .withTypeHeader(AttributeValueType.STRING)
                .withType(TYPE.SYSTEM)
                .withIsMandatory(false));

        // Fill headers with attributes data
        Map<String, AttributeElement> attrsInfo = el.getAttributes();
        for (Entry<String, AttributeElement> entry : attrsInfo.entrySet()) {

            AttributeElement attr = entry.getValue();
            if (attr.isComplex()
             || attr.isLinkTemplate()
             || attr.isBlob()
             || attr.isClob()
             || attr.hasParent()) {
                // Skip
                continue;
            }

            if (entry.getValue().isCode()) {
                headers.add(codeAttributeToHeader(null, entry.getValue()));
            } else if (entry.getValue().isArray()) {
                headers.add(arrayAttributeToHeader(null, entry.getValue()));
            } else if (entry.getValue().isSimple()) {
                headers.add(simpleAttributeToHeader(null, entry.getValue()));
            }
        }

        for (int i = 0; i < headers.size(); i++) {
            headers.get(i).setOrder(i);
        }

        return headers;
    }

    protected int getExcelCoordinates(int coord) {
        return coord + 1;
    }

    protected List<XLSXHeader> createNestedEntityHeaders(NestedElement el) {

        List<XLSXHeader> headers = new ArrayList<>();

        // Add default headers
        // Record ID
        headers.add(new XLSXHeader()
                .withSystemHeader(ID)
                .withHrHeader("")
                .withTypeHeader(AttributeValueType.STRING)
                .withType(TYPE.SYSTEM)
                .withIsMandatory(true));

        // Etalon Record ID
        headers.add(new XLSXHeader()
                .withSystemHeader(ETALON_ID)
                .withHrHeader("")
                .withTypeHeader(AttributeValueType.STRING)
                .withType(TYPE.SYSTEM)
                .withIsMandatory(true));

        // External Record ID
        headers.add(new XLSXHeader()
                .withSystemHeader(EXTERNAL_ID)
                .withHrHeader("")
                .withTypeHeader(AttributeValueType.STRING)
                .withType(TYPE.SYSTEM)
                .withIsMandatory(true));

        // All External Keys (For display)
        // Left validity period boundary
        headers.add(new XLSXHeader()
                .withSystemHeader(FROM)
                .withHrHeader(TextUtils.getText(FROM_COLUMN_LABEL))
                .withTypeHeader(AttributeValueType.TIMESTAMP)
                .withType(TYPE.SYSTEM)
                .withIsMandatory(true));

        // Right validity period boundary
        headers.add(new XLSXHeader()
                .withSystemHeader(TO)
                .withHrHeader(TextUtils.getText(TO_COLUMN_LABEL))
                .withTypeHeader(AttributeValueType.TIMESTAMP)
                .withType(TYPE.SYSTEM)
                .withIsMandatory(true));

        // All External Keys (For display)
        headers.add(new XLSXHeader()
                .withSystemHeader(EXTERNAL_KEYS)
                .withHrHeader("")
                .withTypeHeader(AttributeValueType.STRING)
                .withType(TYPE.SYSTEM)
                .withIsMandatory(false));

        // fill headers with attributes data
        Map<String, AttributeElement> attrsInfo = el.getAttributes();
        for (Entry<String, AttributeElement> entry : attrsInfo.entrySet()) {

            if (entry.getValue().isComplex()
             || entry.getValue().isLinkTemplate()
             || entry.getValue().isBlob()
             || entry.getValue().isClob()
             || entry.getValue().getLevel() > 0) {
                // Skip
                continue;
            }

            if (entry.getValue().isCode()) {
                headers.add(codeAttributeToHeader(null, entry.getValue()));
            } else if (entry.getValue().isArray()) {
                headers.add(arrayAttributeToHeader(null, entry.getValue()));
            } else if (entry.getValue().isSimple()) {
                headers.add(simpleAttributeToHeader(null, entry.getValue()));
            }
        }

        for (int i = 0; i < headers.size(); i++) {
            headers.get(i).setOrder(i);
        }

        return headers;
    }

    protected List<XLSXHeader> createRelationHeaders(RelationElement relation) {

        MeasurementPoint.start();
        try {

            List<XLSXHeader> headers = new ArrayList<>();

            // Add default headers
            // Record ID
            headers.add(new XLSXHeader()
                    .withSystemHeader(ID)
                    .withHrHeader("")
                    .withTypeHeader(AttributeValueType.STRING)
                    .withType(TYPE.SYSTEM).withIsMandatory(true));

            // Etalon From ID
            headers.add(new XLSXHeader()
                    .withSystemHeader(ETALON_ID)
                    .withHrHeader("Left (outgoing) register record system identity " + relation.getLeft().getDisplayName())
                    .withTypeHeader(AttributeValueType.STRING)
                    .withType(TYPE.SYSTEM)
                    .withIsMandatory(true));

            // External Record ID
            headers.add(new XLSXHeader()
                    .withSystemHeader(EXTERNAL_ID)
                    .withHrHeader("Left (outgoing) register record external ID " + relation.getLeft().getDisplayName())
                    .withTypeHeader(AttributeValueType.STRING)
                    .withType(TYPE.SYSTEM)
                    .withIsMandatory(true));

            // Left validity period boundary
            headers.add(new XLSXHeader()
                    .withSystemHeader(FROM)
                    .withHrHeader(TextUtils.getText(FROM_COLUMN_LABEL))
                    .withTypeHeader(AttributeValueType.TIMESTAMP)
                    .withType(TYPE.SYSTEM)
                    .withIsMandatory(true));

            // Right validity period boundary
            headers.add(new XLSXHeader()
                    .withSystemHeader(TO)
                    .withHrHeader(TextUtils.getText(TO_COLUMN_LABEL))
                    .withTypeHeader(AttributeValueType.TIMESTAMP)
                    .withType(TYPE.SYSTEM)
                    .withIsMandatory(true));

            // ADD RELATION HEADERS
            // TO etalon id
            String systemHeader = TO_ETALON_ID;
            headers.add(new XLSXHeader()
                    .withHrHeader("Right (incoming) register record system identity " + relation.getRight().getDisplayName())
                    .withType(TYPE.SYSTEM)
                    .withTypeHeader(AttributeValueType.STRING)
                    .withSystemHeader(systemHeader));

            // TO external Record ID
            headers.add(new XLSXHeader()
                    .withSystemHeader(TO_EXTERNAL_ID)
                    .withHrHeader("Right (incoming) register record external ID " + relation.getRight().getDisplayName())
                    .withTypeHeader(AttributeValueType.STRING)
                    .withType(TYPE.SYSTEM)
                    .withIsMandatory(true));

            // All External Keys (For display)
            headers.add(new XLSXHeader()
                    .withSystemHeader(EXTERNAL_KEYS)
                    .withHrHeader("")
                    .withTypeHeader(AttributeValueType.STRING)
                    .withType(TYPE.SYSTEM)
                    .withIsMandatory(false));

            // main display name
            systemHeader = TO_DISPLAY_NAME;
            headers.add(new XLSXHeader()
                    .withHrHeader("Right (incoming) register record identity " + relation.getRight().getDisplayName())
                    .withType(TYPE.SYSTEM)
                    .withTypeHeader(AttributeValueType.STRING)
                    .withSystemHeader(systemHeader));

            // add simple attributes to headers
            Map<String, AttributeElement> attrs = relation.isContainment()
                    ? relation.getRight().getAttributes()
                    : relation.getAttributes();

            for (Entry<String, AttributeElement> entry : attrs.entrySet()) {

                if (!entry.getValue().isSimple()) {
                    continue;
                }

                if (!entry.getValue().isLinkTemplate()
                 && !entry.getValue().isBlob()
                 && !entry.getValue().isClob()) {
                    headers.add(simpleAttributeToHeader(null, entry.getValue())
                            .withType(TYPE.RELATION)
                            .withIsRel(true));
                }
            }

            for (int i = 0; i < headers.size(); i++) {
                headers.get(i).setOrder(i);
            }

            return headers;
        } finally {
            MeasurementPoint.stop();
        }
    }
    /**
     * Simple attribute to header.
     *
     * @param attributeHolder the simple attribute def
     * @return the XLSX header
     */
    protected XLSXHeader simpleAttributeToHeader(String prefix, AttributeElement attributeHolder) {

        Pair<String, String> paths = getPaths(prefix, attributeHolder);
        String unit = "";
        if (attributeHolder.isMeasured()) {

            MeasurementCategoryElement el = metaModelService.instance(Descriptors.MEASUREMENT_UNITS)
                .getCategory(attributeHolder.getMeasured().getCategoryId());

            unit = " (" + el.getBaseUnit().getDisplayName() + ")";
        }

        return new XLSXHeader()
                .withType(TYPE.DATA_ATTRIBUTE)
                .withIsMandatory(!attributeHolder.isNullable())
                .withSystemHeader(paths.getLeft())
                .withHrHeader(paths.getRight() + unit)
                .withTypeHeader(attributeHolder.getValueType())
                .withAttributeHolder(attributeHolder);
    }

    /**
     * Simple attribute to header.
     *
     * @param attributeHolder the simple attribute def
     * @return the XLSX header
     */
    protected XLSXHeader arrayAttributeToHeader(String prefix, AttributeElement attributeHolder) {

        Pair<String, String> paths = getPaths(prefix, attributeHolder);
        return new XLSXHeader()
                .withType(TYPE.DATA_ATTRIBUTE)
                .withIsMandatory(!attributeHolder.isNullable())
                .withSystemHeader(paths.getLeft())
                .withHrHeader(paths.getRight())
                .withTypeHeader(attributeHolder.getValueType())
                .withAttributeHolder(attributeHolder);
    }

    /**
     * Code attribute to header.
     *
     * @param attributeHolder the code attribute def
     * @return the XLSX header
     */
    protected XLSXHeader codeAttributeToHeader(String prefix, AttributeElement attributeHolder) {

        Pair<String, String> paths = getPaths(prefix, attributeHolder);
        return new XLSXHeader()
                .withType(TYPE.DATA_ATTRIBUTE)
                .withIsMandatory(true)
                .withSystemHeader(paths.getLeft())
                .withHrHeader(paths.getRight())
                .withTypeHeader(attributeHolder.getValueType())
                .withAttributeHolder(attributeHolder);
    }

    /**
     * Gets sys path and display path as a pair.
     *
     * @param attributeHolder holder being processed
     * @return pair, where left is sys path and right is display names path
     */
    protected Pair<String, String> getPaths(String prefix, AttributeElement attributeHolder) {


        List<String> displayNames = new ArrayList<>();
        AttributeElement next = attributeHolder;
        AttributeElement current;
        do {
            current = next;
            displayNames.add(current.getDisplayName());
            next = current.getParent();
        }
        while (next != null);

        displayNames.add(current.getContainer().getDisplayName());

        Collections.reverse(displayNames);

        if (prefix != null) {
            return Pair.of(
                    String.join(SYSTEM_PATH_DELIMITER, prefix, current.getContainer().getName(), attributeHolder.getPath()),
                    String.join(H_R_PATH_DELIMITER, displayNames));
        } else {
            return Pair.of(
                    String.join(SYSTEM_PATH_DELIMITER, current.getContainer().getName(), attributeHolder.getPath()),
                    String.join(H_R_PATH_DELIMITER, displayNames));

        }
    }

    /**
     * Creates the template workbook.
     *
     * @param entityName the entity name
     * @return the workbook
     */
    protected Workbook createTemplateWorkbook(String entityName) {

        EntityElement el = metaModelService.instance(Descriptors.DATA)
                .getElement(entityName);

        // 1. create new workbook
        final Workbook wb = new SXSSFWorkbook();

        List<XLSXHeader> headers = createHeaders(el);
        Sheet sheet = wb.createSheet(buildSheetName(entityName, OBJECT_TYPE.ENTITY, (short) 1));

        fillTitleRow(el.isLookup() ? LOOKUP_ENTITY_TITLE_LABEL : ENTITY_TITLE_LABEL, el.getName(), el.getDisplayName(), sheet, wb);

        fillHeaders(wb, headers, sheet);

        if (el.isRegister()) {

            // 2. Fill sheets for nested entities (only for entities)
            Short num = (short) 1;
            for (NestedElement ne : el.getRegister().getReferencedNesteds()) {
                List<XLSXHeader> nestedHeaders = createNestedEntityHeaders(ne);
                Sheet nestedSheet = wb.createSheet(buildSheetName(ne.getName(), OBJECT_TYPE.NESTED, num++));
                fillTitleRow(NESTED_ENTITY_TITLE_LABEL, ne.getName(), ne.getDisplayName(), nestedSheet, wb);
                fillHeaders(wb, nestedHeaders, nestedSheet);
            }

            // 3. Fill sheets for relations
            num = (short) 1;
            for (Entry<RelationElement, EntityElement> ree : el.getRegister().getOutgoingRelations().entrySet()) {
                RelationElement re = ree.getKey();
                List<XLSXHeader> relationHeaders = createRelationHeaders(re);
                Sheet relationSheet = wb.createSheet(buildSheetName(re.getName(), OBJECT_TYPE.RELATION, num++));
                fillTitleRow(RELATION_TITLE_LABEL, re.getName(), re.getDisplayName(), relationSheet, wb);
                fillHeaders(wb, relationHeaders, relationSheet);
            }
        }

        return wb;
    }

    private void fillHeaders(Workbook wb, List<XLSXHeader> headers, Sheet sheet) {

        // create cell style
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setWrapText(true);
        cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);

        CellStyle systemCellStyle = wb.createCellStyle();
        systemCellStyle.setWrapText(true);
        systemCellStyle.setFillForegroundColor(IndexedColors.PALE_BLUE.index);
        systemCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        systemCellStyle.setBorderLeft(BorderStyle.THIN);
        systemCellStyle.setBorderRight(BorderStyle.THIN);

        Row sysRowRelation = sheet.createRow(SYSTEM_HEADER_IDX);
        Row hrRowRelation = sheet.createRow(H_R_HEADER_IDX);

        for (XLSXHeader header : headers) {
            Cell sysCell = sysRowRelation.createCell(header.getOrder(), CellType.STRING);
            sysCell.setCellStyle(header.getType() == TYPE.SYSTEM ? systemCellStyle : cellStyle);
            sysCell.setCellValue(header.getSystemHeader());
            Cell hrCell = hrRowRelation.createCell(header.getOrder(), CellType.STRING);
            hrCell.setCellStyle(header.getType() == TYPE.SYSTEM ? systemCellStyle : cellStyle);
            hrCell.setCellValue(header.getHrHeader());
            if (sheet instanceof SXSSFSheet) {
                ((SXSSFSheet) sheet).trackColumnForAutoSizing(header.getOrder());
            }
            sheet.autoSizeColumn(header.getOrder());
        }
        // block editing for headers
        sheet.createFreezePane(0, SYSTEM_ROWS_QTY);
    }

    public enum OBJECT_TYPE {
        ENTITY("E<"),
        NESTED("N<"),
        RELATION("R<"),
        COMPLEX_ATTRIBUTE("CA<");

        OBJECT_TYPE(String value) {
            this.value = value;
        }

        private final String value;

        public String value() {
            return value;
        }
    }

    protected class StatisticSheet {

        public StatisticSheet(OBJECT_TYPE objectType, String name, String displayName, Long totalSize) {
            this.objectType = objectType;
            this.name = name;
            this.displayName = displayName;
            this.totalSize = totalSize;
        }

        private OBJECT_TYPE objectType;
        private String name;
        private String displayName;
        private Long totalSize = 0l;

        public StatisticSheet(OBJECT_TYPE objectType, String name, String displayName, Long totalSize, Long additionalInfoSkipedSize) {
            this.objectType = objectType;
            this.name = name;
            this.displayName = displayName;
            this.totalSize = totalSize;
            this.additionalInfoSkipedSize = additionalInfoSkipedSize;
        }

        /**
         * skip data in parse process
         */
        private Long additionalInfoSkipedSize = 0l;

        public OBJECT_TYPE getObjectType() {
            return objectType;
        }

        public String getName() {
            return name;
        }

        public String getDisplayName() {
            return displayName;
        }

        public Long getTotalSize() {
            return totalSize;
        }

        public Long getAdditionalInfoSkipedSize() {
            return additionalInfoSkipedSize;
        }
    }

    protected String buildSheetName(@NotNull String name, OBJECT_TYPE type, Short num) {
        return StringUtils.left(type.value() + name, 27) + ">" + num.toString();
    }

    protected Sheet identifySheet(Workbook wb, String name, OBJECT_TYPE type) {
        Sheet result = null;
        for (Sheet sheet : wb) {
            if (sheet.getSheetName().startsWith(type.value())
                    && name.equals(StringUtils.substringBefore(
                    sheet.getRow(TITLE_ROW_IDX).getCell(0).getStringCellValue(), H_R_PATH_DELIMITER))) {
                result = sheet;
                break;
            }
        }
        return result;
    }

    private void fillTitleRow(String label, String name, String displayName, Sheet sheet, Workbook wb) {

        CellStyle cellStyle = wb.createCellStyle();
        Font titleFont = wb.createFont();
        titleFont.setFontHeightInPoints((short) 14);
        cellStyle.setFont(titleFont);

        Row titleRow = sheet.createRow(TITLE_ROW_IDX);
        Cell titleCell = titleRow.createCell(0, CellType.STRING);
        StringBuilder titleValue = new StringBuilder(name)
                .append(H_R_PATH_DELIMITER)
                .append(TextUtils.getText(label))
                .append(": ")
                .append(displayName);

        titleCell.setCellValue(titleValue.toString());
        titleCell.setCellStyle(cellStyle);

        sheet.addMergedRegion(new CellRangeAddress(TITLE_ROW_IDX, TITLE_ROW_IDX, 0, 10));
    }

    private static Date normalizeTo(String dateAsString, Date convertedFromString){

        if(convertedFromString == null){
            return convertedFromString;
        }

        ZonedDateTime i = convertedFromString.toInstant().atZone(ZoneId.systemDefault());
        if(DATE_PATTERN_WITH_HHMMSS_SSS.matcher(dateAsString).matches()){
            return normalizeTo(i, i.getHour(), i.getMinute(), i.getSecond(), i.getNano());
        } else if(DATE_PATTERN.matcher(dateAsString).matches()){
            return normalizeTo(i.toInstant().atZone(ZoneId.systemDefault()), 23, 59, 59, DEFAULT_TIME_NANOS);
        } else if(DATE_PATTERN_WITH_HHMMSS.matcher(dateAsString).matches()){
            return normalizeTo(i, i.getHour(), i.getMinute(), i.getSecond(), DEFAULT_TIME_NANOS);
        } else if(DATE_PATTERN_WITH_HHMM.matcher(dateAsString).matches()){
            return normalizeTo(i, i.getHour(), i.getMinute(), 59, DEFAULT_TIME_NANOS);
        } else if(DATE_PATTERN_WITH_HH.matcher(dateAsString).matches()){
            return normalizeTo(i, i.getHour(), 59, 59, DEFAULT_TIME_NANOS);
        }
        return convertedFromString;
    }

    public static Date normalizeTo(ZonedDateTime to, int hour, int minute, int second, int nanos) {

        if (Objects.isNull(to)) {
            return null;
        }

        ZonedDateTime zdt = ZonedDateTime.of(to.getYear(), to.getMonthValue(), to.getDayOfMonth(),
                hour, minute, second, nanos, ZoneId.systemDefault());
        return Date.from(zdt.toInstant());
    }

}
