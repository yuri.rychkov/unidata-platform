/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.type.draft;

import java.util.Set;

/**
 * @author Mikhail Mikhailov on Apr 16, 2021
 * Tags, additionally exported with data draft objects.
 */
public class DataDraftTags {
    /**
     * Entity (register, lookup or relation) name.
     */
    public static final String ENTITY_NAME = "entity-name";
    /**
     * Namespace ID.
     */
    public static final String NAMESPACE = "namespace";
    /**
     * Current operation code.
     */
    public static final String OPERATION_CODE = "operation-code";
    /**
     * Record external ID tag.
     */
    public static final String RECORD_EXTERNAL_ID = "record-external-id";
    /**
     * Specific non-public relation code (from etalon id).
     */
    public static final String RELATION_FROM_ETALON_ID = "relation-from-etalon-id";
    /**
     * Specific non-public relation code (from external id).
     */
    public static final String RELATION_FROM_EXTERNAL_ID = "relation-from-external-id";
    /**
     * Specific non-public relation code (to etalon id).
     */
    public static final String RELATION_TO_ETALON_ID = "relation-to-etalon-id";
    /**
     * Specific non-public relation code (to external id).
     */
    public static final String RELATION_TO_EXTERNAL_ID = "relation-to-external-id";
    /**
     * All tags as list to be exported by providers.
     */
    public static final Set<String> RECORD_SYSTEM_TAGS = Set.of(
            ENTITY_NAME,
            NAMESPACE,
            OPERATION_CODE,
            RECORD_EXTERNAL_ID);
    /**
     * All tags as list to be exported by providers.
     */
    public static final Set<String> RELATION_SYSTEM_TAGS = Set.of(
            ENTITY_NAME,
            NAMESPACE,
            OPERATION_CODE,
            RELATION_FROM_ETALON_ID,
            RELATION_FROM_EXTERNAL_ID,
            RELATION_TO_ETALON_ID,
            RELATION_TO_EXTERNAL_ID);
    /**
     * Constructor.
     */
    private DataDraftTags() {
        super();
    }
}
