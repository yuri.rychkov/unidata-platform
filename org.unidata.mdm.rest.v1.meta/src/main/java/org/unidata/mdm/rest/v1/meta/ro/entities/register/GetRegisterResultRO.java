package org.unidata.mdm.rest.v1.meta.ro.entities.register;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.meta.ro.entities.RegisterEntityRO;

/**
 * Get register result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class GetRegisterResultRO extends DetailedOutputRO {

    private RegisterEntityRO registerEntity;

    public RegisterEntityRO getRegisterEntity() {
        return registerEntity;
    }

    public void setRegisterEntity(RegisterEntityRO registerEntity) {
        this.registerEntity = registerEntity;
    }

}
