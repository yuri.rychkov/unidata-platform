package org.unidata.mdm.rest.v1.meta.ro.system;

/**
 * Upsert source system request
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class UpsertSystemRequestRO {

    private SourceSystemRO sourceSystem;

    public SourceSystemRO getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(SourceSystemRO sourceSystem) {
        this.sourceSystem = sourceSystem;
    }
}
