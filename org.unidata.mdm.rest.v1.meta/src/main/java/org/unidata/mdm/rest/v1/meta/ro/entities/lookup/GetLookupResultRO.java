package org.unidata.mdm.rest.v1.meta.ro.entities.lookup;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.meta.ro.entities.LookupEntityRO;

/**
 * Get lookup entity result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class GetLookupResultRO extends DetailedOutputRO {

    private LookupEntityRO lookupEntity;

    public LookupEntityRO getLookupEntity() {
        return lookupEntity;
    }

    public void setLookupEntity(LookupEntityRO lookupEntity) {
        this.lookupEntity = lookupEntity;
    }

}
