package org.unidata.mdm.rest.v1.meta.ro.system;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Delete system request
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class DeleteSystemResultRO extends DetailedOutputRO {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
