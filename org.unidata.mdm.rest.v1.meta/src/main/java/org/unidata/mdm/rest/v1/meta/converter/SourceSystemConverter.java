/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.core.type.model.SourceSystemElement;
import org.unidata.mdm.meta.type.model.sourcesystem.SourceSystem;
import org.unidata.mdm.rest.core.converter.CustomPropertiesConverter;
import org.unidata.mdm.rest.v1.meta.ro.system.SourceSystemRO;

/**
 * The Class SourceSystemConverter.
 * Convert instance of {@see SourceSystemDef} to {@see SourceSystemRO}.
 */
public class SourceSystemConverter {

    private SourceSystemConverter() {
        super();
    }


    /**
     * Convert instance of {@see SourceSystemDef} to {@see
     * SourceSystemRO}.
     *
     * @param source
     *            the source
     * @return the source system definition
     */
    public static SourceSystemRO to(SourceSystemElement source) {

        if (source == null) {
            return null;
        }

        SourceSystemRO target = new SourceSystemRO();
        target.setName(source.getName());
        target.setDescription(source.getDisplayName());
        target.setWeight(source.getWeight());
        target.setCustomProperties(CustomPropertiesConverter.to(source.getCustomProperties()));

        return target;
    }

    /**
     * TODO Remove this method
     * Convert instance of {@see SourceSystemDef} to {@see
     * SourceSystemRO}.
     *
     * @param source
     *            the source
     * @return the source system definition
     */
    @Deprecated(forRemoval = true)
    public static SourceSystemRO to(SourceSystem source) {

        if (source == null) {
            return null;
        }

        SourceSystemRO target = new SourceSystemRO();
        target.setName(source.getName());
        target.setDescription(source.getDisplayName());
        target.setWeight(source.getWeight() == null ? 0 : source.getWeight().intValue());
        //target.setCustomProperties(AbstractEntityDefinitionConverter.to(source.getCustomProperties()));

        return target;
    }

    /**
     * TODO: Remove this method after merge settings refactoring!
     * Convert instance of {@see SourceSystemDef} to {@see
     * SourceSystemRO}.
     *
     * @param source
     *            the source
     * @return the source system definition
     */
    @Deprecated(forRemoval = true)
    public static List<SourceSystemRO> to(List<SourceSystem> source) {

        if (source == null || source.isEmpty()) {
            return Collections.emptyList();
        }

        List<SourceSystemRO> target = new ArrayList<>();
        for (SourceSystem s : source) {
        	target.add(to(s));
        }

        return target;
    }

    /**
     * Converts from RO to system.
     * @param source the source
     * @return system object
     */
    public static SourceSystem from(SourceSystemRO source) {

        SourceSystem result = null;
    	if (source != null) {

            result = new SourceSystem()
                .withName(source.getName())
                .withDisplayName(source.getDescription())
                .withWeight(Integer.valueOf(source.getWeight()))
                .withCustomProperties(CustomPropertiesConverter.from(source.getCustomProperties()));
        }

    	return result;
    }

    /**
     * Convert instance of {@see SourceSystemDef} to {@see
     * SourceSystemRO}.
     *
     * @param source
     *            the source
     * @return the source system definition
     */
    public static List<SourceSystem> from(List<SourceSystemRO> source) {

        if (CollectionUtils.isEmpty(source)) {
            return Collections.emptyList();
        }

        List<SourceSystem> target = new ArrayList<>();
        for (SourceSystemRO s : source) {
        	target.add(from(s));
        }

        return target;
    }
}
