package org.unidata.mdm.rest.v1.meta.ro.system;

import java.util.List;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Get system result
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class GetSystemsResultRO extends DetailedOutputRO {

    /** The admin system name. */
    private String adminSystemName;
    /** The source system. */
    private List<SourceSystemRO> sourceSystems;

    public String getAdminSystemName() {
        return adminSystemName;
    }

    public void setAdminSystemName(String adminSystemName) {
        this.adminSystemName = adminSystemName;
    }

    public List<SourceSystemRO> getSourceSystems() {
        return sourceSystems;
    }

    public void setSourceSystems(List<SourceSystemRO> sourceSystems) {
        this.sourceSystems = sourceSystems;
    }
}
