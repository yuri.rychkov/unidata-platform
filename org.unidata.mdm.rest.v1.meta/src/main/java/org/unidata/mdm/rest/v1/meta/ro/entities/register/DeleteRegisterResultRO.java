package org.unidata.mdm.rest.v1.meta.ro.entities.register;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Delete register result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class DeleteRegisterResultRO extends DetailedOutputRO {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
