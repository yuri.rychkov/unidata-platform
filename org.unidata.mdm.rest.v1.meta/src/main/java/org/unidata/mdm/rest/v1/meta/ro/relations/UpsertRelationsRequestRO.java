package org.unidata.mdm.rest.v1.meta.ro.relations;

import java.util.List;

/**
 * Upsert Relations Request
 *
 * @author Alexandr Serov
 * @since 25.11.2020
 **/
public class UpsertRelationsRequestRO {

    private String name;
    private List<RelationRO> relations;
    private Long draftId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RelationRO> getRelations() {
        return relations;
    }

    public void setRelations(List<RelationRO> relations) {
        this.relations = relations;
    }

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }
}
