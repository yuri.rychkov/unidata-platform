package org.unidata.mdm.rest.v1.meta.ro.measurement;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Import Measurement Result
 *
 * @author Alexandr Serov
 * @since 27.11.2020
 **/
public class ImportMeasurementResultRO extends DetailedOutputRO {

    private String instanceId;
    private String storageId;

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getStorageId() {
        return storageId;
    }

    public void setStorageId(String storageId) {
        this.storageId = storageId;
    }

}
