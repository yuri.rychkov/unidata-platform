/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.ro.relations;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.rest.v1.meta.exception.MetaRestExceptionIds;
import org.unidata.mdm.system.exception.PlatformBusinessException;

/**
 * Presentation RelType for rest
 */
public enum RelationTypeRO {
    REFERENCES("References"),
    CONTAINS("Contains"),
    MANY_TO_MANY("ManyToMany");

    private final String value;

    private final static String innerRoName = "relType";

    RelationTypeRO(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RelationTypeRO fromValue(String v) {
        for (RelationTypeRO c : RelationTypeRO.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new PlatformBusinessException(
                "RelationTypeRO wrong definition : " + v,
                MetaRestExceptionIds.EX_ENUM_WRONG_VALUE,
                innerRoName,
                v,
                StringUtils.join(RelationTypeRO.values(), ", ")
        );
    }

}
