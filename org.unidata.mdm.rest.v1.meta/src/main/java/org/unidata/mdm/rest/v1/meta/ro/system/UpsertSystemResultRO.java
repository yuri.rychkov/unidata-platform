package org.unidata.mdm.rest.v1.meta.ro.system;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Upsert system result
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class UpsertSystemResultRO extends DetailedOutputRO {

    private String name;
    // TODO: coming soon, wait response from upsert
//    private SourceSystemRO system;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
