/**
 * Rest meta service rendering
 *
 * @author Alexandr Serov
 * @since 28.11.2020
 **/
package org.unidata.mdm.rest.v1.meta.type.rendering;