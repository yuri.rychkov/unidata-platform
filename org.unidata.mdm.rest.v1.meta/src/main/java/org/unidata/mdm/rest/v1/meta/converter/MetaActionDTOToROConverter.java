package org.unidata.mdm.rest.v1.meta.converter;

import org.unidata.mdm.meta.type.input.meta.MetaAction;
import org.unidata.mdm.rest.v1.meta.ro.MetaActionRO;

/**
 * The Class MetaActionDTOToROConverter.
 * @author ilya.bykov
 */
public class MetaActionDTOToROConverter {
	
	/**
	 * Convert.
	 *
	 * @param source the source
	 * @return the meta action RO
	 */
	public static MetaActionRO convert(MetaAction source) {
		if (source == null) {
			return null;
		}
		return MetaActionRO.valueOf(source.name());
	}
}
