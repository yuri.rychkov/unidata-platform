package org.unidata.mdm.rest.v1.meta.ro.model;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.meta.ro.graph.MetaGraphRO;

/**
 * Get Meta Dependency Result
 *
 * @author Alexandr Serov
 * @since 28.11.2020
 **/
public class GetMetaDependencyResultRO extends DetailedOutputRO {

    private MetaGraphRO modelGraph;

    public MetaGraphRO getModelGraph() {
        return modelGraph;
    }

    public void setModelGraph(MetaGraphRO modelGraph) {
        this.modelGraph = modelGraph;
    }
}
