package org.unidata.mdm.rest.v1.meta.ro.entities.lookup;

import org.unidata.mdm.rest.v1.meta.ro.entities.LookupEntityRO;

/**
 * Upsert lookup result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class UpsertLookupRequestRO {

    private LookupEntityRO lookupEntity;
    private Long draftId;

    public LookupEntityRO getLookupEntity() {
        return lookupEntity;
    }

    public void setLookupEntity(LookupEntityRO lookupEntity) {
        this.lookupEntity = lookupEntity;
    }

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }
}
