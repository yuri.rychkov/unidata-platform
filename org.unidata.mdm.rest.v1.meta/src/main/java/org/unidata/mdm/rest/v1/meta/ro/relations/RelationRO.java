/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.ro.relations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.rest.core.ro.CustomPropertyRO;
import org.unidata.mdm.rest.v1.meta.ro.attributes.SimpleAttributeRO;
import org.unidata.mdm.rest.v1.meta.ro.generation.ExternalIdGenerationStrategyRO;

/**
 * The Class RelationDefinition.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RelationRO {

    /** The rel name. */
    private String name;

    /** The display name. */
    private String displayName;
    /** The simple attributes. */
    private List<SimpleAttributeRO> simpleAttributes;

    /** The from entity. */
    private String fromEntity;

    /** The to entity. */
    private String toEntity;

    /** The rel type. */
    private RelationTypeRO relType;

    /** The required. */
    private boolean required;

    /** The to entity default display attributes. */
    private List<String> toEntityDefaultDisplayAttributes;

    /** The to entity default display attributes. */
    private List<String> toEntitySearchAttributes;

    private final List<CustomPropertyRO> customProperties = new ArrayList<>();

    /** Show attr name or not. */
    private boolean useAttributeNameForDisplay;

    /**
     * Ext. ID generation strategy.
     */
    protected ExternalIdGenerationStrategyRO externalIdGenerationStrategy;

    /**
     * Has some data already or not.
     */
    protected boolean hasData;

    /**
     * Gets the simple attributes.
     *
     * @return the simple attributes
     */
    public List<SimpleAttributeRO> getSimpleAttributes() {
        return simpleAttributes;
    }

    /**
     * Sets the simple attributes.
     *
     * @param simpleAttributes
     *            the new simple attributes
     */
    public void setSimpleAttributes(List<SimpleAttributeRO> simpleAttributes) {
        this.simpleAttributes = simpleAttributes;
    }

    /**
     * Gets the from entity.
     *
     * @return the from entity
     */
    public String getFromEntity() {
        return fromEntity;
    }

    /**
     * Sets the from entity.
     *
     * @param fromEntity
     *            the new from entity
     */
    public void setFromEntity(String fromEntity) {
        this.fromEntity = fromEntity;
    }

    /**
     * Gets the to entity.
     *
     * @return the to entity
     */
    public String getToEntity() {
        return toEntity;
    }

    /**
     * Sets the to entity.
     *
     * @param toEntity
     *            the new to entity
     */
    public void setToEntity(String toEntity) {
        this.toEntity = toEntity;
    }

    /**
     * Gets the rel type.
     *
     * @return the rel type
     */
    @JsonGetter
    public String getRelType() {
        return relType.value();
    }

    @JsonIgnore
    public void setRelType(RelationTypeRO relType) {
        this.relType = relType;
    }

    /**
     * Sets the rel type.
     *
     * @param relType
     *            the new rel type
     */
    @JsonSetter
    public void setRelType(String relType) {
        this.relType = RelationTypeRO.fromValue(relType);
    }

    /**
     * Checks if is required.
     *
     * @return true, if is required
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * Sets the required.
     *
     * @param required
     *            the new required
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * Gets the to entity default display attributes.
     *
     * @return the to entity default display attributes
     */
    public List<String> getToEntityDefaultDisplayAttributes() {
        return toEntityDefaultDisplayAttributes;
    }

    /**
     * Sets the to entity default display attributes.
     *
     * @param toEntityDefaultDisplayAttributes
     *            the new to entity default display attributes
     */
    public void setToEntityDefaultDisplayAttributes(List<String> toEntityDefaultDisplayAttributes) {
        this.toEntityDefaultDisplayAttributes = toEntityDefaultDisplayAttributes;
    }

    public List<String> getToEntitySearchAttributes() {
        return toEntitySearchAttributes;
    }

    public void setToEntitySearchAttributes(List<String> toEntitySearchAttributes) {
        this.toEntitySearchAttributes = toEntitySearchAttributes;
    }

    /**
     * Gets the rel name.
     *
     * @return the rel name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the rel name.
     *
     * @param relName
     *            the new rel name
     */
    public void setName(String relName) {
        this.name = relName;
    }

    /**
     * Gets the display name.
     *
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Sets the display name.
     *
     * @param displayName
     *            the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public boolean isHasData() {
        return hasData;
    }

    public void setHasData(boolean hasData) {
        this.hasData = hasData;
    }

    public List<CustomPropertyRO> getCustomProperties() {
        return customProperties;
    }

    public void setCustomProperties(final Collection<CustomPropertyRO> customProperties) {
        if (CollectionUtils.isEmpty(customProperties)) {
            return;
        }
        this.customProperties.addAll(customProperties);
    }
    /**
     * @return the useAttributeNameForDisplay
     */
    public boolean isUseAttributeNameForDisplay() {
        return useAttributeNameForDisplay;
    }

    /**
     * @param useAttributeNameForDisplay the useAttributeNameForDisplay to set
     */
    public void setUseAttributeNameForDisplay(boolean useAttributeNameForDisplay) {
        this.useAttributeNameForDisplay = useAttributeNameForDisplay;
    }

    /**
     * @return the externalIdGenerationStrategy
     */
    public ExternalIdGenerationStrategyRO getExternalIdGenerationStrategy() {
        return externalIdGenerationStrategy;
    }

    /**
     * @param externalIdGenerationStrategy the externalIdGenerationStrategy to set
     */
    public void setExternalIdGenerationStrategy(ExternalIdGenerationStrategyRO externalIdGenerationStrategy) {
        this.externalIdGenerationStrategy = externalIdGenerationStrategy;
    }
}
