package org.unidata.mdm.rest.v1.meta.ro.enumerations;

/**
 * Delete enumeration result
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class DeleteEnumerationResultRO {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
